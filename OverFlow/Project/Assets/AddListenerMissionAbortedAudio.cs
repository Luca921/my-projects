﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AddListenerMissionAbortedAudio : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<Button>().onClick.AddListener(PlayMissionAbortedAudio);
    }

    void PlayMissionAbortedAudio()
    {
        FMODManager.instance.PlayMissionAborted();
    }
}
