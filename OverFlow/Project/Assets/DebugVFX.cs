using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class DebugVFX : MonoBehaviour
{
    public float duration;

    float timeout;
    PostProcessVolume volume;

    // Start is called before the first frame update
    void Start()
    {
        volume = GetComponent<PostProcessVolume>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F12))
        {
            timeout = duration;
            volume.enabled = true;
        }

        if (timeout > 0)
        {
            timeout -= Time.deltaTime;
        }
        else if (volume.enabled)
        {
            volume.enabled = false;
        }
    }
}
