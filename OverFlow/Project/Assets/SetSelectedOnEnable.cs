﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SetSelectedOnEnable : MonoBehaviour
{
    public GameObject buttonToSet;
    public GameObject[] panelsToClose;


    private void OnEnable()
    {
        StartCoroutine(SetSelectedDelayed());
        foreach(GameObject p in panelsToClose)
        {
            p.SetActive(false);
        }
    }

    private void OnDisable()
    {
        GameManager.instance.inputManager.enabled = true;

        if (transform.root.GetComponent<NPCRigger>() != null)
        {
            transform.root.GetComponent<NPCRigger>().canStartDialogue = true;
        }
        GameManager.instance.SetPause();
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    
    // Update is called once per frame
    void Update()
    {
        
    }





    IEnumerator SetSelectedDelayed()
    {
        yield return new WaitForEndOfFrame();
        EventSystem.current.SetSelectedGameObject(buttonToSet);
    }

}
