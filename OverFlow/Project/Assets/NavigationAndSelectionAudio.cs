﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEditor;

public class NavigationAndSelectionAudio : MonoBehaviour, ISelectHandler, ISubmitHandler
{
    public void OnSelect(BaseEventData eventData)
    {
        FMODManager.instance.PlayMenuNavigation();
    }

    public void OnSubmit(BaseEventData eventData)
    {
        FMODManager.instance.PlayeMenuSelection();
    }
}
