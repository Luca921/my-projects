﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ButtonSelectionBehaviour : MonoBehaviour
{
    MyButton thisButton;
    TextMeshProUGUI buttonText;
    public Material selectedMaterial;
    public Material unselectedMaterial;
    FontStyles boldStyle;


    // Start is called before the first frame update
    void Start()
    {
        thisButton = GetComponent<MyButton>();
        buttonText = transform.GetChild(0).GetComponent<TextMeshProUGUI>();
        boldStyle = FontStyles.Bold;
    }

    private void OnDisable()
    {
        //GameManager.instance.inputManager.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        

        if(thisButton.Highlight())
        {
            buttonText.fontMaterial = selectedMaterial;
            buttonText.fontStyle = boldStyle;      
        }
        else
        {
            buttonText.fontMaterial = unselectedMaterial;
            buttonText.fontStyle = FontStyles.Normal;
        }
    }
}
