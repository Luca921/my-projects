using System.Collections.Generic;
using System.Collections;
using UnityEngine.UI;
using UnityEngine;
using System;
using TMPro;
using UnityEditor;

[Serializable]
public class DataToSave
{
    #region Fields

    [Header("Cards"), Space(10)]
    public List<string> temporaryCards;
    public List<string> inventoryCards;
    public List<string> deckCards;

    [Header("Stats"), Space(10)]
    public float strength;
    public float intellect;
    public float agility;
    public float neuralHealth;
    public float luck;
    public float neuralResistance;
    public float ability;
    public float defense;
    public float exp;
    public int level;
    public int attributeP;
    public int skillP;
    public int slotsDeck;

    [Header("Game time"), Space(10)]
    public float gTF;
    public string gTS;

    [Header("Game completion"), Space(10)]
    public bool mission1;
    public bool mission2;
    public bool mission3;
    public bool mission4;
    public bool mission5;
    public bool mission6;
    public float gCompletion;

    [Header("Talent tree"), Space(10)]
    public List<string> unlockedSkills;
    public List<string> equippedSkills;

    [Header("NPC Rigger Exchange Index"), Space(10)]
    public int exIndex;

    [Header("Sliders' Volume")]
    public float sVMusic;
    public float sVSFX;

    #endregion
}

[Serializable]
public class LastSlotSelected
{
    #region Fields

    public int lastSS;

    #endregion

    #region Methods

    public void SaveLS(int lSS)
    {
        lastSS = lSS;
    }

    public void LoadLS(int lSS)
    {
        lSS = lastSS;
    }

    #endregion
}
