using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Collections;

public interface IPersistentData
{
    void SaveDataTo(DataToSave data);
    void LoadDataFrom(DataToSave data);
}

public class DataManager : MonoBehaviour
{
    #region Fields

    public static DataManager instance = null;

    [Header("[Class that contains each data to save and load]")]
    public DataToSave[] dataSaved = new DataToSave[3];

    [Space(10)]
    public LastSlotSelected lastSlotSelected;

    public DataToSave DataToSave { get => dataSaved[currentSlot]; }

    [Space(10)]
    //To check what slot is being used
    public int currentSlot;

    string lastSLot;

    List<IPersistentData> subscribers = new List<IPersistentData>();

    public GameObject savingFeedback;

    #endregion

    #region Methods

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        lastSLot = Path.Combine(Application.persistentDataPath, "LastSlotSaved.txt");
    }

    public void OnDestroy()
    {
        if (instance == this)
        {
            instance = null;
        }
    }

    string GetDataPath(int slot)
    {
        //Initialize the text data file in the project's root and get its data path
        return Path.Combine(Application.persistentDataPath, string.Format("DataSaved{0}.txt", slot));
        //LoadData();
    }

    public void AddSubscriber(IPersistentData s)
    {
        subscribers.Add(s);
    }

    public void RemoveSubscriber(IPersistentData s)
    {
        subscribers.Remove(s);
    }

    private void Update()
    {
        //if (Input.GetKeyUp(KeyCode.S))
        //{
        //    SaveData();
        //}
        //if (Input.GetKeyUp(KeyCode.L))
        //{
        //    LoadData();
        //}
    }

    /*private void OnValidate()
    {
        assetID = AssetDatabase.AssetPathToGUID(AssetDatabase.GetAssetPath(FindObjectOfType<PlayerUI>().experienceBar));
        Debug.Log(assetID);
    }*/
    
    public void SaveData()
    {
        // Before saving you have to collect the data...
        foreach (var s in subscribers)
        {
            s.SaveDataTo(DataToSave);
        }

		string jsonString = JsonUtility.ToJson(DataToSave);
        using (StreamWriter streamWriter = File.CreateText(GetDataPath(currentSlot)))
        {
            streamWriter.Write(jsonString);
        }
    }

    //Save the last slot selected by user
    public void SaveLastSlot()
    {
        lastSlotSelected.SaveLS(currentSlot);
        string jsonString = JsonUtility.ToJson(lastSlotSelected);
        using (StreamWriter streamWriter = File.CreateText(lastSLot))
        {
            streamWriter.Write(jsonString);
        }
    }

    //Load the last slot selected by user
    public void LoadLastSlot()
    {
        try
        {
            using (StreamReader streamReader = File.OpenText(lastSLot))
            {
                string jsonString = streamReader.ReadToEnd();
                lastSlotSelected = JsonUtility.FromJson<LastSlotSelected>(jsonString);
            }
            lastSlotSelected.LoadLS(currentSlot);
        }
        catch (System.IO.FileNotFoundException)
        { }
    }

    public void LoadData()
    {
        //the try and catch syntax is necessary to manage the case the file doesn't exist
		try
		{
            using (StreamReader streamReader = File.OpenText(GetDataPath(currentSlot)))
            {
                string jsonString = streamReader.ReadToEnd();
                dataSaved[currentSlot] = JsonUtility.FromJson<DataToSave>(jsonString);
                FMODManager.instance.slidersVolumeMusic = dataSaved[currentSlot].sVMusic;
                FMODManager.instance.slidersVolumeSFX = dataSaved[currentSlot].sVSFX;
                //if (currentSlot == 0)
                //{
                //    dataSaved[0] = JsonUtility.FromJson<DataToSave>(jsonString);
                //}

                //if (currentSlot == 1)
                //{
                //    dataSaved[1] = JsonUtility.FromJson<DataToSave>(jsonString);
                //}

                //if(currentSlot == 2)
                //{
                //    dataSaved[2] = JsonUtility.FromJson<DataToSave>(jsonString);
                //}
            }

            //...and then after loading you have to set every state of each element according to the data loaded
            foreach (var s in subscribers)
            {
                s.LoadDataFrom(DataToSave);
            }
		}
		catch (System.IO.FileNotFoundException)
		{ }
    }

    public IEnumerator SavingFeedback()
    {
        savingFeedback.SetActive(true);
        yield return new WaitForSeconds(Random.Range(0.2f, 0.4f));
        savingFeedback.SetActive(false);
    }

    #endregion
}
