﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardsResources
{
    Card[] resources;

    public CardsResources()
    {
        resources = Resources.FindObjectsOfTypeAll<Card>();
    }

    public Card InstantiateCardWithName(string name)
    {
        foreach (Card c in resources)
        {
            if (c.name == name)
            {
                return Object.Instantiate<Card>(c);
            }
        }

        return null;
    }
}
