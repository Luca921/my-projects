﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class StatisticsManagerUI : MonoBehaviour
{

    public TextMeshProUGUI[] statisticsBoxes;
    public InGameSlot[] allSlots;
    
    CharacterStatistics pl;
    PlayerController pc;

    void Start()
    {
        pl = GameManager.instance.player;
        pc = pl.GetComponent<PlayerController>();
        //InitStatistics(); // initialize the statistics
    }

    private void OnEnable()
    {
        HandCard currentHandCard = EventSystem.current.currentSelectedGameObject.GetComponentInParent<HandCard>();

        if (currentHandCard != null)
        {
            TestUIUpdateTemp(currentHandCard.thisCard);
        }
    }

    //shows all the player stats in the buildGUI
    public void UpdateStatistics()
    {
        if (pl == null)
        {
            pl = GameManager.instance.player;
            pc = pl.GetComponent<PlayerController>();

        }

        TestUIUpdate();
    }


    private void InitStatistics()
    {
        if (pl == null)
        {
            pl = GameManager.instance.player;
                        pc = pl.GetComponent<PlayerController>();

        }

        statisticsBoxes[0].text = pl.baseNeuralHealth.ToString(); 
        statisticsBoxes[1].text = pl.baseStrength.ToString(); 
        statisticsBoxes[2].text = pl.baseIntellect.ToString(); 
        statisticsBoxes[3].text = pl.baseAgility.ToString(); 
        statisticsBoxes[4].text = pl.baseNeuralResistence.ToString(); 
        statisticsBoxes[5].text = pl.baseLuck.ToString();
        statisticsBoxes[6].text = pc.speed.ToString();

    }

    IEnumerator LateStatUpdate()
    {
        yield return new WaitForEndOfFrame();
        TestUIUpdate();
    }

    public void TestUIUpdate()
    {
        InitStatistics();

        if (pl.bonusNeuralHealth != 0)
        {
            statisticsBoxes[0].text += " + " + (pl.bonusNeuralHealth).ToString();
        }

        if (pl.bonusStrength != 0)
        {
            statisticsBoxes[1].text += " + " + (pl.bonusStrength).ToString();
        }

        if (pl.bonusIntellect != 0)
        {
            statisticsBoxes[2].text += " + " + (pl.bonusIntellect).ToString();
        }

        if (pl.bonusAgility != 0)
        {
            statisticsBoxes[3].text += " + " + (pl.bonusAgility).ToString();
        }

        if (pl.bonusNeuralResistence != 0)
        {
            statisticsBoxes[4].text += " + " + (pl.bonusNeuralResistence).ToString();
        }

        if (pl.bonusLuck != 0)
        {
            statisticsBoxes[5].text += " + " + (pl.bonusLuck).ToString();
        }
        //if (pl.bonusLuck != 0)
        //{
        //    statisticsBoxes[6].text += " + " + (pc.speed).ToString();
        //}
    }

    public void TestUIUpdateTemp(Card c)
    {

        bool affectsAttributes = false;
        bool speedAffected = false;
        bool attributesWereAffected = false;
        bool speedWasAffected = false;
        bool skip = false;

        CardEffectAttributes incomingEffect = new CardEffectAttributes();
        SpeedUp incomingEffectSpeed = new SpeedUp();

        Card.Archetypes incomingArchetype = Card.Archetypes.Armor;
        
        CardEffectAttributes presentEffect = new CardEffectAttributes();
        SpeedUp presentEffectSpeed = new SpeedUp();

        //check if the card modifies the attributes or speed
        foreach (Trigger t in c.triggers)
        {
            
            if(t.trigger == Trigger.Triggers.Equipped )
            { 

                foreach (Effect e in t.effects)
                {
                    //if it modifies attributes
                    if(e is CardEffectAttributes)
                    {
                        incomingEffect = (CardEffectAttributes)e ;

                        incomingArchetype = c.cardArchetypes[c.triggers.IndexOf(t)];

                        affectsAttributes = true;
                        
                        
                    }
                    //if it modifies speed
                    if(e is SpeedUp)
                    {
                        incomingArchetype = c.cardArchetypes[c.triggers.IndexOf(t)];
                        incomingEffectSpeed = (SpeedUp)e;
                        speedAffected = true;
                    }
                }
            }
        }

       
        if(!affectsAttributes && !speedAffected)
        {
            //if it does not modify anything if the card has only one archetype
            if (c.cardArchetypes.Count == 1)
            {
                incomingArchetype = c.cardArchetypes[0];
            }
            //else skip
            else
            {
                skip = true;
            }
        }

        //checks if the slot contains something that affects attributes
        foreach(InGameSlot slot in allSlots)
        {
            //if the slot has the same archetype as the incoming card
            if(slot.archetype == incomingArchetype && !skip)
            {

                if (slot.cards.Count > 0)
                {
                    
                    foreach (Effect e in slot.cards[0].triggers[0].effects)
                    {
                        //if it already contains a card that modifies attributes
                        if (e is CardEffectAttributes)
                        {
                            presentEffect = (CardEffectAttributes)e;
                            attributesWereAffected = true;

                        }

                        //if it already contains a card that modifies speed
                        if (e is SpeedUp)
                        {
                            presentEffectSpeed = (SpeedUp)e;
                            speedWasAffected = true;

                        }
                    }
                }
            }
        }

        //if the slot contains a card that modifies attributes, or the new card is going to , we calculate the new value 
        if (affectsAttributes || attributesWereAffected || speedWasAffected || speedAffected)
        {
            InitStatistics();

            if (incomingEffect.neuralHealthModifier!= 0 || pl.bonusNeuralHealth != 0 || presentEffect.neuralHealthModifier != 0)
            {
                if (pl.bonusNeuralHealth + incomingEffect.neuralHealthModifier - presentEffect.neuralHealthModifier >= 0)
                {
                    statisticsBoxes[0].text += " + " + ((pl.bonusNeuralHealth + incomingEffect.neuralHealthModifier - presentEffect.neuralHealthModifier).ToString());
                }
                else
                {
                    statisticsBoxes[0].text += "  " + ((pl.bonusNeuralHealth + incomingEffect.neuralHealthModifier - presentEffect.neuralHealthModifier).ToString());
                }
            }

            if (incomingEffect.strenghtModifier != 0 ||  pl.bonusStrength != 0 || presentEffect.strenghtModifier != 0)
            {
                if (pl.bonusStrength + incomingEffect.strenghtModifier - presentEffect.strenghtModifier >= 0)
                {
                    statisticsBoxes[1].text += " + " + ((pl.bonusStrength + incomingEffect.strenghtModifier - presentEffect.strenghtModifier).ToString());
                }
                else
                {
                    statisticsBoxes[1].text += "  " + ((pl.bonusStrength + incomingEffect.strenghtModifier - presentEffect.strenghtModifier).ToString());

                }
            }
           
            if (incomingEffect.intellectModifier != 0 ||  pl.bonusIntellect  != 0 ||presentEffect.intellectModifier != 0)
            {
                if (pl.bonusIntellect + incomingEffect.intellectModifier - presentEffect.intellectModifier >= 0)
                {
                    statisticsBoxes[2].text += " + " + (pl.bonusIntellect + incomingEffect.intellectModifier - presentEffect.intellectModifier).ToString();
                }
                else
                {
                    statisticsBoxes[2].text += "  " + (pl.bonusIntellect + incomingEffect.intellectModifier - presentEffect.intellectModifier).ToString();
                }
            }

            if (incomingEffect.agilityModifier != 0 || pl.bonusAgility != 0 || presentEffect.agilityModifier != 0)
            {
                if (pl.bonusAgility + incomingEffect.agilityModifier - presentEffect.agilityModifier >= 0)
                {
                    statisticsBoxes[3].text += " + " + (pl.bonusAgility + incomingEffect.agilityModifier - presentEffect.agilityModifier).ToString();
                }
                else
                {
                    statisticsBoxes[3].text += "  " + (pl.bonusAgility + incomingEffect.agilityModifier - presentEffect.agilityModifier).ToString();
                }
            }

            if (incomingEffect.neuralResistenceModifier != 0 || pl.bonusNeuralResistence != 0 || presentEffect.neuralResistenceModifier != 0)
            {
                if (pl.bonusNeuralResistence + incomingEffect.neuralResistenceModifier - presentEffect.neuralResistenceModifier >= 0)
                {
                    statisticsBoxes[4].text += " + " + (pl.bonusNeuralResistence + incomingEffect.neuralResistenceModifier - presentEffect.neuralResistenceModifier).ToString();
                }
                else
                {
                    statisticsBoxes[4].text += "  " + (pl.bonusNeuralResistence + incomingEffect.neuralResistenceModifier - presentEffect.neuralResistenceModifier).ToString();
                }
            }

            if (incomingEffect.luckModifier != 0 || pl.bonusLuck != 0 || presentEffect.luckModifier != 0)
            {
                if (pl.bonusLuck + incomingEffect.luckModifier - presentEffect.luckModifier >= 0)
                {
                    statisticsBoxes[5].text += " + " + (pl.bonusLuck + incomingEffect.luckModifier - presentEffect.luckModifier).ToString();
                }
                else
                {
                    statisticsBoxes[5].text += "  " + (pl.bonusLuck + incomingEffect.luckModifier - presentEffect.luckModifier).ToString();
                }
            }
           
            if (incomingEffectSpeed.speedModifier !=0 || presentEffectSpeed.speedModifier != 0)
            {
                if (incomingEffectSpeed.speedModifier - presentEffectSpeed.speedModifier >= 0)
                {
                    statisticsBoxes[6].text += " + " + (incomingEffectSpeed.speedModifier - presentEffectSpeed.speedModifier).ToString();
                }
                else
                {
                    statisticsBoxes[6].text += " - " + (Mathf.Abs(incomingEffectSpeed.speedModifier - presentEffectSpeed.speedModifier)).ToString();
                }
            }

        }
        //otherwise we rewrite the old values
        else
        {
            TestUIUpdate();
        }
    }

    private void OnDisable()
    {
        //reactivates the player input

    }
}
