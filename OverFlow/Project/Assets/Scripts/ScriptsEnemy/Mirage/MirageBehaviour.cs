﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MirageBehaviour : EnemyBehaviour
{
    bool playSound = true;
    public float cooldownSound;

    public override void EnterPatrolB(Enemy enemy)
    {
        Mirage mirage = enemy as Mirage;

        base.EnterPatrolB(enemy);

        // Destroy its clones
        foreach (GameObject clone in mirage.clones)
        {
            Destroy(clone);
        }
    }

    public override void EnterSearchB(Enemy enemy)
    {
        base.EnterSearchB(enemy);

        //if (playSound)
        //{
        //    FMODManager.instance.HumanEnemiesTriggered();
        //    playSound = false;
        //    StartCoroutine(CooldownAfterFirstAttack());
        //}
    }

    public override void UpdateSearchB(Enemy enemy)
    {
        Mirage mirage = enemy as Mirage;

        if (playSound)
        {
            FMODManager.instance.HumanEnemiesTriggered();
            playSound = false;
            StartCoroutine(CooldownAfterFirstAttack());
        }

        //Calculate the distance between enemy and player position        
        float distance = (enemy.player.transform.position - transform.position).sqrMagnitude;

        //If Mirage can spawn clone change its state and start a coroutine to start the spawning phase
        if (mirage.canSpawn && mirage.onFloor && distance > mirage.attackRange * mirage.attackRange)
        {
            int nClones = mirage.maxClones - mirage.clones.Count;
            if (nClones > 0)
            {
                mirage.canSpawn = false;
                //Start coroutine that manage the spawn clone phase
                StartCoroutine(SpawnClones(mirage, nClones));
                //Change state in spawnClones
                enemy.state = Enemy.EnemyState.spawnClones;
                //Change animation
                mirage.anim.SetBool("SpawningClones", true);
                return;
            }
        }

        base.UpdateSearchB(enemy);        
    }

    public override void EnterAttackB(Enemy enemy)
    {
        base.EnterAttackB(enemy);
    }

    public override void EnterHitB(Enemy enemy)
    {
        Mirage mirage = enemy as Mirage;
        
        //Disable weapon collider
        mirage.DisableWeapon();

        //Stop spawning clone coroutine 
        if (mirage.anim.GetBool("SpawningClones"))
        {
            mirage.anim.SetBool("SpawningClones", false);
            StopAllCoroutines();
            //Set a delay that allow mirage to spawn clone
            StartCoroutine(DelaySpawn(mirage));
        }

        base.EnterHitB(mirage);
    }

    public override void EnterDeadB(Enemy enemy)
    {
        Mirage mirage = enemy as Mirage;

        base.EnterDeadB(mirage);

        // Destroy its clones
        foreach (GameObject clone in mirage.clones)
        {
            Destroy(clone);
        }
    }

    IEnumerator SpawnClones(Mirage enemy, int nClones)
    {
        //Choose different x spawn position for every clone
        float spawnPoint = transform.position.x + Random.Range(-enemy.spawnRange, enemy.spawnRange);
        Vector3 spawnPosition = new Vector3(spawnPoint, transform.position.y, transform.position.z);
        //Create new clone
        GameObject clone = Instantiate(enemy.clonePrefab, spawnPosition, Quaternion.identity);
        MirageClone mirageClone = clone.GetComponent<MirageClone>();
        //Set all clone's references
        mirageClone.mirage = enemy;
        mirageClone.startPathNode = enemy.startPathNode;
        //add the clones to the room ' s enemy list
        mirageClone.room = enemy.room;
        clone.GetComponent<AStar>().graphMaker = enemy.aStar.graphMaker;
        //Add the next clone reference to a clone list
        enemy.clones.Add(clone);
        //Attend a delay to spawn next clone
        yield return new WaitForSeconds(enemy.delaySpawn);

        // Return to search state
        if (enemy.state == Enemy.EnemyState.spawnClones)
        {
            enemy.state = Enemy.EnemyState.search;
        }
        enemy.anim.SetBool("SpawningClones", false);
        //Coroutine to reset, after X seconds, that Mirage can spawn another clones
        StartCoroutine(DelaySpawn(enemy));
    }

    IEnumerator DelaySpawn(Mirage enemy)
    {
        yield return new WaitForSeconds(enemy.countdownSpawn);
        //Enable Mirage to spawn another clones
        enemy.canSpawn = true;
    }

    public IEnumerator CooldownAfterFirstAttack()
    {
        yield return new WaitForSeconds(cooldownSound);
        playSound = true;
    }
}
