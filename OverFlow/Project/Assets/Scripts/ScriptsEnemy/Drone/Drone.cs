﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drone : Enemy
{
    [Header("Drone Parameters")]
    [Tooltip("Bullet Prefab to spawn when attack")]
    public GameObject bullet;
    [Tooltip("Shoot speed")]
    public float bulletSpeed;
    [HideInInspector]
    public bool canAttack;
    [Tooltip("Number of bullet to shoot when attack")]
    public int nBullet;
    [Tooltip("Offset on y axis that have to move before attack")]
    public float offsetYAttack = 0.5f;
    [Tooltip("Position offset where spawn bullet")]
    public Vector3 offsetBulletSpawn;
    [HideInInspector]
    public int counterBullet;
    [Tooltip("Delay to spawn next bullet")]
    public float attackDelay;
    [HideInInspector]
    public float cooldownAttack;
    [HideInInspector]
    public bool onPosition;

    public GameObject chargeFx;

    [HideInInspector]
    public bool attackFinished = true;
}
