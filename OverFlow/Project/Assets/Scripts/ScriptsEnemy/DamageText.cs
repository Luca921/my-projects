﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DamageText : MonoBehaviour
{
    public float time;
    public float moveAmount;
    public TextMeshPro textMesh;
    public VertexGradient colorGradient;

    // Start is called before the first frame update
    void Start()
    {
        float finalPos = transform.position.y + moveAmount;
        StartCoroutine(AnimationText(finalPos));
    }

    protected IEnumerator AnimationText(float finalPosition)
    {
        WaitForEndOfFrame eof = new WaitForEndOfFrame();
        float startValue = transform.position.y;
        float animationAmount = 0.0f;
        float alphaAmount = 0.0f;
        float speed = 1.0f / time;
        float speedAlpha = 1.0f / (time - 0.75f);

        while (animationAmount < 1.0f)
        {
            animationAmount = Mathf.Min(1.0f, animationAmount + Time.deltaTime * speed);

            float newPos = Mathf.Lerp(startValue, finalPosition, animationAmount);
            transform.position = new Vector3(transform.position.x, newPos, transform.position.z);

            if(animationAmount > 0.75f)
            {
                alphaAmount = Mathf.Min(1.0f, alphaAmount + Time.deltaTime * speedAlpha);

                float newAlpha = Mathf.Lerp(1, 0, alphaAmount);
                textMesh.color = new Color(textMesh.color.r, textMesh.color.g, textMesh.color.b, newAlpha);
            }
            

            yield return eof;
        }

        Destroy(gameObject, 0.1f);
    }
}
