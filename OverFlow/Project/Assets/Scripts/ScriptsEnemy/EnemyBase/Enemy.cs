﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    // ENEMY STATISTICS
    #region Enemy Statistics
    [Space(3), Header("Enemy Parameters")]
    public EnemyParameters enemyStats;  // Scriptable used to set all parameters of enemy
    public float dropExperience;

    [HideInInspector]
    public float speed;
    [HideInInspector]
    public float patrolSpeed;
    [HideInInspector]
    public float attackRange;
    [HideInInspector]
    public float visionRange;
    [HideInInspector]
    public float maxAggroTime;
    [HideInInspector]
    public float loseAggroTimer;
    [HideInInspector]
    public bool isInvulnerable = false;


    //Specify if enemy have a type of champion buff
    public bool isChampion;
    public BuffDebuff greenLeak;
    public ChampionVersion championType;
    public enum ChampionVersion
    {
        none,
        red,
        yellow,
        green,
        blue
    }

    public WeaponClass.Type weaponResistance = WeaponClass.Type.None;
    
    #endregion

    // PATROL PARAMETERS
    #region Patrol Parameters
    [HideInInspector]
    public Node startNode;
    [HideInInspector]
    public float patrolStart;
    [HideInInspector]
    public float patrolEnd;
    [Space(3), Header("Patrol Parameter")]
    public Transform patrolPosition;

    // PATHFINDING REFERENCE
    public Node startPathNode;
    public AStar aStar;
    public PathFollowBehaviour pathFollow;
    [HideInInspector]
    public bool onFloor;
    [HideInInspector]
    public bool canJump;
    [HideInInspector]
    public bool onPatrolPosition = true;
    [HideInInspector]
    public bool stun = false;
    [HideInInspector]
    public bool locked = false;
    #endregion

    // DROP PARAMETERS
    #region Drop Parameters
    [Space(3), Header("Drop Parameters")]
    public int dropChance;
    public float rarityDropIncreaser;
    public GameObject dropPref;
    public float offsetDropY;
    #endregion

    // WEAPON PARAMETERS
    #region Weapon Parameters
    [Space(3),Header("Weapon Collider")]
    public GameObject weaponCollider;
    #endregion

    // ROOM REFERENCE
    #region Room Reference
    //[HideInInspector]
    public RoomBehaviour room;
    #endregion    

    // ENEMY COMPONENTS
    #region Enemy Components
    [HideInInspector]
    public Animator anim;
    [HideInInspector]
    public Rigidbody rb;
    #endregion

    // PLAYER REFERENCE
    #region Player Reference
    [HideInInspector]
    public GameObject player;
    #endregion

    // ENEMY STATE
    #region Enemy State
    public EnemyState state;
    public float deadTime;

    public enum EnemyState
    {
        idle,
        patrol,
        spawnClones,
        search,
        attack,
        hit,
        stun,
        locked,
        dead
    }
    #endregion

    public GameObject[] mod;

    public Material[] mat;

    [HideInInspector]
    public SpawnEffect spawnEffect;

    public GameObject deathFx;

    public GameObject auraFx;

    Color col;

    protected virtual void Awake()
    {
        spawnEffect = GetComponent<SpawnEffect>();
        spawnEffect.ps = deathFx.GetComponentInChildren<ParticleSystem>();

        // Set patrol positions
        if (patrolPosition != null)
        {
            patrolStart = startPathNode.transform.position.x;
            patrolEnd = patrolPosition.transform.position.x;
        }

        if(startPathNode != null)
        {
            startNode = startPathNode;
        }

        // Get animator and rigidbody
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();

        // Set enemy parameters
        speed = enemyStats.speed;
        patrolSpeed = enemyStats.patrolSpeed;
        attackRange = enemyStats.attackRange;
        visionRange = enemyStats.visionRange;
        maxAggroTime = enemyStats.maxAggroTime;

        if (isChampion)
        {
            int champ = Random.Range(1, 5);
            championType = (ChampionVersion)champ;

            //GameObject aura = Instantiate(auraFx, mod[0].GetComponent<SkinnedMeshRenderer>().rootBone);
            //aura.SetActive(true);

            ParticleSystem fx = auraFx.GetComponent<ParticleSystem>();
            var p = fx.main;

            switch (championType)
            {
                case (ChampionVersion.red):
                    {
                        WeaponClass weapon = weaponCollider.GetComponent<WeaponClass>();
                        //Increase base attack stats of 10%
                        if (weapon != null)
                        {
                            weapon.baseAtk += (weapon.baseAtk * enemyStats.attackBonus / 100);
                        }

                        col = new Color(1f, 0f, 0f, 0.7f);

                        break;
                    }

                case (ChampionVersion.yellow):
                    {
                        //Increase speed stats of 10%
                        speed += (speed * enemyStats.speedBonus / 100);

                        col = new Color(1f, 0.5f, 0.2f, 0.7f);

                        break;
                    }

                case (ChampionVersion.blue):
                    {
                        GetComponent<CharacterStatistics>().baseNeuralResistence *= 1.1f;

                        col = new Color(0f, 1f, 1f, 0.7f);

                        break;
                    }
                case (ChampionVersion.green):
                    {
                        col = new Color(0f, 1f, 0f, 0.7f);
                        break;
                    }
                default:
                    {
                        break;
                    }
            }

            p.startColor = new Color(col.r, col.g, col.b, col.a);
            auraFx.SetActive(true);
        }       
    }

    protected virtual void Start()
    {        
        // Get the player instance from Game manager
        player = GameManager.instance.player.gameObject;
    }

    public void DisableWeapon()
    {
        if(weaponCollider != null)
        {
            weaponCollider.SetActive(false);
        }        
    }

    public void EnableWeapon()
    {
        if (weaponCollider != null)
        {
            weaponCollider.SetActive(true);
        }
    }

    public virtual IEnumerator Dead()
    {
        player.GetComponent<CharacterStatistics>().GetXP(dropExperience);
        FMODManager.instance.PlayEnemyDeath();

        //If contains almost one card, drop random one of them
        //if (dropList.Count > 0)
        //{
        //    int indexCard = Random.Range(0, dropList.Count - 1);
        //    // Instantiate the drop from enemy position with offset on Y axis
        //    GameObject drop = Instantiate(dropPref, transform.position + new Vector3(0, offsetDropY, 0), Quaternion.identity);
        //    // Set on drop object the type of card it is
        //    drop.GetComponent<DropBehaviour>().drop = dropList[indexCard];
        //}

        Card dropCard;
        dropCard = GameManager.instance.CurrentMission.Mission.GetTheDrop(dropChance , rarityDropIncreaser);

        Debug.Log("morto!");

        if (dropCard != null)
        {
            

            // Instantiate the drop from enemy position with offset on Y axis
            GameObject drop = Instantiate(dropPref, transform.position + new Vector3(0, offsetDropY, 0), Quaternion.identity);
            // Set on drop object the type of card it is
            drop.GetComponent<DropBehaviour>().drop = dropCard;
        }

        if (room != null && room.enemies.Contains(this))
        {
             room.EnemyDestroyed(this);
        }

        foreach (GameObject g in mod)
        {
            g.GetComponent<SkinnedMeshRenderer>().material = mat[1];
        }

        deathFx.SetActive(true);
        
        spawnEffect.enabled = true;

        // Wait X seconds to drop an object
        yield return new WaitForSeconds(spawnEffect.spawnEffectTime/*deadTime*/);              

        // Destroy enemy
        Destroy(gameObject);
    }

    private void OnCollisionStay(Collision collision)
    {
        //Check if is touching the floor
        if(!onFloor && (collision.gameObject.layer == 9 || collision.gameObject.layer == 12) && rb.velocity.y <= 0)
        {
            onFloor = true;
        }
    }
}
