﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
    LineRenderer lr;
    Vector3 finalPosition;

    public float damage;

    // Start is called before the first frame update
    protected void Awake()
    {
        lr = GetComponent<LineRenderer>();               
    }

    private void OnEnable()
    {
        //Set laser's initial position
        lr.SetPosition(0, transform.position);        

        //Calculate laser's final position
        {
            int layerMask = (1 << 12); //LayerMask of Map layer
            RaycastHit hit;
            if (Physics.Raycast(transform.position, Vector3.right * transform.parent.localScale.x, out hit, 1000f, layerMask))
            {
                finalPosition = hit.point;
                lr.SetPosition(1, finalPosition);
            }
        }
    }

    // Update is called once per frame
    protected void Update()
    {
        //Check if laser is touching player
        {
            int layerMask = (1 << 11); //LayerMask of Player layer
            RaycastHit hit;
            if (Physics.Raycast(transform.position, Vector3.right * transform.parent.localScale.x, out hit, 1000f, layerMask))
            {
                hit.collider.GetComponent<PlayerController>().TakeDamage(damage, false);
            }
        }

    }
}
