﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossBehaviour : EnemyBehaviour
{
    #region Search Functions
    public override void EnterSearchB(Enemy enemy)
    {
        //Set enemy animation to don't run
        enemy.anim.SetBool("Run", false);

        enemy.state = Enemy.EnemyState.search;
    }

    public override void UpdateSearchB(Enemy enemy)
    {
        //Calculate the distance between enemy and player position        
        float distance = (enemy.player.transform.position - transform.position).sqrMagnitude;

        //Calculate the direction where enemy look at
        float direction = Mathf.Sign((enemy.player.transform.position.x - transform.position.x));
        //Look at player position            
        transform.localScale = new Vector3(direction, transform.localScale.y, transform.localScale.z);

        //Check if player is on enemy attack range
        if (distance < enemy.attackRange * enemy.attackRange)
        {
            EnterAttackB(enemy);
            return;
        }
        else
        {            
            //Set enemy animation to run
            enemy.anim.SetBool("Run", true);

            //Move to player position
            Vector3 enemyMovement = transform.right * (direction * enemy.speed * Time.deltaTime);
            enemy.rb.MovePosition(enemy.rb.position + enemyMovement);
        }
    }
    #endregion

    #region Attack Functions
    public override void EnterAttackB(Enemy enemy)
    {
        Boss boss = enemy as Boss;

        enemy.state = Enemy.EnemyState.attack;

        if (boss.useAbilityAttak)
        {
            StartCoroutine(Ability(boss));
            return;
        }
        else if (boss.useRangedAttack)
        {
            enemy.anim.SetTrigger("Backflip");
            boss.rb.AddForce(Vector3.right * -transform.localScale.x * 5000f);


            float choose = Random.value;
            if(choose < 0.5f)
            {
                enemy.anim.SetBool("Shockwave", true);
                StartCoroutine(Shockwave(boss));
            }
            else
            {
                enemy.anim.SetBool("Laser", true);
                StartCoroutine(Laser(boss));
            }

            return;
        }
        else if(boss.whirlwindCooldown > boss.whirlwindTimer)
        {
            enemy.anim.SetBool("Whirlwind", true);
            StartCoroutine(Whirlwind(5, boss));

            return;
        }
        else
        {
            //Set enemy animation to attack melee
            enemy.anim.SetBool("AttackMelee", true);

            return;
        }
    }

    public override void UpdateAttackB(Enemy enemy)
    {
        if (enemy.anim.GetBool("Whirlwind"))
        {
            float distance = enemy.player.transform.position.x - transform.position.x;
            if(Mathf.Abs(distance) >= 0.3f)
            {
                //Calculate the direction where enemy look at
                float direction = Mathf.Sign(distance);
                //Move to player position
                Vector3 enemyMovement = transform.right * (direction * enemy.speed * Time.deltaTime);
                enemy.rb.MovePosition(enemy.rb.position + enemyMovement);
            }            
        }
    }

    public override void AttackFinishB(Enemy enemy)
    {
        Boss boss = enemy as Boss;

        if (boss.abilityUsed)
        {
            boss.abilityUsed = false;
            boss.useAbilityAttak = false;
        }

        if (enemy.locked)
        {
            UpdateLockB(enemy);
        }

        //Return to enemy search state
        if (enemy.state != Enemy.EnemyState.patrol && !enemy.locked)
        {
            EnterSearchB(enemy);
        }
    }

    public IEnumerator Ability(Boss boss)
    {        
        boss.useAbilityAttak = false;
        boss.anim.SetTrigger("AbilityAttack");
        boss.abilityUsed = true;

        yield return new WaitForSeconds(0.5f);

        AbilityWeapon magneticImpulse = Instantiate(boss.magneticImpulse, transform.position + Vector3.up, Quaternion.identity).GetComponent<AbilityWeapon>();
        magneticImpulse.boss = boss;

        yield return new WaitForSeconds(2f);

        AttackFinishB(boss);
    }

    public IEnumerator Laser(Boss boss)
    {
        yield return new WaitForSeconds(1f);
        boss.laser.SetActive(true);

        yield return new WaitForSeconds(2f);

        boss.laser.SetActive(false);
        boss.anim.SetBool("Laser", false);
        boss.useRangedAttack = false;
        AttackFinishB(boss);
    }

    public IEnumerator Shockwave(Boss boss)
    {        
        yield return new WaitForSeconds(1f);

        for (int i = -1; i < 2; ++i)
        {
            //Instantiate new shockwave
            GameObject shockwave = Instantiate(boss.shockwave, transform.position + new Vector3(i,0.5f,0), Quaternion.identity);

            //Apply the reference of its stats
            BulletWeapon shockwaveBe = shockwave.GetComponent<BulletWeapon>();
            shockwaveBe.ownerStats = GetComponent<CharacterStatistics>();
            shockwaveBe.WeaponTypeCases();

            //Apply a force to shoot
            if (i != 0)
            {
                shockwave.GetComponent<Rigidbody>().velocity = Vector3.right * 5f * i;
            }
            else
            {
                shockwave.GetComponent<Rigidbody>().velocity = Vector3.up * 5f;
            }

            if(boss.bossStats.currentHealth <= boss.bossStats.totalHp*0.5f && i != 0)
            {
                //Instantiate new shockwave
                GameObject shockwave2 = Instantiate(boss.shockwave, transform.position + new Vector3(i, 0.5f, 0), Quaternion.identity);

                //Apply the reference of its stats
                BulletWeapon shockwaveBe2 = shockwave2.GetComponent<BulletWeapon>();
                shockwaveBe2.ownerStats = GetComponent<CharacterStatistics>();
                shockwaveBe2.WeaponTypeCases();

                //Apply a force to shoot
                shockwave.GetComponent<Rigidbody>().velocity = new Vector3(5f*i, 5f, 0);
            }
        }

        yield return new WaitForSeconds(2f);

        boss.anim.SetBool("Shockwave", false);
        boss.useRangedAttack = false;
        AttackFinishB(boss);
    }

    public IEnumerator Whirlwind(float time, Boss boss)
    {
        boss.whirlwindVortex.SetActive(true);
        boss.isInvulnerable = true;

        boss.whirlwindFx.SetActive(true);

        //Wait X seconds to reset enemy state
        yield return new WaitForSeconds(time);

        boss.whirlwindFx.SetActive(false);

        boss.anim.SetBool("Whirlwind", false);
        boss.whirlwindCooldown = 0f;
        boss.whirlwindVortex.SetActive(false);
        boss.isInvulnerable = false;

        //Exit from stun state
        EnterStunB(boss,boss.whirlwindStun);
    }

    #endregion

    #region Hit Functions
    public override void EnterHitB(Enemy enemy)
    {
        Boss boss = enemy as Boss;

        //Disable weapon collider
        enemy.DisableWeapon();

        //If hp is greater than 1 enemy is hitted, else the enemy is dead
        if (boss.bossStats.currentHealth > 1)
        {
            //Disable the reference of current active effect
            GameManager.instance.triggerManager.ActivateEffect(Trigger.Triggers.EnemyHit, enemy);

            float rangeTrigger = boss.tempHealth - (boss.tempHealth * boss.rangeAttack / 100f);
            float abilityTrigger = boss.tempHealth - (boss.tempHealth * boss.abilityAttack / 100f);
            
            if(boss.bossStats.currentHealth <= abilityTrigger)
            {
                boss.useAbilityAttak = true;
                boss.tempHealth = abilityTrigger;
            }
            else if(boss.bossStats.currentHealth <= rangeTrigger)
            {
                boss.useRangedAttack = true;
                boss.tempHealth = rangeTrigger;
            }
        }
        else
        {
            //Enter to dead state
            EnterDeadB(enemy);
        }
    }
    #endregion

    public override void EnterDeadB(Enemy enemy)
    {
        StopAllCoroutines();

        base.EnterDeadB(enemy);
    }

    #region Stun Functions
    public virtual void EnterStunB(Enemy enemy, float timeStun)
    {
        enemy.stun = true;

        enemy.anim.SetBool("Stun", true);
        //Start coroutine that manage the stun state of enemy
        StartCoroutine(StunState(timeStun, enemy));
    }

    public override void ExitStunB(Enemy enemy)
    {
        enemy.stun = false;

        enemy.anim.SetBool("Stun", false);

        if(enemy.state != Enemy.EnemyState.dead)
        {
            //Return to search state
            EnterSearchB(enemy);
        }        
    }

    public override IEnumerator StunState(float time, Enemy enemy)
    {
        //Change state to stun
        enemy.state = Enemy.EnemyState.stun;
        //Wait X seconds to reset enemy state
        yield return new WaitForSeconds(time);
        //Exit from stun state
        ExitStunB(enemy);
    }
    #endregion
}
