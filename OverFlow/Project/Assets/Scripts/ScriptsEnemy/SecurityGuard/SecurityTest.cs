﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecurityTest : EnemyBehaviour
{
    int counter;
    Vector3 lastPosition;

    bool playSound = true;
    public float cooldownSound;

    #region Patrol Functions
    public override void EnterPatrolB(Enemy enemy)
    {
        //Stop running animation
        enemy.anim.SetBool("Run", false);
        
        //Change the state on patrol
        enemy.state = Enemy.EnemyState.patrol;

    }

    public override void UpdatePatrolB(Enemy enemy)
    {
        if (enemy.onPatrolPosition)
        {
            //Calculate the distance between enemy and patrolEnd position
            float distance = enemy.patrolEnd - transform.position.x;
            //Invert the patrol points
            if (Mathf.Abs(distance) < 0.1f)
            {
                float temp = enemy.patrolEnd;
                enemy.patrolEnd = enemy.patrolStart;
                enemy.patrolStart = temp;
            }
            //Set direction of enemy
            float direction = Mathf.Sign(distance);
            transform.localScale = new Vector3(direction, transform.localScale.y, transform.localScale.z);
            //Move enemy to patrolEnd position
            Vector3 enemyMovement = transform.right * (direction * enemy.patrolSpeed * Time.deltaTime);
            enemy.rb.MovePosition(enemy.rb.position + enemyMovement);
        }

        // Control distance between enemy and player position
        Vector3 playerPos = enemy.player.transform.position;
        bool seePlayer = (playerPos - transform.position).sqrMagnitude <= enemy.visionRange * enemy.visionRange;
        if (seePlayer)
        {
            if((playerPos.y >= transform.position.y - 0.25f && playerPos.y < transform.position.y + 1.5f))
            {
                ExitPatrolB(enemy);
                EnterSearchB(enemy);
            }
        }
    }

    public override void ExitPatrolB(Enemy enemy)
    {
    }
    #endregion

    #region Attack Functions
    public override void EnterAttackB(Enemy enemy)
    {
        //Change animator and state to Attack
        enemy.state = Enemy.EnemyState.attack;
        enemy.anim.SetTrigger("Attack");
        enemy.anim.SetBool("Run", false);
    }

    public override void UpdateAttackB(Enemy enemy)
    {
    }

    public override void ExitAttackB(Enemy enemy)
    {
        AttackFinishB(enemy);
    }

    public override void AttackFinishB(Enemy enemy)
    {
        if (!enemy.anim.GetBool("Attack") && enemy.locked)
        {
            UpdateLockB(enemy);
        }

        //Return to enemy search state
        if (!enemy.anim.GetBool("Attack") && enemy.state != Enemy.EnemyState.patrol && !enemy.locked)
        {
            if (enemy.state != Enemy.EnemyState.stun)
            {
                enemy.state = Enemy.EnemyState.search;
            }
        }


    }
    #endregion

    #region Search Functions
    public override void EnterSearchB(Enemy enemy)
    {
        //Change to search state
        enemy.state = Enemy.EnemyState.search;

        //if (playSound)
        //{
        //    FMODManager.instance.HumanEnemiesTriggered();
        //    playSound = false;
        //    StartCoroutine(CooldownAfterFirstAttack());
        //}
    }

    public override void UpdateSearchB(Enemy enemy)
    {
        Vector3 distanceV = enemy.player.transform.position - transform.position;

        if (playSound)
        {
            FMODManager.instance.HumanEnemiesTriggered();
            playSound = false;
            StartCoroutine(CooldownAfterFirstAttack());
        }

        //Check if enemy is near platform's end
        Collider[] collisions = Physics.OverlapSphere(transform.position + new Vector3(0.6f*transform.localScale.x,0,0), 0.1f);
        if (collisions.Length == 0)
        {
            //Return to patrol state
            ExitSearchB(enemy);
            EnterPatrolB(enemy);
            enemy.state = Enemy.EnemyState.patrol;
            return;
        }

        //Calculate the distance between enemy and player position        
        float distance = (distanceV).sqrMagnitude;

        //Look at player position
        float direction = Mathf.Sign((enemy.player.transform.position.x - transform.position.x));        
        transform.localScale = new Vector3(direction, transform.localScale.y, transform.localScale.z);

        //Check if player is on enemy attack range
        if (distance < enemy.attackRange * enemy.attackRange && enemy.onFloor)
        {
            EnterAttackB(enemy);
            return;
        }
        else
        {
            //Check if enemy lose the aggro
            if (distance > enemy.visionRange * enemy.visionRange)
            {
                enemy.loseAggroTimer += Time.deltaTime;

                if (enemy.loseAggroTimer >= enemy.maxAggroTime)
                {
                    //Return to patrol state
                    ExitSearchB(enemy);
                    EnterPatrolB(enemy);
                    enemy.state = Enemy.EnemyState.patrol;
                    return;
                }
            }
            else
            {
                //Reset the timer of lose aggro
                enemy.loseAggroTimer = 0;
            }

            //Move to player position
            enemy.anim.SetBool("Run", true);

            float distanceX = Mathf.Abs(distanceV.x);
            //Move enemy to patrolEnd position
            if ( distanceX >= 0.3f)
            {
                //Move to player position
                Vector3 enemyMovement = transform.right * (direction * enemy.speed * Time.deltaTime);
                enemy.rb.MovePosition(enemy.rb.position + enemyMovement);                
            }

            //Check if enemy is blocked 
            if (lastPosition != transform.position)
            {
                lastPosition = transform.position;
            }
            else
            {
                if(counter >= 5)
                {
                    //Return to patrol state
                    ExitSearchB(enemy);
                    EnterPatrolB(enemy);
                    enemy.state = Enemy.EnemyState.patrol;
                    counter = 0;
                    return;
                }
                else
                {
                    counter++;
                }
                
            }

        }
    }

    public override void ExitSearchB(Enemy enemy)
    {
        //Stop running animation
        enemy.anim.SetBool("Run", false);
    }

    public IEnumerator CooldownAfterFirstAttack()
    {
        yield return new WaitForSeconds(cooldownSound);
        playSound = true;
    }

    #endregion

    #region Hit Functions
    public override void EnterHitB(Enemy enemy)
    {
        //Disable weapon collider
        enemy.DisableWeapon();

        //If hp is greater than 1 enemy is hitted, else the enemy is dead
        if (enemy.GetComponent<CharacterStatistics>().currentHealth > 1)
        {
            if (enemy.state != Enemy.EnemyState.stun)
            {
                //Change state and animator to hit
                enemy.anim.SetTrigger("Hit");
                enemy.state = Enemy.EnemyState.hit;
            }

            //Disable the reference of current active effect
            GameManager.instance.triggerManager.ActivateEffect(Trigger.Triggers.EnemyHit, enemy);
        }
        else
        {
            //Enter to dead state
            EnterDeadB(enemy);
        }
    }

    public override void UpdateHitB(Enemy enemy)
    {

    }

    public override void ExitHitB(Enemy enemy)
    {
        //Return to search state
        if (!enemy.anim.GetBool("Hit") && enemy.state != Enemy.EnemyState.patrol)
        {
            if(enemy.state != Enemy.EnemyState.stun)
            {
                enemy.state = Enemy.EnemyState.search;
            }            
        }
    }
    #endregion

    #region Dead Functions
    public override void EnterDeadB(Enemy enemy)
    {
        //Change state and animator to dead
        enemy.anim.SetTrigger("Dead");
        enemy.state = Enemy.EnemyState.dead;

        //Reset basic material
        CharacterStatistics body = GetComponent<CharacterStatistics>();
        body.bodyMesh.material = body.bodyMaterial;

        //Disable the reference of current active effect
        GameManager.instance.triggerManager.ActivateEffect(Trigger.Triggers.EnemyDead, null);

        //Can collide only with Ground and Map Layer
        gameObject.layer = 14; // Dead layer

        //Start the drop system using coroutine "Dead"
        StartCoroutine(enemy.Dead());
    }
    #endregion

    #region Stun Functions

    public override void EnterStunB(Enemy enemy)
    {
        enemy.stun = true;
        //Start coroutine that manage the stun state of enemy
        StartCoroutine(StunState(stunTime, enemy));
    }

    public override void UpdateStunB(Enemy enemy)
    {

    }

    public override void ExitStunB(Enemy enemy)
    {
        enemy.stun = false;
        //Return to search state
        EnterSearchB(enemy);
    }

    public override IEnumerator StunState(float time, Enemy enemy)
    {
        //Change state to stun
        enemy.state = Enemy.EnemyState.stun;
        //Wait X seconds to reset enemy state
        yield return new WaitForSeconds(time);
        //Exit from stun state
        ExitStunB(enemy);
    }
    #endregion

    #region Lock Functions

    public override void EnterLockB(Enemy enemy)
    {
        enemy.locked = true;
        //Start a coroutine that manage the lock state
        StartCoroutine(LockState(lockTime, enemy));
    }

    public override void UpdateLockB(Enemy enemy)
    {
        //Calculate distance between enemy and player position
        float distance = (enemy.player.transform.position - transform.position).sqrMagnitude;

        //If is near player position and is on floor, then attack
        if (distance < enemy.attackRange * enemy.attackRange && enemy.onFloor)
        {
            EnterAttackB(enemy);
            return;
        }
    }

    public override void ExitLockB(Enemy enemy)
    {
        enemy.locked = false;
        //Return to search state
        EnterSearchB(enemy);
    }

    public override IEnumerator LockState(float time, Enemy enemy)
    {
        //Change state in locked
        enemy.state = Enemy.EnemyState.locked;
        //Wait X seconds to exit from lock state
        yield return new WaitForSeconds(time);
        //Exit from lock state
        ExitLockB(enemy);
    }
    #endregion
}
