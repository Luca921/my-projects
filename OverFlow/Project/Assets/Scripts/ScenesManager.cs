﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScenesManager : MonoBehaviour
{

    // Method to activate and deactivate panels
    public void PanelManager(GameObject panel)
    {
        panel.SetActive(!panel.activeSelf);        
    }

    // manages the player's input
    public void InputActivation(bool b)
    {
        GameManager.instance.inputManager.enabled = b;
    }

    public void ReturnToMenu()
    {
        Destroy(GameManager.instance.player.gameObject);
        Destroy(GameManager.instance.gameObject);
        Time.timeScale = 1;        
        SceneManager.LoadScene("New Scene");
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    // Method to load a scene by build settings index sync
    public void LoadScene(int index)
    {
        SceneManager.LoadScene(index);
    }

    // Method to load a scene by build settings index Async
    public void LoadSceneAsync(int index)
    {
        StartCoroutine(AsyncLoad(index));
    }

    IEnumerator AsyncLoad(int index)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(index);

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
}
