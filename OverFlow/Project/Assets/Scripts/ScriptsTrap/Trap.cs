﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trap : MonoBehaviour
{
    [HideInInspector]
    public bool blackout;
    [HideInInspector]
    public bool canActivate = true;
}
