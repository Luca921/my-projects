﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HVoltage : Trap
{
    public GameObject voltage;
    
    LineRenderer lr;
    Vector3 finalPosition;
    Vector3 initialPosition;

    public float time;
    public float damage;
    public float laserDistance;
    public int warningTime;
    bool canDamage = true;

    bool electricity = false;
    public GameObject chargeFx;
    public GameObject electricityFx;

    GameObject elec;

    public GameObject[] models;
    public Material[] materials;
    
    // Start is called before the first frame update
    private void Start()
    {
        //lr = GetComponent<LineRenderer>();
        
        StartCoroutine(ActivatorLaser());

        //Set laser's initial and final position
        initialPosition = transform.position + new Vector3(0f, 0.2f, 0f);
        //lr.SetPosition(0, initialPosition);
        finalPosition = (voltage.transform.position.x * Vector3.right) + new Vector3(-0.5f,voltage.transform.position.y+0.2f,0f);
        //lr.SetPosition(1, finalPosition);

        laserDistance = Mathf.Abs(transform.position.x - voltage.transform.position.x);

        elec = Instantiate(electricityFx, transform);
        elec.transform.rotation = Quaternion.Euler(0f, 0f, 90f);
        elec.transform.position = transform.position + new Vector3(0.5f * laserDistance, 0.3f, 0f);

        ParticleSystem fx = elec.GetComponent<ParticleSystem>();
        var sh = fx.shape;
        sh.scale = new Vector3(fx.shape.scale.x, laserDistance * 0.5f, fx.shape.scale.z);

        var em = fx.emission;
        em.rateOverTime = sh.scale.y * 25;
    }

    private void Update()
    {
        if (!canActivate && !blackout)
        {
            StopAllCoroutines();
            blackout = true;
            //lr.enabled = false;

            foreach (GameObject m in models)
            {
                m.GetComponent<MeshRenderer>().material = materials[1];
            }            

            electricity = false;

            elec.SetActive(false);

            StartCoroutine(BlackOut());
        }

        if (electricity == true/*lr.enabled*/)
        {
            //Check if laser is touching player
            int layerMask = (1 << 11);
            RaycastHit hit;
            if (Time.timeScale != 0 && canDamage && Physics.Raycast(initialPosition, Vector3.right, out hit, laserDistance, layerMask)/*Physics.Raycast(lr.GetPosition(0), lr.GetPosition(1), out hit, laserDistance, layerMask)*/)
            {
                PlayerController player = hit.collider.GetComponent<PlayerController>();

                if (player.SecLifeBar == false)
                {
                    player.TakeDamage(damage, true);
                    StartCoroutine(ResetDamage());
                }                
            }
        }
    }

    protected IEnumerator ActivatorLaser()
    {
        yield return new WaitForSeconds(time);
        
        GameObject dx = Instantiate(chargeFx, initialPosition + new Vector3(0f, 0.2f, 0f), Quaternion.identity);
        GameObject sx = Instantiate(chargeFx, finalPosition + new Vector3(0f, 0.2f, 0f), Quaternion.identity);

        Destroy(dx, warningTime);
        Destroy(sx, warningTime);        
                
        yield return new WaitForSeconds(warningTime);

        electricity = !electricity;

        //lr.enabled = !lr.enabled;

        if (electricity)
        {
            elec.SetActive(true);
        }
        else
        {
            elec.SetActive(false);
        }

        yield return new WaitForSeconds(time);

        //lr.enabled = !lr.enabled;

        electricity = !electricity;

        if (electricity)
        {
            elec.SetActive(true);
        }
        else
        {
            elec.SetActive(false);
        }

        StartCoroutine(ActivatorLaser());
    }
    
    IEnumerator BlackOut()
    {
        yield return new WaitUntil(() => canActivate);

        blackout = false;

        foreach (GameObject m in models)
        {
            m.GetComponent<MeshRenderer>().material = materials[0];
        }        

        StartCoroutine(ActivatorLaser());
    }

    IEnumerator ResetDamage()
    {
        canDamage = false;

        yield return new WaitForSeconds(1f);

        canDamage = true;
    }
}
