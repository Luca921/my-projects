﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EMine : Trap
{
    SphereCollider spCollider;
    bool isActive;
    bool fieldOn = false;
    public int explosionTime;
    public float hp;
    public float damage;
    public GameObject flash;
    public GameObject explosion;
    public GameObject mineField;

    public GameObject model;
    public Material[] materials;

    private void Awake()
    {
        spCollider = GetComponent<SphereCollider>();
    }

    private void Start()
    {
        GameObject field = Instantiate(mineField, transform);

        SphereCollider collider = GetComponent<SphereCollider>();
        var rad = collider.radius;

        ParticleSystem fx = field.GetComponent<ParticleSystem>();
        var size = fx.startSize;

        size = rad;
    }

    private void Update()
    {
        if (!canActivate)
        {
            if (!blackout)
            {
                blackout = true;

                model.GetComponent<MeshRenderer>().material = materials[1];
            }            
        }
        else
        {
            if (blackout)
            {
                blackout = false;

                model.GetComponent<MeshRenderer>().material = materials[0];
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (canActivate && !isActive)
        {
            if (other.gameObject.layer == 11) //Player layer
            {
                isActive = true;               

                StartCoroutine(Explode());
            }
        }
    }

    IEnumerator Explode()
    {
        GameObject f = Instantiate(flash, transform.position, Quaternion.identity);
        Destroy(f, explosionTime);
                
        yield return new WaitForSeconds(explosionTime);

        float radius = spCollider.radius;

        Collider[] explosionCollision = Physics.OverlapSphere(transform.position, radius);

        foreach(Collider c in explosionCollision)
        {
            if(c.gameObject.layer == 11) //Player layer
            {
                c.GetComponent<PlayerController>().TakeDamage(damage, true);
            }
            else if(c.gameObject.layer == 10) //Enemy layer
            {
                c.GetComponent<EnemyController>().TakeDamage(damage, true);
            }
        }

        GameObject expl = Instantiate(explosion, transform.position, Quaternion.identity);
        Destroy(expl, 2f);

        Destroy(gameObject);
    }
}
