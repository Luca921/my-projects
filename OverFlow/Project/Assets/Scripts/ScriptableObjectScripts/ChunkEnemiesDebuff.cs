﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Card Effects/Chunk Enemies Debuff")]
public class ChunkEnemiesDebuff : Effect
{
    public BuffDebuff debuff;

    public override void Discard()
    {
        
    }

    public override void Use()
    {
        target = FindObjectOfType<TargetManager>();

        ApplyDebuff(target.GetAllEnemies());
    }

    //apply the debuff to every enemy
    public void ApplyDebuff(List<Enemy> enemies)
    {
        if (enemies.Count > 0)
        {
            foreach (Enemy en in enemies)
            {
                CharacterStatistics e = en.gameObject.GetComponent<CharacterStatistics>();

                e.DebuffAdd(new BuffDebuff(debuff));
            }
        }
    }
}
