﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Card Effects/Chunk Enemies Status")]
public class ChunkEnemiesStatus : Effect
{
    public Enemy.EnemyState status;

    public override void Discard()
    {
        
    }

    public override void Use()
    {
        target = FindObjectOfType<TargetManager>();

        GameManager.instance.StartCoroutine(ApplyStatus(target.GetAllEnemies()));
    }

    //set the status of every enemy , after a brief delay it sets their states to search
    public IEnumerator ApplyStatus(List<Enemy> enemies)
    {
        if (enemies.Count > 0)
        {
            foreach (Enemy en in enemies)
            {
                en.state = status;
            }

            yield return new WaitForSeconds(duration);

            foreach (Enemy en in enemies)
            {
                en.GetComponent<EnemyController>().EnterPatrol();
            }
        }
    }
}
