using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedShoot : ActiveAbility
{
    public GameObject muzzleFlash;

    int maxCharges;
    int currentCharges;

    private void Start()
    {
        GameManager.instance.OnDungeonStart += SkillCheck; //setup the skill on dungeon start

        player = GetComponentInParent<PlayerController>();

        player.UpdateValue += UpdateCurrent;
    }

    private void OnDestroy()
    {
        player.UpdateValue -= UpdateCurrent;
    }

    private void Update()
    {
        //check if the player can shoot
        if (player.Pressed && canDo && (player.playerState == PlayerController.PlayerState.idle || player.playerState == PlayerController.PlayerState.jump  || player.playerState == PlayerController.PlayerState.walk))
        {

            if (currentCharges > 0)
            {
                currentCharges--;
                player.CaricheCorrenti--;

                GameObject m = Instantiate(muzzleFlash, transform.position, Quaternion.identity);
                FMODManager.instance.PlayerRanged();
                Destroy(m, 1f);

                //bullet
                GameObject g = Instantiate(obj, transform.position, Quaternion.identity);

                player.playerState = PlayerController.PlayerState.ability;
            }

            //if he has enough charges
            if (currentCharges != maxCharges)
            {
                player.StartCooldown = true;
            }
        }
    }

    public void UpdateCurrent(int i)
    {
        currentCharges += i;
    }

    //if it is the selected skill , update the player values
    public override void SkillCheck()
    {
        foreach (Skill s in GameManager.instance.EquippedSkills)
        {
            if (activeSkill.name == s.name)
            {
                canDo = true;

                ActiveSkillEffect f = s.trigger.effects[0] as ActiveSkillEffect;

                maxCharges = f.maxShots;

                player.MaxCariche = maxCharges;

                player.CaricheCorrenti = maxCharges;
                currentCharges = maxCharges;


                player.SkillCooldown = f.cooldown;
                player.CurrentCooldown = player.SkillCooldown;
            }
        }
    }
}
