using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blackout : ActiveAbility
{
    float rad;
    float duration;
    float timer;
    List<Trap> traps = new List<Trap>();
    bool activate = false;

    void Start()
    {
        GameManager.instance.OnDungeonStart += SkillCheck;

        player = GetComponentInParent<PlayerController>();

        playerStats = GetComponentInParent<CharacterStatistics>();
    }

    void Update()
    {
        if (player.Pressed && canDo && (player.playerState == PlayerController.PlayerState.idle || player.playerState == PlayerController.PlayerState.walk) && player.StartCooldown == false)
        {
            TechCheck();

            DisableTraps();

            player.StartCooldown = true;

            player.playerState = PlayerController.PlayerState.ability;
        }

        if (activate)
        {
            if (timer > 0f)
            {
                timer -= Time.deltaTime;
            }
            else if (timer <= 0f)
            {
                foreach (Trap t in traps)
                {
                    if (t != null)
                    {
                        t.canActivate = true;
                    }
                }

                timer = duration;
                traps = new List<Trap>();
                activate = false;
            }
        }
    }

    public void TechCheck()
    {
        duration += playerStats.intellectValues[(int)(playerStats.baseIntellect + playerStats.bonusIntellect)].blackoutDuration;
        timer = duration;
    }

    public override void SkillCheck()
    {
        foreach (Skill s in GameManager.instance.EquippedSkills)
        {
            if (activeSkill.name == s.name)
            {
                canDo = true;

                ActiveSkillEffect f = s.trigger.effects[0] as ActiveSkillEffect;

                rad = f.radius;
                duration = f.totalDuration;
                timer = duration;

                player.MaxCariche = f.maxShots;
                player.SkillCooldown = f.cooldown;
                player.CurrentCooldown = player.SkillCooldown;
            }
        }
    }

    //deactivates all the traps
    public void DisableTraps()
    {
        Vector3 startPos = player.gameObject.transform.position;

        Collider[] colliders = Physics.OverlapSphere(startPos, rad);

        foreach (Collider hit in colliders)
        {
            Trap trap = hit.GetComponent<Trap>();

            if (trap != null)
            {
                trap.canActivate = false;
                traps.Add(trap);
            }
        }

        activate = true;
    }
}
