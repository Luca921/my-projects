﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Mission")]
public class Mission : ScriptableObject
{
    public int missionIndex;
    public RoomBehaviour startRoom;
    public RoomBehaviour finishRoom;
    public RoomBehaviour plotRoom;
    public int minRooms;
    public PoolChunk pool;

    public List<Card> dropCardList;

    [HideInInspector]
    public List<Card> commonDropCards = new List<Card>();
    [HideInInspector]
    public List<Card> unCommonDropCards = new List<Card>();
    [HideInInspector]
    public List<Card> rareDropCards = new List<Card>();

    public int commonTopThreshold = 60;
    public int unCommonTopThreshold = 90;


    public int enemiesMin;
    public int enemiesMax;
    public int enemiesLVMin;
    public int enemiesLVMax;
    public int trapsCountMin;
    public int trapsCountMax;

    CharacterStatistics player;

    //divides the cards to the specific list
    public void setCardsList()
    {
        player = GameManager.instance.player;
        commonDropCards = new List<Card>();
        unCommonDropCards = new List<Card>();
        rareDropCards = new List<Card>();
        foreach (Card card in dropCardList)
        {
            if (card.cardRarity == Card.Rarity.Common)
            {
                commonDropCards.Add(card);
            }
            if (card.cardRarity == Card.Rarity.Uncommon)
            {
                unCommonDropCards.Add(card);
            }
            if (card.cardRarity == Card.Rarity.Rare)
            {
                rareDropCards.Add(card);
            }

        }
    }

    public Card GetTheDrop(int dropChance , float additiveDropChance)
    {

        float playerDrop = player.luckIncreasePerLevel * (player.baseLuck + player.bonusLuck);

        

        //if the card gets dropped
        if(Random.Range(0,10) >= 10-dropChance)
        {
            float random = Random.Range(0, 100) + additiveDropChance + playerDrop;

            //the rarity of the card dropped
            if (random >= 0 && random <= commonTopThreshold)
            {
                return commonDropCards[Random.Range(0, commonDropCards.Count)];
            }
            if (random > commonTopThreshold && random <= unCommonTopThreshold)
            {
                return unCommonDropCards[Random.Range(0, unCommonDropCards.Count)];
            }
            if (random > unCommonTopThreshold)
            {
                return rareDropCards[Random.Range(0, rareDropCards.Count)];
            }
        }

        return null;
    }
}
