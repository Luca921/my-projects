﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Card Effects/On Third Combo Status")]
public class WeaponStatus : Effect
{
    public Enemy.EnemyState status;

    public override void Discard()
    {
        
    }

    public override void Use()
    {
        GameManager.instance.StartCoroutine( ApplyStatus(enemy));
    }

    public IEnumerator ApplyStatus(Enemy en)
    {
        EnemyController e = en.GetComponent<EnemyController>();

        en.state = status;

        yield return new WaitForSeconds(duration);

        if (e.EnemyStats.currentHealth <= 0f)
        {
            en.state = Enemy.EnemyState.dead;
        }
        else
        {
            e.ExitStun();
        }
    }
}
