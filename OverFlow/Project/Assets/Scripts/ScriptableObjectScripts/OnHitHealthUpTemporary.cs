﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Card Effects/On Hit Health Up Temporary")]
public class OnHitHealthUpTemporary : Effect
{
    public float effectDur;
    public float perc;

    bool coroutineStarted = false;    

    private void Awake()
    {
        //when the players gets hitted it activates
        GameManager.instance.OnHit += HealthIncrease;
    }

    public override void Discard()
    {
        
    }

    public override void Use()
    {
        GameManager.instance.StartCoroutine(EffectCo());
    }    

    public IEnumerator EffectCo()
    {
        coroutineStarted = true;

        yield return new WaitForSeconds(effectDur);

        coroutineStarted = false;
    }

    public void HealthIncrease(GameObject en, CharacterStatistics pl)
    {
        if (coroutineStarted)
        {
            EnemyController e = en.GetComponent<EnemyController>();
            //heal the player back of a value when he gets hitted
            pl.currentHealth += (e.Dmg / 100f) * perc;
        }        
    }
}
