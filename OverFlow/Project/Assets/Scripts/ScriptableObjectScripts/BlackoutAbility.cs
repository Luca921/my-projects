﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Active Skills Effects/Blackout")]
public class BlackoutAbility : ActiveSkillEffect
{
    public override void Discard()
    {
        target = FindObjectOfType<TargetManager>();

        Blackout blackout = target.Player.GetComponent<Blackout>();

        if (blackout == null) return;

        blackout.canDo = false;
    }

    public override void Use()
    {
        
    }
}
