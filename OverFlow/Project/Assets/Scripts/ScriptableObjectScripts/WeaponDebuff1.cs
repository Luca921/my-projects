﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Card Effects/Weapon Debuff 2")]
public class WeaponDebuff1 : Effect
{
    public BuffDebuff debuff;

    public override void Use()
    {
        GameManager.instance.OnHit += ApplyDebuff;

        target = FindObjectOfType<TargetManager>();

        WeaponColor(target.PlayerController);

        hasBeenUsed = true;
    }

    public override void Discard()
    {
        if (hasBeenUsed)
        {
            hasBeenUsed = false;

            DisableWeaponAura(target.PlayerController);

            GameManager.instance.OnHit -= ApplyDebuff;
        }
    }

    public void DisableWeaponAura(PlayerController player)
    {
        foreach (PlayerController.Weapon wp in player.weapons)
        {
            ParticleSystem fx = wp.w.GetComponentInChildren<ParticleSystem>();
            fx.gameObject.SetActive(false);
        }
    }

    Color col1;
    Color col2;

    public void WeaponColor(PlayerController player)
    {
        foreach (PlayerController.Weapon wp in player.weapons)
        {
            ParticleSystem fx = wp.w.transform.GetChild(1).GetComponentInChildren<ParticleSystem>();
            var p = fx.transform.GetChild(0).GetComponent<ParticleSystem>().main;

            if (debuff.stat == BuffDebuff.Stat.Leak)
            {
                col1 = new Color(1f, 0f, 0f);
                col2 = new Color(1f, 0f, 0.9013f);
            }
            else
            {
                col1 = new Color(0f, 0.5504f, 1f);
                col2 = new Color(0f, 0.9245f, 0.3331f);
            }

            ParticleSystem.MinMaxGradient grad = new ParticleSystem.MinMaxGradient(col1, col2);
            grad.mode = ParticleSystemGradientMode.TwoColors;

            p.startColor = grad;

            fx.gameObject.SetActive(true);
        }        
    }

    public void ApplyDebuff(GameObject en, CharacterStatistics stats)
    {
        if (stats.identity == CharacterStatistics.Identity.Enemy && stats.currentHealth > 0f)
        {
            stats.DebuffAdd(new BuffDebuff(debuff));
        }

        GameManager.instance.OnHit -= ApplyDebuff;
    }
}
