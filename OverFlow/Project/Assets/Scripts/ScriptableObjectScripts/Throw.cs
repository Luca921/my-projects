﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Card Effects/Parabolic Launch")]
public class Throw : Effect
{
    public int bulletsNum;
    public float angle = 45f;
    public float gravity = 9.8f;
    public float range = 20f;
    public GameObject bullet;

    Vector3 r;
    Transform tr;

    public IEnumerator Projectile()
    {
        yield return null;

        for (int i = 0; i < bulletsNum; i++)
        {
            GameObject b = Instantiate(bullet, tr.position, Quaternion.identity);

            float targetDistance = Vector3.Distance(b.transform.position, r); // gets the distance from "target"

            float bVelocity = targetDistance / (Mathf.Sin(2 * angle * Mathf.Deg2Rad) / gravity);//calculates the velocity

            float vx = Mathf.Sqrt(bVelocity) * Mathf.Cos(angle * Mathf.Deg2Rad); //separate it in the 2 components x,y
            float vy = Mathf.Sqrt(bVelocity) * Mathf.Sin(angle * Mathf.Deg2Rad);

            b.transform.rotation = Quaternion.LookRotation(r - b.transform.position);

            float elapsedTime = 0f;

            while (b != null)
            {
                b.transform.Translate(0, (vy - (gravity * elapsedTime)) * Time.deltaTime, vx * Time.deltaTime);//movement of the projectile

                elapsedTime += Time.deltaTime;                

                yield return null;
            }
        }        
    }

    public override void Use()
    {
        target = FindObjectOfType<TargetManager>();

        //player
        tr = target.Player.transform.GetChild(0); 

        //take as target a point that is at "range" distance from player
        r = tr.position + new Vector3(range * Mathf.Sign(target.Player.transform.localScale.x), 0f, 0f);

        GameManager.instance.StartCoroutine(Projectile());
    }

    public override void Discard()
    {
        
    }
}
