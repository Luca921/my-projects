﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PoolChunk")]
public class PoolChunk : ScriptableObject
{
    public List<RoomBehaviour> rooms;
}
