﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// base effect Scriptable Object 
public abstract class Effect : ScriptableObject
{    
    // a target affected by the effect
    public TargetManager target;

    public Enemy enemy;

    public float duration = 0f;

    // a method that applies the effect
    public abstract void Use();

    // a method that removes the effect
    public abstract void Discard();

    public bool hasBeenUsed = false;

    public void LifeCheck()
    {

    }
}
