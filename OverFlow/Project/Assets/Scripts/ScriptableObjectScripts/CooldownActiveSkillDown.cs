﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Card Effects/Active Skill Cooldown Decrease")]
public class CooldownActiveSkillDown : Effect
{
    public float perc;

    public override void Discard()
    {
        PlayerController player = target.PlayerController;

        if (player == null) return;

        player.CurrentCooldown += (player.CurrentCooldown * perc / 100f);
    }

    public override void Use()
    {
        target = FindObjectOfType<TargetManager>();

        ReduceCooldown(target.PlayerController);
    }

    //decrease the cooldown of the asctive skill
    public void ReduceCooldown(PlayerController player)
    {
        player.CurrentCooldown -= (player.CurrentCooldown * perc/100f);
    }
}
