﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

[CreateAssetMenu(menuName = "Card Effects/Second Life Bar")]
public class SecondLifeBar : Effect
{
    [HideInInspector]
    public float shieldHp;
    public float time = 3f;
    
    public IEnumerator LifeBar(PlayerController p)
    {
        yield return new WaitForSeconds(0.1f);       

        RectTransform bar = target.secondLifeBar.GetComponent<RectTransform>();

        bar.gameObject.SetActive(true);

        p.SecLifeBar = true; //tells the player the bar is active so he doesnt lose health

        float damage = 0f;
        

        //check if we finished the time
        while (bar.localScale.x > 0)
        {
            damage = p.Dmg;
            float scaleToSubtract = damage / shieldHp;

            //the bar decrease faster if we take damage
            bar.localScale = new Vector3(Mathf.Clamp01(bar.localScale.x - ((Time.deltaTime/time) + scaleToSubtract)), bar.localScale.y, bar.localScale.z);

            yield return null;            
        }

        bar.gameObject.SetActive(false);

        bar.localScale = Vector3.one;

        p.SecLifeBar = false;
    }

    //starts the coroutine when the effect has been used
    public override void Use()
    {
        target = FindObjectOfType<TargetManager>();

        hasBeenUsed = true;

        GameManager.instance.StartCoroutine(LifeBar(target.PlayerController));
    }

    //unable the bar when the effect expire
    public override void Discard()
    {
        if (hasBeenUsed)
        {
            if(target == null)
            {
                target = FindObjectOfType<TargetManager>();
            }

            PlayerController player = target.PlayerController;

            if (player == null) return;

            player.SecLifeBar = false;
        }        
    }
}
