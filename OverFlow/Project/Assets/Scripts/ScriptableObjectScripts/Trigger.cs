﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Trigger 
{
    //it groups all the type of effects' s  triggers of the game 

    public enum Triggers { Equipped, PlayerHit, WeaponChange, LowHealth, HighHealth, ChunkEnter, Consumable, Consumable1, Consumable2, Dash, Jump, EnemyHit, EnemyDead, ChunkCompleted, ThirdComboAttack, CardSentToGraveyard, ActiveAbility, DebuffOnPlayer, Attribute, UntilLowHealth, UntilHighHealth, None }
    public Triggers trigger;
    public int life;
    public bool effectUsed;
    public List<Effect> effects;
    public string description;
    public bool canActivate;

    public Trigger(Triggers trigger, int lf, bool use, List<Effect> effects, string ds, bool can)
    {
        this.trigger = trigger;
        this.life = lf;
        this.effectUsed = use;
        this.effects = effects;
        this.description = ds;
        this.canActivate = can;
    }
}
