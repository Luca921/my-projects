﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Card Effects/Debuff Modifier")]
public class DebuffModifier : Effect
{
    public enum Character { Enemy, Player }
    public Character character;

    public float perc;

    public override void Discard()
    {
        switch (character)
        {
            case (Character.Enemy):
                DebuffDown(target.GetAllEnemies());
                break;

            case (Character.Player):
                DebuffDown(target.CharacterStatistics);
                break;
        }
    }

    //increase the duration of the debuffs
    public override void Use()
    {
        target = FindObjectOfType<TargetManager>();

        switch (character)
        {
            case (Character.Enemy):
                DebuffUp(target.GetAllEnemies());
                break;

            case (Character.Player):
                DebuffUp(target.CharacterStatistics);
                break;
        }

    }

    public void DebuffUp(List<Enemy> enemies)
    {
        foreach (Enemy en in enemies)
        {
            CharacterStatistics stats = en.gameObject.GetComponent<CharacterStatistics>();

            stats.debuffTimeIncreaser = perc;
        }
    }

    public void DebuffUp(CharacterStatistics p)
    {
        p.debuffTimeIncreaser = perc;
    }

    public void DebuffDown(List<Enemy> enemies)
    {
        foreach (Enemy en in enemies)
        {
            CharacterStatistics stats = en.gameObject.GetComponent<CharacterStatistics>();

            stats.debuffTimeIncreaser = 0f;
        }
    }

    public void DebuffDown(CharacterStatistics p)
    {
        p.debuffTimeIncreaser = 0f;
    }
}
