﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Card Effects/AbilityCurrentCooldown")]
public class ActiveSkillCurrentCooldownDecrease : Effect
{
    public float currentCooldownMod;

    public override void Discard()
    {
        PlayerController player = target.PlayerController;

        if (player == null) return;

        player.CurrentCooldown += currentCooldownMod;
    }

    //decrease the cooldown the player is actually waiting to reset
    public override void Use()
    {
        target = FindObjectOfType<TargetManager>();

        target.PlayerController.CurrentCooldown -= currentCooldownMod;
    }    
}
