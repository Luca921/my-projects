﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(GridLayoutGroup))]
public class InventoryNavigationController : MonoBehaviour
{
    [Tooltip("Card slot prefab")]
    public GameObject cardSlot;
    [Tooltip("The default selected object")]
    public Button saveDeckButton;

    public GameObject otherPanel;

    //menu navigation
    Navigation customNav;

    int columns;
    int elementsCounter = 0;

    //sets the navigation of the lists (deck and inventory in the Hub)
    public void SetNavigation(List<GameObject> ObjectsGUI, GridLayoutGroup myGrid)
    {
        columns = myGrid.constraintCount;
        elementsCounter = 0;
        float rows = (float)ObjectsGUI.Count / (float)columns;
        
        // cycles the rows
        for (int row = 0; row < (int)rows; row++)
        {
            customNav = new Navigation();
            customNav.mode = Navigation.Mode.Explicit;

            //cycles every column
            for (int column = 0; column < myGrid.constraintCount; column++)
            {
                //checks on right 
                if (elementsCounter + 1 < ObjectsGUI.Count)
                {
                    customNav.selectOnRight = ObjectsGUI[elementsCounter + 1].transform.GetChild(1).GetComponent<Button>();
                }
                //checks left
                if (elementsCounter != 0 )
                {
                    customNav.selectOnLeft = ObjectsGUI[elementsCounter - 1].transform.GetChild(1).GetComponent<Button>();
                }
                //checks up
                if (elementsCounter - myGrid.constraintCount >= 0)
                {
                    customNav.selectOnUp = ObjectsGUI[elementsCounter - myGrid.constraintCount].transform.GetChild(1).GetComponent<Button>();
                }
                //checks down
                if (elementsCounter + myGrid.constraintCount < ObjectsGUI.Count)
                {
                    customNav.selectOnDown = ObjectsGUI[elementsCounter + myGrid.constraintCount].transform.GetChild(1).GetComponent<Button>();
                }

                
                
                //im in the deck (becouse im on left side)
                if (otherPanel.transform.position.x < transform.position.x)
                {
                    //check if the element is the most left
                    if ( column == 0 )
                    {
                        customNav.selectOnLeft = null;
                    }
                    if ( column == 3 )
                    {
                        customNav.selectOnRight = null;
                    }
                }

                //assign the navigation to the card
                ObjectsGUI[elementsCounter].transform.GetChild(1).GetComponent<Button>().navigation = customNav;

                elementsCounter++;


                //checks if there is another element and we can keep counting 
                if (elementsCounter +1 > ObjectsGUI.Count)
                {
                    break;
                }
            }

            
        }

        //means there is a non complete row ( check the last row )
        if(rows %2 != 0)
        {

            for(int k = elementsCounter; k < ObjectsGUI.Count; k++)
            {
                customNav = new Navigation();
                customNav.mode = Navigation.Mode.Explicit;

                //down check
                customNav.selectOnDown = null;

                //up check
                if(k - myGrid.constraintCount >= 0)
                {
                    customNav.selectOnUp = ObjectsGUI[elementsCounter -  myGrid.constraintCount].transform.GetChild(1).GetComponent<Button>();
                }


                //left check
                if(k!=0)
                {
                    customNav.selectOnLeft = ObjectsGUI[elementsCounter - 1].transform.GetChild(1).GetComponent<Button>();
                }
                else if (k == 0 && saveDeckButton.transform.position.x < transform.position.x)
                {
                    customNav.selectOnLeft = saveDeckButton;
                }

                //right check
                if(k + 1 < ObjectsGUI.Count)
                {
                    customNav.selectOnRight = ObjectsGUI[elementsCounter + 1].transform.GetChild(1).GetComponent<Button>();
                }
                else if (k + 1 >= ObjectsGUI.Count && saveDeckButton.transform.position.x > transform.position.x)
                {
                    customNav.selectOnRight = saveDeckButton;
                }
                else if (k + 1 >= ObjectsGUI.Count && saveDeckButton.transform.position.x < transform.position.x)
                {
                    customNav.selectOnRight = null;
                }

                //assign the navigation to the card
                ObjectsGUI[k].transform.GetChild(1).GetComponent<Button>().navigation = customNav;

                elementsCounter++;
            }
        }

    }

    

}
