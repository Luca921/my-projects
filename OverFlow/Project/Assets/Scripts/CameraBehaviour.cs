﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBehaviour : MonoBehaviour
{
    [Tooltip("Distance of the camera from the target on Y axis")]
    public float offsetY;

    [Tooltip("Distance from the middle screen the character has to outmatch before the camera starts moving")]
    public float offsetX;

    [Tooltip("Distance of the camera from the target on Z axis")]
    public float offsetZ;

    public GameObject glitchFx;
    public float glitchDuration;

    [HideInInspector]
    public Transform target;

    public float lerpSpeed;
    bool cameraFollowing;

    Vector3 oldPlayerPos;

    PlayerController player;
    bool glitchOn = false;

    private void Awake()
    {
        //FMODManager.instance.StopRainMusic();
    }

    private void Start()
    {
        cameraFollowing = false;

        

        if (target == null)
        {
            target = GameManager.instance.p.transform;
            player = target.GetComponent<PlayerController>();
        }
    }

    private void Update()
    {
        if (glitchFx != null && player.Hit == true && glitchOn == false)
        {
            StartCoroutine(GlitchActivation());
        }
    }

    public IEnumerator GlitchActivation()
    {
        player.Hit = false;

        glitchOn = true;

        glitchFx.SetActive(true);

        yield return new WaitForSeconds(glitchDuration);

        glitchFx.SetActive(false);

        glitchOn = false;    
    }

    void FixedUpdate()
    {
        if(oldPlayerPos == target.transform.position) // check if the player is moving , so knows if it has to stop moving 
        {
            cameraFollowing = false;
        }

        if (transform.position.x < target.position.x - offsetX && !cameraFollowing || transform.position.x > target.position.x + offsetX)
        {
            cameraFollowing = true; // player is outside the "non camera moving box "
        }

        if (cameraFollowing)
        {
            //if the player is moving the camera follows it
            Vector3 yOffset = new Vector3(0, offsetY, offsetZ);
            transform.position = Vector3.Lerp(transform.position, target.position + yOffset, lerpSpeed);
        }

        if(!cameraFollowing)
        {
            // if the player is not moving outside the box the camera just adjust her Y
            transform.position = new Vector3(transform.position.x, Mathf.Lerp(transform.position.y , target.position.y + offsetY, lerpSpeed) , transform.position.z);
        }

        oldPlayerPos = target.transform.position; // if the player stops moving the camera wont follow
    }
}
