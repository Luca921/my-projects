﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//class on every enemy to load the level point distributions of every stat
public class EnemyLevelUpData : MonoBehaviour
{
    [Tooltip ("The XML file we get the data from")]
    public TextAsset dataToGet; //we ll get the data from here (every enemy will have his own one)

    [Range(0,50)]
    public int enemyLevel;

    public bool manualLevel;

    public float xpPerLevel;

    CharacterStatistics thisEnemyStats;

    
    public EnemyLevelValues[] enemyValues;

    private XmlDataGetter xmlDataGetter; // the xml values will be stored here

    void Start()
    {


        thisEnemyStats = GetComponent<CharacterStatistics>();
        enemyValues = XmlDataGetter.LoadFromXML<EnemyLevelValues>(dataToGet);

        if(!manualLevel)
        {
            enemyLevel = GameManager.instance.CurrentMission.Mission.enemiesLVMin;
        }

        //we load the data
      //  xmlDataGetter = XmlDataGetter.LoadFromXML(dataToGet);

        //if the enemy level has been set more than 2 we assign the values
        if (enemyLevel >= 2)
        {
            
            thisEnemyStats.level = enemyLevel;
            
            for (int i = 0; i <= enemyLevel - 2; i++)
            {
                thisEnemyStats.baseStrength += enemyValues[i].STRENGTH;
            }
            for (int i = 0; i <= enemyLevel - 2; i++)
            {
                thisEnemyStats.baseAgility += enemyValues[i].DEXTERITY;
            }
            for (int i = 0; i <= enemyLevel - 2; i++)
            {
                thisEnemyStats.baseIntellect += enemyValues[i].TECH;
            }
            for (int i = 0; i <= enemyLevel - 2; i++)
            {
                thisEnemyStats.baseNeuralHealth += enemyValues[i].NEURALHEALTH;
                
            }

            for (int i = 0; i <= enemyLevel - 2; i++)
            {
                thisEnemyStats.baseNeuralResistence += enemyValues[-2 + enemyLevel].NEURALRESISTANCE;
            }

            thisEnemyStats.currentHealth = thisEnemyStats.hpValues[(int)(thisEnemyStats.bonusNeuralHealth + thisEnemyStats.baseNeuralHealth) - 1];

            thisEnemyStats.NeuralHealthModified();


        }

        GetComponent<Enemy>().dropExperience = enemyLevel * xpPerLevel;
    }

}
