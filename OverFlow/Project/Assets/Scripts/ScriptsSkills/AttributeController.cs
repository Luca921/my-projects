using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AttributeController : AbilityController
{
    private new void Start()
    {
        base.Start();

        Init();

        Refresh();

        UpdateAbilities();
    }

    //equip the skill
    public override void Equip(Skill a, Image img)
    {
        if (img.color == Color.white && !justOne)
        {
            img.color = Color.red;
            GameManager.instance.EquipSkill(a);
            justOne = true;
        }
        else if (img.color == Color.red)
        {
            img.color = Color.white;
            GameManager.instance.UnequipSkill(a);
            justOne = false;
        }

        UpdateAbilities();
    }

    //initialize the actributes panel
    public override void Init()
    {
        foreach (Skill attribute in tier.attributes)
        {
            Skill a = ScriptableObject.Instantiate(attribute);
            GameObject g = Instantiate(slot, transform);
            g.name = a.name;
            Image img = g.transform.GetChild(1).GetComponent<Image>();
            img.sprite = a.icon;
            Button b = g.GetComponentInChildren<Button>();
            b.onClick.AddListener(() => Equip(a, img));
            SkillInfo info = g.GetComponent<SkillInfo>();
            info.ShowAbilityInfo(a);
            objects.Add(g);
            abilities.Add(a);
        }
    }

    public override void Refresh()
    {
        // every start check if the player has unlocked a new tier
        if (tierController.tierUnlocked)
        {
            //if its unlocked sets the images as interactable and sets up the description
            foreach (Skill a in tier.attributes)
            {
                GameObject g = objects.Find(GameObject => GameObject.name == a.name);
                Image img = g.transform.GetChild(1).GetComponent<Image>();
                img.color = Color.white;
                SkillInfo info = g.GetComponent<SkillInfo>();
                info.ShowAbilityInfo(a);
            }
        }
    }

    public override void UpdateAbilities()
    {
        tier.attributes = abilities;
    }
}
