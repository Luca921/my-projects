﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//review , utile ?
public class SkillInfo : MonoBehaviour
{    
    public GameObject panel;

    public new Text name;
    public Text type;
    public Text description;
    public Text cost;
    
    private MyButton b;
    private Image img;

    private void Update()
    {
        //open the panel only if the skill is hovered
        if ( b.Highlight() == true)
        {
            if (img.color.a == 1f)
            {
                panel.SetActive(true);
            }
        }
        else
        {
            panel.SetActive(false);
        }        
    }

    private void Start()
    {        
        b = GetComponentInChildren<MyButton>();
        img = b.GetComponent<Image>();
    }

    public void ShowAbilityInfo(Skill s)
    {
        name.text = "Name : " + s.name;
        type.text = "Type : " + s.type.ToString();
        description.text = "Effect : " + s.description;
        cost.text = "Cost : " + s.abilityPointsNeeded.ToString();
    }
}
