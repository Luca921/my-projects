﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


//review usefull
public abstract class AbilityController1 : MonoBehaviour
{
    public GameObject slot; // prefab of the card shape
    public int number;
    public GameObject sign;

    [HideInInspector]
    public Tier tier; //what tier this controller manages

    [HideInInspector]
    public TierController tierController;

    [HideInInspector]
    public bool justOne = false;

    [HideInInspector]
    public List<GameObject> objects = new List<GameObject>(); // list of the card slots instantiated

    [HideInInspector]
    public List<Skill> abilities = new List<Skill>(); // list of the skills in that panel

    TierController[] tierControllers;

    public TierController[] TierControllers
    {
        get { return tierControllers; }
        set { tierControllers = value; }
    }

    public void Start()
    {
        tierControllers = GetComponentsInParent<TierController>();
        tierController = tierControllers[number - 1];
        tier = tierController.CurrentTier;
    }

    public abstract void Init();

    public abstract void Refresh();

    public abstract void Equip(Skill s, GameObject g);

    public abstract void UpdateAbilities();
}
