using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//review usefull ?
public class SkillController : AbilityController
{
    private new void Start()
    {
        base.Start();

        Init();

        Refresh();

        UpdateAbilities();
    }

    public override void Equip(Skill s, Image img)
    {
        if (img.color == Color.white && !justOne)
        {
            img.color = Color.red; //sets the card as selected
            GameManager.instance.player.skillPoints -= s.abilityPointsNeeded; //subtact the points needed to unlock the skill
            s.abilityPointsNeeded = 0;
            GameManager.instance.EquipSkill(s); //add it to the equipped skill
            GameManager.instance.UnlockSkill(s);
            justOne = true;
        }
        //deselect the skill
        else if (img.color == Color.red)
        {
            img.color = Color.white;
            GameManager.instance.UnequipSkill(s); // removes it from the equipped skills
            justOne = false;
        }

        Refresh();

        UpdateAbilities();
    }

    public override void Init()
    {
        //cicle all the skills inserted in this tier and instantiate them
        foreach (Skill skill in tier.skills)
        {
            Skill s = ScriptableObject.Instantiate(skill);
            GameObject g = Instantiate(slot, panel.transform);
            g.name = s.name; // stores the name of the skill
            Image img = g.transform.GetChild(1).GetComponent<Image>();
            img.sprite = s.icon;
            Button b = g.GetComponentInChildren<Button>();
            b.onClick.AddListener(() => Equip(s, img)); //on click add this function
            SkillInfo info = g.GetComponent<SkillInfo>();
            info.ShowAbilityInfo(s);
            objects.Add(g);
            abilities.Add(s);
        }
    }

    public override void Refresh()
    {
        if (tierController.tierUnlocked)
        {
            foreach (Skill s in tier.skills)
            {
                GameObject g = objects.Find(GameObject => GameObject.name == s.name); //check every skill by the name between the instantiated gameobjects
                Image img = g.transform.GetChild(1).GetComponent<Image>();
                SkillInfo info = g.GetComponent<SkillInfo>();

                // check if the ability is buyable
                var gm = GameManager.instance;
                bool isUnlocked = gm.IsSkillUnlocked(s);
                if (!isUnlocked && gm.player.skillPoints >= s.abilityPointsNeeded)
                {
                    img.color = Color.white;
                }
                //if not
                else if (!isUnlocked && gm.player.skillPoints < s.abilityPointsNeeded)
                {
                    float alpha = .5f;
                    img.color = new Color(1, 1, 1, alpha);
                }
                else if (isUnlocked && gm.IsSkillEquipped(s))
                {
                    img.color = Color.red;
                }

                info.ShowAbilityInfo(s);
            }
        }
    }

    public override void UpdateAbilities()
    {
        tier.skills = abilities;
    }
}
