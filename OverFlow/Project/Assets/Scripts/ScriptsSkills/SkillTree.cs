using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillTree : MonoBehaviour
{
    private Tier[] currentTiers = new Tier[8];

    public Tier[] CurrentTiers
    {
        get { return currentTiers; }
        set { currentTiers = value; }
    }
}
