﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletWeapon : WeaponClass
{
    private void OnTriggerEnter(Collider other)
    {
        //If touch player, ground and map layer, destroy itself
        if(other.gameObject.layer == 9 || other.gameObject.layer == 12 || other.gameObject.layer == 11)
        {
            Destroy(gameObject);
        }        
    }
}
