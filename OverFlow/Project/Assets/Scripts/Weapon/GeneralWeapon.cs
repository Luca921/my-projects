﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneralWeapon : MonoBehaviour
{
    public enum WeaponType
    {
        Katana , Hammer , Spear
    }

    public WeaponType thisWeapon;


    public float baseAtk; // base damage *= all the scalings of the weapon
    float atk;
    float totalDamage; // damage after the formula
    float scaling;

    public CharacterStatistics ownerStatistics;

    private void Start()
    {
        atk = baseAtk;
    }

    public void DamageEnemy(CharacterStatistics defender , CharacterStatistics attacker)
    {


        #region Calculate atk

        atk = baseAtk;
        scaling = 0;

        //calculates the atk if i have the katana
        if (thisWeapon == WeaponType.Katana)
        {
            float totalStrength = attacker.level;

            atk += (atk / 100) * attacker.levelValues[(int)totalStrength].katanaScaling;
        }

        //calculates the atk if i have the hammer
        if (thisWeapon == WeaponType.Hammer)
        {
            int totalStrength = (int)(Mathf.Clamp(attacker.baseStrength + attacker.bonusStrength, 0, 100));

            for (int i = 0; i < totalStrength ; i++)
            {
                atk += ( atk / 100 ) * attacker.agilityStrenghtValues[i].Bab;
               
            }
            if (totalStrength >= 1)
            {
                scaling = attacker.agilityStrenghtValues[totalStrength - 1].scaling;
            }
        }


        //calculates the attack if i have the spear
        if (thisWeapon == WeaponType.Spear)
        {
            int totalAgility = (int)(Mathf.Clamp(attacker.baseAgility + attacker.bonusAgility, 0, 100));

            for (int i = 0; i < totalAgility ; i++)
            {
                atk *= attacker.agilityStrenghtValues[i].Bab;
            }

            if (totalAgility >= 1)
            {
                scaling = attacker.agilityStrenghtValues[totalAgility - 1].scaling;
            }
           

        }
        #endregion

        #region Calculate Def
        // calculates the defence
        float def = defender.baseDefense;

        for( int i = 0; i < defender.baseNeuralResistence; i++)
        {
            def += ( def / 100 ) * defender.defenseValues[i];
        }
        #endregion

        #region Formula
        if ( atk >= def)
        {
            //calculates the damage without scaling
            float damageNoScaling = 0.5f * ((Mathf.Pow(atk, 3) / Mathf.Pow(def, 2)) * 0.1f * (Mathf.Pow(atk, 2) / def) * 0.1f * atk);

            //deal the damage adding the scaling
            defender.currentHealth -= damageNoScaling + (damageNoScaling/100)*scaling;
        }
        else
        {
            //calculates the damage without scaling
            float damageNoScaling = (atk - 0.8f) * ((def * (Mathf.Pow(2.72f, (-0.3f * (def / atk))))));

            //deal the damage adding the scaling
            defender.currentHealth -= damageNoScaling + (damageNoScaling/100)*scaling;
        }
        #endregion

    }

}
