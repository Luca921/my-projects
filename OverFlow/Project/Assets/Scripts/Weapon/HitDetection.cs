﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitDetection : MonoBehaviour
{
    public enum Target { Player = 11, Enemy = 10 }
    public Target target;

    public CharacterStatistics character;

    int layer;

    private void Awake()
    {  
        layer = (int)target;
    }

    void Start()
    {
        character = GetComponentInParent<CharacterStatistics>();

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == layer)
        {
            StartCoroutine(ResetAttack(other));
        }
    }

    public IEnumerator ResetAttack(Collider co)
    {
        yield return new WaitForSeconds(.5f);

        GameManager.instance.Hit(co.gameObject, character);
    }
}
