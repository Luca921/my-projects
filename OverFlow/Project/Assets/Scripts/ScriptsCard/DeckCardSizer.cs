﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RectTransform)), RequireComponent(typeof(GridLayoutGroup))]
public class DeckCardSizer : MonoBehaviour
{    
    public RectTransform panelRectTransform; // panel that contain us
    public GameObject terminalPanel;
    public Scrollbar scrollbar; //the scrollbar we need to change the value to
    public int cardsInView;
    GameObject currentHoveredCard;
    GameObject prevHoveredCard;
    GridLayoutGroup myGrid;

    float panelHeight;
    float panelWidth;
    float topThreshold;
    float botThreshold;
    bool imInRightPanel;

    float xCellValue , yCellValue;

    private void OnEnable()
    {
        scrollbar.value = 1;
    }

    void Start()
    {
        imInRightPanel = false;

        myGrid = GetComponent<GridLayoutGroup>();

        panelWidth = panelRectTransform.rect.width; // width of the panel
        panelHeight = panelRectTransform.rect.height; // height of the panel
        xCellValue = panelWidth *0.85f;
        yCellValue = xCellValue / (cardsInView * 0.5f);

        myGrid.cellSize = new Vector2(xCellValue, yCellValue);
        //myGrid.spacing = new Vector2(xCellValue / ((myGrid.constraintCount + 2) / 2), myGrid.spacing.y); 

        //myGrid.padding.top = (int)panelHeight / 7;

        topThreshold = panelRectTransform.position.y + panelHeight / 4; //if hovered card y is bigger then this value then the scroll has to move up
        botThreshold = panelRectTransform.position.y - panelHeight / 4; //if hovered card y is lower then this value then the scroll has to move down
    }

    private void Update()
    {
        currentHoveredCard = EventSystem.current.currentSelectedGameObject; // get the card hovered        

        #region CheckPanel

        //checks if the element selected is in this panel otherwise quit the update
        for (int i = 0; i < transform.childCount ; i++)
        {

            if(currentHoveredCard == transform.GetChild(i).GetChild(1).gameObject)
            {
                imInRightPanel = true;
                break;
            }
            else
            {
                imInRightPanel = false;
            }
        }
        if(!imInRightPanel) 
        {
            return;
        }
        #endregion

        // move till the position of the card is visible
        if (currentHoveredCard.transform.position.y < botThreshold)
        {
            IncreaseScrollbarValue(0.05f , scrollbar);
        }
        if (currentHoveredCard.transform.position.y > topThreshold)
        {
            DecreaseScrollbarValue(0.05f , scrollbar);
        }

        if (currentHoveredCard == prevHoveredCard)
        {
            return;
        }

        prevHoveredCard = currentHoveredCard; // to see if the selected card is changed
    }

    private void IncreaseScrollbarValue(float value , Scrollbar myScrollbar)
    {
        myScrollbar.value -= value;
    }

    private void DecreaseScrollbarValue(float value, Scrollbar myScrollbar )
    {
        myScrollbar.value += value;
    }
}
