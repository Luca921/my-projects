﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TalentTreePanelInfo : MonoBehaviour
{
    public enum PlayerStat { SkillPoints, Level }
    public PlayerStat playerStat;

    public Text t;
    public Text p;
    public Text playerP;
    public RectTransform tr;
    public float speed;
    public float offset;

    public GameObject skillPointsBox;

    bool right;
    bool left;
    float rightStickX;
    Vector3 startPos;

    private void OnDisable()
    {
        if (skillPointsBox != null)
        {
            skillPointsBox.SetActive(false);
        }        
    }
    private void OnEnable()
    {
        if (skillPointsBox != null)
        {
            skillPointsBox.SetActive(false);
        }
    }

    private void Awake()
    {
        GameManager.instance.OnSInfo += Display;
    }

    void Start()
    {
        if (playerStat == PlayerStat.SkillPoints)
        {
            playerP.text = GameManager.instance.player.skillPoints.ToString();
        }
        else
        {
            playerP.text = GameManager.instance.player.level.ToString();
        }

        startPos = tr.position;
    }

    private void Update()
    {
        rightStickX = Input.GetAxis("RStickX");

        left = (rightStickX < -.2f) ? true : false;
        right = (rightStickX > .2f) ? true : false;
        
        //movement of the panel right/left
        if (left && Mathf.Abs(Screen.width - tr.position.x) < tr.rect.xMax - offset)
        {
            tr.position += new Vector3(-speed, 0f, 0f);
        }

        if (right && tr.position.x < startPos.x)
        {
            tr.position += new Vector3(speed, 0f, 0f);
        }
    }

    private void OnDestroy()
    {
        GameManager.instance.OnSInfo -= Display;
    }

    public void Display(string s, int c)
    {
        t.text = s;

        if (playerStat == PlayerStat.SkillPoints)
        {
            if (p != null)
            {
                p.text = c.ToString();
            }

            playerP.text = GameManager.instance.player.skillPoints.ToString();
        }        
    }
}
