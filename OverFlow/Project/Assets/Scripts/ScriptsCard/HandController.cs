﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HandController : MonoBehaviour
{    

    public Button readyButton;    
    public GameObject cardSlot; // card slot prefab to instantiate
    public GameObject mulliganUI;
    public ChooseCardType chooseCardTypePopUp; // the script to decide the archetype to use

    public List<InGameSlot> gameSlots;

    [HideInInspector]
    public bool chipsetOpenedByInput = false;

    public InspectManagerUI inspectManager;
    public StatisticsManagerUI statisticsManager;
    public StatisticsManagerUI detailManager; // to notify the statistics panel that a card has been hovered and havs to update the values;

    public delegate void cardSelected(Card card);
    public event cardSelected thisCardSelected; //tells the listeners when a card has been chosen



    Navigation handNavigation;

    List<GameObject> cardsToRemove = new List<GameObject>();


    void Start()
    {
       
        // subscribes to hand events
        GameManager.instance.hand.OnCardAdded += CardIn;
        GameManager.instance.hand.OnCardRemoved += CardOut;

        EventSystem.current.firstSelectedGameObject = readyButton.gameObject;
    }



    // unsubscrbes from hand events
    private void OnDestroy()
    {
        if (GameManager.instance != null)
        {
            GameManager.instance.hand.OnCardAdded -= CardIn;

            GameManager.instance.hand.OnCardRemoved -= CardOut;
        }
    }

    private void Update()
    {
        if(chooseCardTypePopUp.gameObject.activeSelf && Input.GetButtonDown("Back"))
        {
            StopAllCoroutines();
            chooseCardTypePopUp.gameObject.SetActive(false);
        }

    }

    //instantiate all the card slots in the hand GUI and add a function on click
    public void CardIn(Card card)
    {
        
        GameObject go = Instantiate(cardSlot, gameObject.transform);
        HandCard thisHandCard = go.GetComponent<HandCard>();
        thisHandCard.thisCard = card;
        thisHandCard.myNotifyInspect += inspectManager.UpdateImage; // every card in the hand will notify the detail manager when highlighted for the first time
        thisHandCard.myNotifyInspect += statisticsManager.TestUIUpdateTemp;
       
        // go.GetComponent<HandCard>().

        foreach(Card.Archetypes arche in card.cardArchetypes)
        {
            foreach(InGameSlot slot in gameSlots)
            {
                if(arche == slot.archetype)
                {
                    go.GetComponent<HandCard>().linkedSlots.Add(slot);
                }
            }
        }

        cardsToRemove.Add(go);
        HandNavigation();
        go.name = card.name;
        Image img = go.transform.GetChild(1).GetComponent<Image>();
        img.sprite = card.artwork;
        Button b = go.GetComponentInChildren<MyButton>();
        b.onClick.AddListener(() => SendCard(card, go));
        EventSystem.current.SetSelectedGameObject(cardsToRemove[0].transform.GetChild(0).gameObject);
    }

    //removes the card from the hand and adds it to the ingame invenctory
    public void SendCard(Card c, GameObject g)
    {
        if (!chipsetOpenedByInput)
        {
            if (c.cardArchetypes.Count > 1)
            {
                //ask how to use the card
                StartCoroutine(ChoosingCard(c));
            }
            //ask the slots to insert the card
            else
            {
                //choses the right slot for the card
                if (thisCardSelected != null)
                {
                    thisCardSelected(c);
                }
            }
        }
    }

    // removes the card from the GUI
    public void CardOut(Card c)
    {        
        GameObject g = cardsToRemove.Find(GameObject => GameObject.name == c.name);
        cardsToRemove.Remove(g);
        Destroy(g);
        g.GetComponent<HandCard>().DeselectAll();
        HandNavigation();
        if (g.transform == transform.GetChild(0) && transform.childCount > 1)
        {
            EventSystem.current.SetSelectedGameObject(transform.GetChild(1).GetChild(0).gameObject);
        }
        else if(g.transform == transform.GetChild(0) && transform.childCount <= 1)
        {
            EventSystem.current.SetSelectedGameObject(readyButton.gameObject);
        }
        else if(g.transform != transform.GetChild(0))
        {
            EventSystem.current.SetSelectedGameObject(transform.GetChild(0).GetChild(0).gameObject);
        }
    }

    //shows the archetype (to choose) GUI 
    IEnumerator ChoosingCard(Card card)
    {
        chooseCardTypePopUp.gameObject.SetActive(true);

        chooseCardTypePopUp.DisplayArchetype(card);

        yield return new WaitUntil(() => chooseCardTypePopUp.done);

        if (thisCardSelected != null)
        {
            thisCardSelected(card);
        }

        chooseCardTypePopUp.done = false;
    }

    void HandNavigation()
    {
        for (int i = 0; i < cardsToRemove.Count; i++)
        {
            handNavigation = new Navigation();
            handNavigation.mode = Navigation.Mode.Explicit;

            handNavigation.selectOnDown = null;
            handNavigation.selectOnUp = readyButton;

            //left check
            if (i - 1 >= 0)
            {
                handNavigation.selectOnLeft = cardsToRemove[i - 1].transform.GetChild(0).GetComponent<MyButton>();
            }

            //right check
            if (i + 1 < cardsToRemove.Count)
            {
                handNavigation.selectOnRight = cardsToRemove[i + 1].transform.GetChild(0).GetComponent<MyButton>();
            }
            
            //checks if is the first element
            if (i == 0)
            {
                handNavigation.selectOnLeft = readyButton;
            }
            
            //checks the last element
            if (i == cardsToRemove.Count - 1)
            {
                handNavigation.selectOnRight = readyButton;
            }

            cardsToRemove[i].transform.GetChild(0).GetComponent<MyButton>().navigation = handNavigation;
        }

        handNavigation = new Navigation();
        handNavigation.mode = Navigation.Mode.Explicit;
        if (cardsToRemove.Count > 0)
        {
            handNavigation.selectOnRight = cardsToRemove[0].transform.GetChild(0).GetComponent<MyButton>();
            readyButton.GetComponent<MyButton>().navigation = handNavigation;
        }
    }
}
