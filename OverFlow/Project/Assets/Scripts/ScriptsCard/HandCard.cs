﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// review terribile da rifare
public class HandCard : MonoBehaviour
{
    public enum CardState {CanSelect , CannotSelect }
    public CardState myState;
    public GameObject myButton;

    public Card thisCard;

    public Sprite selected;
    //public Sprite notSelected;
    public Image imageToChange;

    public delegate void NotifySlots(List<Card.Archetypes> archetypes , bool condition);
    public NotifySlots myNotifySlot;

    public delegate void NotifyInspectPanel(Card card);
    public NotifyInspectPanel myNotifyInspect;

    public Sprite imageWhite;
    public Sprite imageGreen;

    public List<InGameSlot> linkedSlots;

    public bool triedToSetWhite = false;

    bool lastFrameNotHighlighted;
    bool firstNotHighLighted = true;
    bool firstHighlighted = false;
    bool lastFrameHighlighted;


    // Update is called once per frame
    void Update()
    {
        if(myState == CardState.CanSelect && myButton.GetComponent<MyButton>().Highlight())
        {


            if (lastFrameHighlighted)
            {
                firstHighlighted = false; ///////////
                //firstNotHighLighted = false;
            }
            else
            {
                firstHighlighted = true;
            }

            if (firstHighlighted)
            {
                myNotifyInspect(thisCard); // tells the detail panel that this is the inspected card;
            }


            imageToChange.sprite = selected;


            foreach (InGameSlot slot in linkedSlots)
            {
                slot.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = imageGreen;
                if (slot.archetype == Card.Archetypes.Consumable)
                {
                    slot.transform.GetChild(1).GetChild(0).GetComponent<Image>().sprite = imageGreen;
                }

            }
            triedToSetWhite = false;
            


        }
        else if(myState == CardState.CanSelect && !myButton.GetComponent<MyButton>().Highlight())
        {

            firstHighlighted = false;

            if (!lastFrameHighlighted)
            {
                firstNotHighLighted = false;
            }
            else
            {
                firstNotHighLighted = true;
            }

            imageToChange.sprite = null;

            foreach (InGameSlot slot in linkedSlots)
            {
                if (!triedToSetWhite)
                {
                    slot.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = imageWhite;
                    if (slot.archetype == Card.Archetypes.Consumable)
                    {
                        slot.transform.GetChild(1).GetChild(0).GetComponent<Image>().sprite = imageWhite;
                    }

                }
            }

            triedToSetWhite = true;
        }

      

        lastFrameHighlighted = myButton.GetComponent<MyButton>().Highlight();
        
    }


    public void DeselectAll()
    {
        foreach (InGameSlot slot in linkedSlots)
        {
            slot.transform.GetChild(0).transform.GetChild(0).GetComponent<Image>().sprite = imageWhite;
            if (slot.archetype == Card.Archetypes.Consumable)
            {
                slot.transform.GetChild(1).transform.GetChild(0).GetComponent<Image>().sprite = imageWhite;
            }

        }
    }

    public void RefreshStatistics()
    {
        myNotifyInspect(thisCard); // tells the detail panel that this is the inspected card;
    }
}
 