using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

// when the player dies, a percentage of his cards are taken and he can choose only one of two
public class DeathController : MonoBehaviour
{
    // the percentage of cards to be taken
    public float perc = 20f;

    // the pop-up panel which displays the two cards
    public ChooseCardToSave cardsToSavePopUp;

    CardInventory sendToInventory;
    List<Card> sendToDeck;

    // the list of cards to be chosen
    List<Card> cardsToChoose = new List<Card>();
    int cardsToSacrifice;

    // adds the card inventory component and subscribes to player's death event
    void Start()
    {
        sendToInventory = gameObject.AddComponent<CardInventory>();
        sendToInventory.cards = new List<Card>();
        sendToDeck = new List<Card>();

        GameManager.instance.player.OnPlayerDeath += PlayerDeath;
    }

    // unsubscribes from player's death event
    private void OnDestroy()
    {
        if (GameManager.instance != null)
        {
            GameManager.instance.player.OnPlayerDeath -= PlayerDeath;

            //GameManager.instance.inputManager.enabled = true;
            if (GameManager.instance.p != null)
            {
                //GameManager.instance.player.GetComponent<PlayerController>().ResetPlayer(); // reset player position and statistics
            }
        }
    }

    public void PlayerDeath()
    {
        // adds the card from tha hand, the graveyard and the deck to a list
        foreach (Card handCard in GameManager.instance.hand.cards)
        {
            sendToDeck.Add(handCard);
        }

        foreach (Card graveyardCard in GameManager.instance.graveyard.cards)
        {
            sendToDeck.Add(graveyardCard);
        }

        foreach (Card deckCard in GameManager.instance.deck.cards)
        {
            sendToDeck.Add(deckCard);
        }

        // send the cards collected during the mission to the hub inventory
        foreach (Card collectedCard in GameManager.instance.objectsInventory.cards)
        {
            GameManager.instance.temporary.AddCard(collectedCard);
        }

        foreach (Card inGameCard in GameManager.instance.inGameInventory.cards)
        {
            sendToDeck.Add(inGameCard);

            foreach (Trigger tr in inGameCard.triggers)
            {
                tr.effectUsed = false;
            }

            foreach (Effect effect in inGameCard.triggers[0].effects)
            {
                if (inGameCard.cardArchetypes[0] != Card.Archetypes.Consumable)
                {
                    effect.Discard();//we discard all the effects that are still in use
                }
            }
        }

        foreach (Skill s in GameManager.instance.EquippedSkills)
        {
            foreach (Effect effect in s.trigger.effects)
            {
                effect.Discard();
            }
        }

        // clear all the cards lists
        GameManager.instance.hand.cards = new List<Card>();
        GameManager.instance.graveyard.cards = new List<Card>();
        GameManager.instance.deck.cards = new List<Card>();
        GameManager.instance.inGameInventory.cards = new List<Card>();
        GameManager.instance.objectsInventory.cards = new List<Card>();

        // shuffles the total cards and takes a certain percentage of them
        sendToInventory.Shuffle();
        cardsToSacrifice = (int)(sendToDeck.Count * (perc / 100f));

        // the number of cards taken must be even because the player has to choose between two
        if (cardsToSacrifice % 2 != 0)
        {
            cardsToSacrifice-- ;
        }

        // add the cards to be chosen to a temporary list
        for (int i = 0; i < cardsToSacrifice; i++)
        {
            cardsToChoose.Add(sendToDeck[0]);
            sendToDeck.Remove(sendToDeck[0]);
        }

        // the rest of the cards is sent to the hub inventory
        foreach (Card c in sendToDeck)
        {
            SendToHubDeck(c);
        }

        StartCoroutine(ChooseCards());



        // empties the list
        sendToDeck = new List<Card>();
    }

    //sends the cards to the inventory in the HUB
    public void SendToHubInventory(Card card)
    {
        GameManager.instance.inventory.AddCard(card);
    }

    //sends the cards to the deck in the HUB
    public void SendToHubDeck(Card card)
    {
        GameManager.instance.deck.AddCard(card);
    }

    // displays two cards and the player must choose which one will be saved and which one will be definitely removed
    // once his decision is done, it will continue until the list is empty
    // then load a new scene and bring the player back to the hub
    IEnumerator ChooseCards()
    {
        while(cardsToChoose.Count != 0)
        {
            cardsToSavePopUp.gameObject.SetActive(true);
            cardsToSavePopUp.DisplayCards(cardsToChoose[0], cardsToChoose[1]);

            yield return StartCoroutine(MakeYourChoice());

            cardsToChoose.Remove(cardsToChoose[0]);
        }


        SceneManager.LoadScene("DungeonToHub");
        //Provvisorio//

        //////////////
        //GameManager.instance.playerController.EnterHub(GameManager.instance.transform.position);


    }

    IEnumerator MakeYourChoice()
    {
        yield return new WaitUntil(() => cardsToSavePopUp.done);

        Image img;

        if(cardsToSavePopUp.num == 0)
        {
            img = cardsToSavePopUp.button2.transform.GetChild(1).GetComponent<Image>();
        }
        else
        {
            img = cardsToSavePopUp.button1.transform.GetChild(1).GetComponent<Image>();
        }

        yield return StartCoroutine(DissolveFx(img));

        GameManager.instance.deck.AddCard(cardsToChoose[cardsToSavePopUp.num]);

        cardsToChoose.Remove(cardsToChoose[cardsToSavePopUp.num]);

        cardsToSavePopUp.done = false;
    }

    public IEnumerator DissolveFx(Image img)
    {
        Coffee.UIExtensions.UIDissolve dissolve = img.gameObject.GetComponent<Coffee.UIExtensions.UIDissolve>();

        dissolve.enabled = true;        

        yield return new WaitForSeconds(dissolve.duration + 0.1f);

        //dissolve.enabled = false;
        //dissolve.play = true;
        
        //dissolve.effectFactor = 0f;
    }
}
