﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

//this script is going to be placed on every statistic button
public class StatisticButtonBehaviour : MonoBehaviour
{

    public StatsManagerHub statsManager; //the monobehaviour that manages the statistics

    public ScalingTextBehaviour scalingTextBehaviour;
    public AbilityTextBehaviour abilityTextBehaviour;

    public Material increasedPointsMaterial;
    public Material startingPointsMaterial;
    public Material relatedBabMaterial;
    public Material relatedTextDefaultMaterial;

    public GameObject leftArrow;
    public GameObject rightArrow;

    public delegate void OnStatUpdated(Stat stat , int value );
    public event OnStatUpdated updateStats; //every time a stat gets updated an event is launched (stats updated , +-1)
    public event OnStatUpdated updateScalings;

    public Stat myStat; // the stat this monobehaviour is going to change

    int myValue; // the value +1 , -1 thats get added ;

    int maxPoints;

    int pointsGiven; //actually the points we put to the statistic 

    bool left, right , canInput = true;

    float trigger, oldTrigger;

    float myStatValue; //keeps track of the current value ( + the player base stat );

    public TextMeshProUGUI myValueText;
    public TextMeshProUGUI[] myRelatedTexts;

    ArrowStatisticsBehaviour rightArrowBe, leftArrowBe;

    public int cap;

    MyButton myButton;

    CharacterStatistics playerStats;


    void Start()
    {

        rightArrowBe = rightArrow.GetComponent<ArrowStatisticsBehaviour>();
        leftArrowBe = leftArrow.GetComponent<ArrowStatisticsBehaviour>();


        myButton = GetComponent<MyButton>();
        maxPoints = playerStats.attributePoints; //track the max manageable points the player can use

        //forces the statmanager to subscribe this event
        //update values gets called when the event is called
        updateStats += statsManager.UpdateValues;
        updateStats += statsManager.UpdatePointsLeft; //the "stat" is nt used the "value" increase/decrease the counter
        updateScalings += scalingTextBehaviour.UpdateMyText;
        updateScalings += abilityTextBehaviour.RefreshValues;
    }

    private void OnEnable()
    {
        playerStats = GameManager.instance.player;

        InitValue();

        pointsGiven = 0;
        if (playerStats != null)
        {
            maxPoints = playerStats.attributePoints; //track the max manageable points the player can use
        }

        myValueText.fontMaterial = startingPointsMaterial;
    }

    void Update()
    {

        if (myButton.Highlight())
        {
            if (pointsGiven == 0)
            {
                leftArrow.SetActive(false);
            }
            else
            {
                leftArrow.SetActive(true);
            }

            if (pointsGiven < maxPoints && statsManager.PointsLeft > 0)
            {
                rightArrow.SetActive(true);
            }
            else
            {
                rightArrow.SetActive(false);
            }

            foreach (TextMeshProUGUI text in myRelatedTexts)
            {
                text.fontMaterial = relatedBabMaterial;
            }
        }
        else
        {
            leftArrow.SetActive(false);
            rightArrow.SetActive(false);

            foreach (TextMeshProUGUI text in myRelatedTexts)
            {
                text.fontMaterial = relatedTextDefaultMaterial;
            }
        }

        #region AxisCheck

        trigger = Input.GetAxisRaw("Horizontal");

        if (Input.GetAxisRaw("Horizontal") > 0 && canInput)
        {
            canInput = false;
            right = true;
            left = false;
            StartCoroutine(ResetInput());
        }

        if (Input.GetAxisRaw("Horizontal") < 0 && canInput)
        {
            canInput = false;
            left = true;
            right = false;
            StartCoroutine(ResetInput());
        }

        if (trigger == oldTrigger)
        {
            left = false;
            right = false;
        }

        oldTrigger = trigger;

        #endregion

        //if is "right" and player still has points
        if (right && pointsGiven < maxPoints && myButton.Highlight())
        {


            

            if (statsManager.PointsLeft > 0 && myStatValue < cap-1)
            {
                rightArrowBe.startAnimation();
                myValue = 1; // set the value to add to +1
                pointsGiven++; // says the points given on this stat
                myStatValue += myValue; //full value of the button stat ( + player base value )
                myValueText.text = myStatValue.ToString();

                myValueText.fontMaterial = increasedPointsMaterial; //set the material when a stat gets updated


                updateStats(myStat, myValue);
                updateScalings(myStat, (int)myStatValue);
            }
        }
        
        //checks if at the value we are going to subtract we added at least 1 point before
        if (left && pointsGiven > 0 && myButton.Highlight())
        {
            


            if (statsManager.PointsLeft < maxPoints)
            {
                leftArrowBe.startAnimation();
                myValue = -1; // set the value to add to -1
                pointsGiven--; // says the points given on this stat
                myStatValue += myValue; //full value of the button stat ( + player base value )
                myValueText.text = myStatValue.ToString();

                if (pointsGiven == 0)
                { 
                    myValueText.fontMaterial = startingPointsMaterial; //change the material if no points are given to this stat
                }

                updateStats(myStat, myValue);
                updateScalings(myStat, (int)myStatValue);
            }
        }

        
    }

    //initialize the value text based on the player statistics
    void InitValue()
    {
        switch (myStat)
        {
            case Stat.strength:
                myStatValue = playerStats.baseStrength;
                break;

            case Stat.dexterity:
                myStatValue = playerStats.baseAgility;
                break;

            case Stat.tech:
                myStatValue = playerStats.baseIntellect;
                break;

            case Stat.neuralHealth:
                myStatValue = playerStats.baseNeuralHealth;
                break;

            case Stat.neuralResistance:
                myStatValue = playerStats.baseNeuralResistence;
                break;

            case Stat.luck:
                myStatValue = playerStats.baseLuck;
                break;
        }
        myValueText.text = myStatValue.ToString();


    }

    IEnumerator ResetInput()
    {
        yield return new WaitForSeconds(0.1f);
        canInput = true;
    }
}