﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebuffUIManager : MonoBehaviour
{

    Transform entityTransform;

    public Sprite strDebuffFull; //strenght Icon

    public Sprite intDebuffFull; //int Icon

    public Sprite dexDebuffFull; //dexterity Icon

    public Sprite nRDebuffFull; //neural resistance Icon

    public Sprite luckDebuffFull; //luck Icon

    public Sprite velocityDebuffFull; //speed Icon
  
    public Sprite nHDebuffFull; //neural health Icon


    public GameObject debuffPrefab; // general debuff prefab , the image needs to be changed based on DEBUFF

    public GameObject buffPrefab; // general buff prefab , the image needs to be changed based on BUFF

    public GameObject leakDebuffFull;

    public GameObject freezeState;

    public GameObject lockState;

    public GameObject jamState;

    public CharacterStatistics linkedEntity; //assigned to the enemy in the editor  (if == null means u are the player)


    private void OnDestroy()
    {

        if (linkedEntity != null)
        {
            linkedEntity.gotDebuff -= ShowDebuffGUI;
            if (linkedEntity.GetComponent<PlayerController>() != null)
            {
                linkedEntity.GetComponent<PlayerController>().disableStateUpdated -= ShowStateGUIPlayer;
            }
            if(linkedEntity.GetComponent<EnemyController>() != null)
            {
                linkedEntity.GetComponent<EnemyController>().disableStateUpdated -= ShowStateGUI;
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {

        //if(linkedEntity != null )
        //{
        if (linkedEntity.identity == CharacterStatistics.Identity.Enemy)
        {
            linkedEntity.GetComponent<EnemyController>().disableStateUpdated += ShowStateGUI;
        }
        else
        {
            linkedEntity.GetComponent<PlayerController>().disableStateUpdated += ShowStateGUIPlayer;
        }
        //}
        //if(linkedEntity == null)
        //{
        //    linkedEntity = GameManager.instance.player; //gets the player
        //    linkedEntity.GetComponent<PlayerController>().disableStateUpdated += ShowStateGUIPlayer;

        //}
        
        linkedEntity.gotDebuff += ShowDebuffGUI;

        entityTransform = linkedEntity.GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        if(entityTransform.localScale.x == -1 )
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
        else
        {
            transform.localScale = new Vector3(1, 1, 1);

        }
    }



    //when u get a debuff
    public void ShowDebuffGUI(BuffDebuff debuff)  // function that instantiate the right DEBUFF ICON
    {
        GameObject go = new GameObject();

        //instantiate the leak prefab 
        if (debuff.stat == BuffDebuff.Stat.Leak)
        {
            go = Instantiate(leakDebuffFull, transform);
            
        }
        else
        {   
            //else checks if it is a buff or debuff
            if (debuff.statInstantMod >= 0)
            {
                go = Instantiate(buffPrefab, transform);
            }
            if (debuff.statInstantMod < 0)
            {
                go = Instantiate(debuffPrefab, transform);
            }
           
            go.transform.GetChild(0).GetComponent<Image>().sprite = CheckType(debuff.stat); // calls the function that return the right sprite to associate
        }

        go.GetComponent<SingleDebuffGUI>().duration = debuff.totDuration;

    }

    //when u get a state change
    public void ShowStateGUI(EnemyController.DisabledState state, bool onOff , float stunTime) // function that instantiate the right STATE ICON
    {
        if(onOff)
        {
             GameObject go = Instantiate(CheckState(state), transform);
             go.GetComponent<SingleDebuffGUI>().duration = stunTime;
        }
    }

    public void ShowStateGUIPlayer(EnemyController.DisabledState state , float time)
    {
        GameObject go = Instantiate(CheckState(state), transform);
        go.GetComponent<SingleDebuffGUI>().duration = time;
    }

    //decides the gameobject to instantiate
    public Sprite CheckType(BuffDebuff.Stat stat)
    {
        switch (stat)
        {
            case BuffDebuff.Stat.Strength:

                return strDebuffFull;
                
            case BuffDebuff.Stat.Intellect:

                return intDebuffFull;

            case BuffDebuff.Stat.Agility:

                return dexDebuffFull;

            case BuffDebuff.Stat.NeuralResistance:

                return nRDebuffFull;

            case BuffDebuff.Stat.Luck:
 
                return luckDebuffFull;
 
            case BuffDebuff.Stat.Velocity:

                return velocityDebuffFull;

            case BuffDebuff.Stat.NeuralHealth:

                return nHDebuffFull;

            default: return null;
        }
    }

    //checks the right object to instantiate for the state
    public GameObject CheckState(EnemyController.DisabledState state)
    {
        switch (state)
        {
            case EnemyController.DisabledState.Freeze:

                return freezeState;

            case EnemyController.DisabledState.Lock:

                return lockState;

            case EnemyController.DisabledState.Jam:

                return jamState;
        }
        GameObject go = new GameObject();
        return go;
    }
}