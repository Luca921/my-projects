﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Coffee.UIExtensions;
using UnityEngine.UI;

public class ParticleScaler : MonoBehaviour
{
    ParticleSystem ps;
    Vector3 fxScale;

    private void Awake()
    {
        GetComponent<UIParticle>().enabled = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        ps = GetComponentInChildren<ParticleSystem>();
        fxScale = ps.shape.scale;

        
    }

    private void OnRectTransformDimensionsChange()
    {
        Vector2 size = GetComponent<RectTransform>().rect.size;

        Vector3 newScale = new Vector3(fxScale.x * size.x / 506f, fxScale.y * size.y / 720f, 1f);
        var p = ps.shape;
        p.scale = newScale;
    }
}
