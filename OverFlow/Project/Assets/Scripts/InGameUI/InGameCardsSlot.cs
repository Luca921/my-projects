﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InGameCardsSlot : MonoBehaviour
{
    public Card.Archetypes archetype;

    public Sprite emptySlotImage;

    public Image image;

    public TextMeshProUGUI text;

    public Trigger.Triggers trigger;

    // Start is called before the first frame update
    void Start()
    {
        GameManager.instance.inGameInventory.OnCardAdded += UpdateCard;
        GameManager.instance.inGameInventory.OnCardRemoved += UpdateCard;
        GameManager.instance.triggerManager.OnCardUsed += UpdateCard;
    }

    private void OnDestroy()
    {
        GameManager.instance.inGameInventory.OnCardAdded -= UpdateCard;
        GameManager.instance.inGameInventory.OnCardRemoved -= UpdateCard;
        GameManager.instance.triggerManager.OnCardUsed -= UpdateCard;
    }

    public void UpdateCard(Card card)
    {
        if (card.cardArchetypes[0] == archetype)
        {           
            if (card.cardLife == 0)
            {
                if (archetype == Card.Archetypes.Consumable)
                {
                    if (trigger == card.triggers[0].trigger)
                    {
                        StartCoroutine(DissolveFx());

                        text.text = "0/0";
                    }
                }
                else
                {
                    StartCoroutine(DissolveFx());

                    text.text = "0/0";
                }              
            }
            else
            {
                if (archetype == Card.Archetypes.Consumable)
                {
                    if (trigger == card.triggers[0].trigger)
                    {
                        image.sprite = card.artwork;

                        text.text = card.cardLife + "/" + card.triggers[0].life;
                    }                    
                }
                else
                {
                    image.sprite = card.artwork;

                    text.text = card.cardLife + "/" + card.triggers[0].life;
                }                
            }            
        }
    }

    public IEnumerator DissolveFx()
    {
        Coffee.UIExtensions.UIDissolve dissolve = image.gameObject.GetComponent<Coffee.UIExtensions.UIDissolve>();

        if (image.sprite != emptySlotImage)
        {
            dissolve.enabled = true;
        }

        yield return new WaitForSeconds(dissolve.duration + 0.1f);

        dissolve.enabled = false;
        dissolve.play = true;

        image.sprite = emptySlotImage;

        //dissolve.effectFactor = 0f;
    }
}
