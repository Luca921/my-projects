﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
    CharacterStatistics player;
    public TextMeshProUGUI[] levelUpStatistics;
    public Image healthBar;
    public Image experienceBar;
    public TextMeshProUGUI levelText;
    public float statisticPanelTime;
    GameObject statisticsPanel;
    float health;
    float experience;
    int level;

    float lastFrameHealth = 0f;
    public GameObject healthFx;

    // Start is called before the first frame update
    void Start()
    {
        player = GameManager.instance.player;
        statisticsPanel = levelUpStatistics[0].rectTransform.parent.gameObject;

        level = player.level;
        levelText.text = level.ToString();

        player.playerLevelUp += LevelUp;

        lastFrameHealth = player.currentHealth;
    }

    void Update()
    {

        if (player.currentHealth > lastFrameHealth)
        {
            GameObject h = Instantiate(healthFx, player.transform);
            Destroy(h, 2f);
        }

        lastFrameHealth = player.currentHealth;


        //health manager UI
        health = player.currentHealth;
        healthBar.fillAmount = Mathf.Clamp(health * 1 / player.hpValues[(int)(player.baseNeuralHealth + player.bonusNeuralHealth) - 1], 0, 1);

        //experience manager UI
        experience = player.exp;
        experienceBar.fillAmount = Mathf.Clamp(experience * 1 / player.levelValues[Mathf.Clamp(player.level, 0, player.levelValues.Length -1)].pointsNeeded, 0, 1);

    }

    //what happens when player level up
    void LevelUp(int levelUp)
    {
        levelText.text = (levelUp+1).ToString();
        UpdateStatistics(levelUp);
        statisticsPanel.SetActive(true);
        StartCoroutine(TurnOfStatPanel());        
    }

    private void OnDestroy()
    {
        player.playerLevelUp -= LevelUp;
    }

    private void UpdateStatistics(int levelUp)
    {
        float x = player.levelValues[levelUp].increaseStrenght;
        float y = player.levelValues[levelUp].increaseAgility;
        float w = player.levelValues[levelUp].increaseIntellect;
        float z = player.levelValues[levelUp].increaseNeuralHealth;
        float k = player.levelValues[levelUp].increaseNeuralResistence;
        float j = player.levelValues[levelUp].increaseLuck;
        levelUpStatistics[1].text = ("Strength + " + x + "\n" +
                                     "Dexterity + " + y + "\n" + 
                                     "Tech + " + w + "\n" + 
                                     "Neural Health + " + z + "\n" + 
                                     "Neural Resistance + "  + k + "\n" + 
                                     "Luck + " + j );
       
    }

    IEnumerator TurnOfStatPanel()
    {
        yield return new WaitForSeconds(statisticPanelTime);
        statisticsPanel.SetActive(false);
    }


}
