﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterHealthUI : MonoBehaviour
{
    public CharacterStatistics stats; //the reference to the stats 

    [HideInInspector]
    public float health;

    public Transform bar; // reference to the effective lifeBar

    Transform target; // character she has to follow
    float yOffSet; // y offset from the character to follow

    private void Start()
    {
        yOffSet = transform.localPosition.y;
        target = transform.parent;
        transform.parent = null;
    }

    void Update()
    {
        if(target == null)
        {
            Destroy(gameObject);
            return;
        }
        
        //The lifebar can't have negative values
        if (stats.currentHealth < 0f)
        {
            health = 0f;
        }
        else
        {
            health = stats.currentHealth;
        }

        //scales the greenBar to give the feeling that the life is going down/up
        bar.localScale = new Vector3 ( health * 1 / stats.hpValues[(int)(stats.baseNeuralHealth + stats.bonusNeuralHealth)-1] , 1 , 1);

        //follows the target 
        transform.position = target.position + new Vector3(0, yOffSet, 0);

    }
}
