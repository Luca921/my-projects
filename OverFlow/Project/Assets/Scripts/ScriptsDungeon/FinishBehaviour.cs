﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FinishBehaviour : MonoBehaviour
{
    GameManager gm;

    bool isInside;
    public GameObject inputImage;
    public GameObject[] screens;

    private void Start()
    {
        gm = GameManager.instance;
        gm.OnExitDungeon += UseTerminal;
    }

    private void OnDestroy()
    {
        gm.OnExitDungeon -= UseTerminal;
    }

    //trigger at the end of the dungeon to get back to the HUB
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isInside = true;
            foreach(var go in screens)
            {
                go.SetActive(true);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isInside = false;
            foreach (var go in screens)
            {
                go.SetActive(false);
            }
        }
    }

    private void Update()
    {
        inputImage.SetActive(isInside);

        if (isInside && Input.GetButtonDown("Interact"))
        {
            FMODManager.instance.StopPlayerLowHp();
            gm.MissionCompleteCall();
            gm.MissionCompleted();
        }
    }
    
    public void UseTerminal()
    {
        //gm.MissionCompleted();

        foreach (Card handCard in gm.hand.cards)
        {
            SendToDeck(handCard);//send to the deck the cards we have in hand   
        }

        foreach (Card graveyardCard in gm.graveyard.cards)
        {
            SendToDeck(graveyardCard);//send to the deck the cards we have in the graveyard
        }

        foreach (Card inGameCard in gm.inGameInventory.cards)
        {
            foreach (Trigger tr in inGameCard.triggers)
            {
                tr.effectUsed = false;
            }

            foreach (Effect effect in inGameCard.triggers[0].effects)
            {
                if (inGameCard.cardArchetypes[0] != Card.Archetypes.Consumable)
                {
                    effect.Discard();//we discard all the effects that are still in use
                }
            }

            SendToDeck(inGameCard);//we send to the deck every card we have equipped
        }

        if (gm.objectsInventory.cards.Count > 0)
        {
            gm.newCards = true;
        }

        foreach (Card collectedCard in gm.objectsInventory.cards)
        {
            gm.temporary.AddCard(collectedCard); //we send to the hub inventory every card we collected
        }

        foreach (Skill s in gm.EquippedSkills)
        {
            foreach (Effect effect in s.trigger.effects)
            {
                effect.Discard(); // we discard every ability we have equipped
            }
        }

        //clean up the list of cards we need in the dungeon
        gm.hand.cards = new List<Card>();
        gm.graveyard.cards = new List<Card>();
        gm.inGameInventory.cards = new List<Card>();
        gm.objectsInventory.cards = new List<Card>();

        ////Check game completion and save all the data
        //gm.CheckGameCompletion();
        //DataManager.instance.SaveData();
        //gm.newMission = true;
        //StartCoroutine(DataManager.instance.SavingFeedback());

        
        gm.player.ResetStatistics(); // reset the player statistics
                
        SceneManager.LoadScene("DungeonToHub");
    }

    public void SendToDeck(Card card)
    {
        gm.deck.AddCard(card);
    }
    public void SendToHubInventory(Card card)
    {
        gm.inventory.AddCard(card);
    }
}
