﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : MonoBehaviour
{
    public RoomBehaviour room;
    
    public List<Card> dropList;
    public GameObject dropPrefab;

    public GameObject inputImage;

    public SpriteRenderer cardImage;

    public GameObject chestModel;
    public GameObject openingFx;

    bool isInside;

    public ChestState chestState;
    public enum ChestState
    {
        Sealed,
        Closed,
        Open
    }

    protected virtual void Start()
    {
        if (chestState == ChestState.Sealed)
        {
            room.currentRoomCleared += RemoveSeal;
        }

        if(dropList.Count > 0)
        {
            cardImage.sprite = dropList[0].artwork;
        }
        
    }

    private void OnDestroy()
    {
        if (room != null)
        {
            room.currentRoomCleared -= RemoveSeal;
        }
    }

    public void RemoveSeal()
    {
        chestState = ChestState.Closed;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isInside = true;

            if (chestState == ChestState.Closed)
            {
                inputImage.SetActive(true);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isInside = false;

            if (chestState == ChestState.Closed)
            {
                inputImage.SetActive(false);
            }
        }
    }
    
    private void Update()
    {
        if (chestState == ChestState.Closed && isInside && Input.GetButtonDown("Interact"))
        {
            FMODManager.instance.PlayMenuBack();

            StartCoroutine(CoChest());

            chestState = ChestState.Open;
        }
    }

    public IEnumerator CoChest()
    {
        GameObject fx = Instantiate(openingFx, new Vector3(chestModel.transform.position.x, chestModel.transform.position.y + 0.65f, chestModel.transform.position.z - 0.5f), Quaternion.identity);

        yield return new WaitForSeconds(1f);

        cardImage.gameObject.SetActive(true);

        foreach (Card card in dropList)
        {
            GameObject drop = Instantiate(dropPrefab, transform.position + new Vector3( 0 , 0.65f , 0 ), Quaternion.identity);
            // Set on drop object the type of card it is
            drop.GetComponent<DropBehaviour>().drop = card;
        }

        chestModel.transform.GetChild(1).gameObject.SetActive(false);
        chestModel.transform.GetChild(2).gameObject.SetActive(true);

        inputImage.SetActive(false);
    }
}
