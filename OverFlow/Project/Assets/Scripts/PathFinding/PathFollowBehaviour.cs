﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFollowBehaviour : MonoBehaviour
{
    public float weight = 1f;
    public float Weight { get { return weight; } }

    public Enemy enemy;
    public bool canFollow = true;

    public void ResetState()
    {
       
        canFollow = true;
        currentNodeIndex = 0;
    }

    public List<Transform> path;
    public int currentNodeIndex = 0;
    public Transform currentTarget;
    public float arriveRadius;

    public AStar pathfindingAlgorithm;
    
        
    public void PathFollow() //GetSteering
    {
        if (!pathfindingAlgorithm.isCalculatingPath)
        {
            if (pathfindingAlgorithm.foundPathTransforms.Count > 0)
            {
                path = pathfindingAlgorithm.foundPathTransforms;
            }

            if (path == null || path.Count == 0) return;
            if (currentNodeIndex >= path.Count)
            {
                float distance = (enemy.player.transform.position.x - transform.position.x);
                float direction = Mathf.Sign(distance);

                //rotate the enemy
                transform.localScale = new Vector3(direction, transform.localScale.y, transform.localScale.z);

                //moves it
                Vector3 enemyMovement = transform.right * (direction * enemy.speed * Time.deltaTime);
                enemy.rb.MovePosition(enemy.rb.position + enemyMovement);                

                return;

            }

            currentTarget = path[currentNodeIndex];
            //if enemy is on floor and close enought to the player
            if ((transform.position - currentTarget.position).sqrMagnitude < arriveRadius * arriveRadius && enemy.onFloor)
            {
                //if there is at least one more node after the one we are on
                if (currentNodeIndex < path.Count-1)
                {
                    //if the next node is higher the ours
                    if((path[currentNodeIndex+1].transform.position.y - path[currentNodeIndex].transform.position.y) > 2f)
                    {
                        enemy.canJump = true;
                    }
                }
                currentNodeIndex++;                
            }
            //otherwise he just move
            else
            {
                MoveOnPath();
            }
        }
    }

    protected virtual void MoveOnPath()
    {
        //jump behaviour
        if (enemy.onFloor && enemy.canJump)
        {
            enemy.rb.AddForce(Vector3.up * 200f, ForceMode.Impulse);
            enemy.canJump = false;
            enemy.onFloor = false;
        }

        float distance = (currentTarget.transform.position.x - transform.position.x);
        float direction = Mathf.Sign(distance);

        transform.localScale = new Vector3(direction, transform.localScale.y, transform.localScale.z);

        Vector3 enemyMovement = transform.right * (direction * enemy.speed * Time.deltaTime);
        enemy.rb.MovePosition(enemy.rb.position + enemyMovement);
    }
}


