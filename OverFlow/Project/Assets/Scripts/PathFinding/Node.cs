﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class Node : MonoBehaviour
{
    public Color col = Color.white;

    public List<Node> nodes = new List<Node>();
    public GraphMaker graphMaker;

    public SpriteRenderer sr;

    // A* state
    public float cost;
    public float h;
        
    private void Update()
    {
        sr.color = col;
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Handles.Label(transform.position, cost.ToString());
    }
#endif

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 10) //Enemy layer
        {
            if (other.GetComponent<Enemy>().startPathNode != null)
            {
                other.GetComponent<Enemy>().startPathNode = this;
            }
        }
        else if (other.gameObject.layer == 11 || other.gameObject.layer == 13) //Player and PlayerDash layers
        {
            other.GetComponent<CharacterStatistics>().playerNode = this;
        }
    }
}
