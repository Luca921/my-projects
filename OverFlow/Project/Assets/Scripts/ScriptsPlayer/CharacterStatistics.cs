﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CharacterStatistics : MonoBehaviour
{
    public enum Identity { Enemy, Player }
    public Identity identity;

    public GameObject debuffPrefab;

    #region Buff
    //spear / hammer scaling
    public class ActiveBuffDebuff
    {
        public BuffDebuff buffDebuff;
        float activeTime;
        float totalTime;

        bool SFXDebuff = true;

        public ActiveBuffDebuff (BuffDebuff b)
        {
            buffDebuff = b;
            activeTime = 0f; //how long the debuff has been active
            totalTime = 0f; //total duration of debuff
        }

        public bool Update(CharacterStatistics characterStatistics)
        {
            if (characterStatistics.currentHealth > 0)
            {
                //Leak , recursive debuff every debuff.time time
                if (buffDebuff.time > 0f)
                {
                    activeTime += Time.deltaTime;

                    //reuse the debuff
                    if (activeTime > buffDebuff.time)
                    {
                        characterStatistics.ModifyStat(buffDebuff, buffDebuff.ricorrente);
                        FMODManager.instance.PlayPlayerDamageDebuff();
                        activeTime -= buffDebuff.time;
                    }
                }

                totalTime += Time.deltaTime;

                //checks the general time of the debuff , return false when it expires
                if (totalTime > buffDebuff.totDuration)
                {
                    return false;
                }

                return true;
            }
            characterStatistics.RemoveAllDebuffs(); // clears the list of debuffs
            return false;
        }
    }

    public List<ActiveBuffDebuff> activeBuffDebuffs = new List<ActiveBuffDebuff>();

    public float debuffTimeIncreaser = 0f;

    //add the debuff to the debuff list of the character statistics
    public void DebuffAdd(BuffDebuff buff)
    {
        buff.totDuration += (buff.totDuration / 100f) * debuffTimeIncreaser; //////////////////////////////////////////////////////////////
        gotDebuff?.Invoke(buff); //calls the event , the related GUI will instantiate the right image on her horizontal layout
        activeBuffDebuffs.Add(new ActiveBuffDebuff(buff));
        ModifyStat(buff, buff.statInstantMod); // instant application of the buf/debuff
    }

    public float Delta(float bb, float mod)
    {
        float unclampedStat = bb;
        bb = Mathf.Clamp(bb + mod, 1f, 99f);
        return (bb - unclampedStat);
    }

    //checks the stat to modify
    private void ModifyStat(BuffDebuff b, float value)
    {
        switch (b.stat)
        {
            case (BuffDebuff.Stat.Agility):

                bonusAgility += b.statInstantMod = Delta(baseAgility + bonusAgility, value);               

                break;

            case (BuffDebuff.Stat.Strength):

                bonusStrength += b.statInstantMod = Delta(baseStrength + bonusStrength, value);

                break;

            case (BuffDebuff.Stat.Intellect):

                bonusIntellect += b.statInstantMod = Delta(baseIntellect + bonusIntellect, value);                

                break;

            case (BuffDebuff.Stat.NeuralResistance):

                bonusNeuralResistence += b.statInstantMod = Delta(baseNeuralResistence + bonusNeuralResistence, value);                

                break;

            case (BuffDebuff.Stat.Luck):

                bonusLuck += b.statInstantMod = Delta(baseLuck + bonusLuck, value);                

                break;

            case (BuffDebuff.Stat.NeuralHealth):

                bonusNeuralHealth += b.statInstantMod = Delta(baseNeuralHealth + bonusNeuralHealth, value);                

                break;

            case (BuffDebuff.Stat.Velocity):

                PlayerController player = GameManager.instance.p.GetComponent<PlayerController>();

                player.speed = player.modifiedSpeed += b.statInstantMod = Delta(player.modifiedSpeed, value);                

                break;

            case (BuffDebuff.Stat.Leak):
                //currentHealth -= (hpValues[(int)(baseNeuralHealth + bonusNeuralHealth)] / 100f) * value;               
                if (currentHealth > 0f)
                {
                    float dmg = (hpValues[(int)(baseNeuralHealth + bonusNeuralHealth)] / 100f) * value;

                    switch (identity)
                    {
                        case Identity.Enemy:
                            EnemyController en = GetComponent<EnemyController>();
                            en.TakeDamage(dmg, true);
                            break;

                        case Identity.Player:
                            PlayerController pl = GetComponent<PlayerController>();
                            pl.TakeDamage(dmg, false);
                            break;
                    }
                }                

                break;
        }
    }
    #endregion

    #region Scaling
    [System.Serializable]
    public struct BabScaling
    {
        public float Bab;
        public float scaling;
    }

    //katana scaling and all the stuffs u get when u level up
    [System.Serializable]
    public struct LevelScaling
    {
        public float pointsNeeded;
        public int increaseStrenght;
        public int increaseIntellect;
        public int increaseAgility;
        public int increaseLuck;
        public int increaseNeuralHealth;
        public int increaseNeuralResistence;
        public int increaseStatsManageable;
        public int increaseAbilityPoints;
        public float katanaScaling;
    }

    //scaling of the specific abilities with tech
    [System.Serializable]
    public struct TechScaling
    {
        public float shieldDuration;
        public float projectileDamage;
        public float shockwaveRange;
        public float blackoutDuration;
        public float companionDuration;
        public float shockwaveDamage;
        public float companionDamage;
    }

    #endregion

    public delegate void PlayerDeath();
    public event PlayerDeath OnPlayerDeath; //called when we die

    public delegate void DebuffApplied(BuffDebuff buff);
    public event DebuffApplied gotDebuff;

    public delegate void LevelUp(int level);
    public event LevelUp playerLevelUp;

    [Space(10), Header("ATTRIBUTES")]

    public float baseStrength;
    public float baseIntellect;
    public float baseAgility;
    public float baseNeuralHealth;//energy and hp
    public float baseLuck;
    public float baseNeuralResistence;
    public float baseAbility;
    public float baseDefense;
    public float luckIncreasePerLevel;

    //[HideInInspector]
    public int level;
    [HideInInspector]
    public float exp;

    public Node playerNode;

    [Space(10), Header("ALPHA HIT VFX REFERENCE")]
    public SkinnedMeshRenderer bodyMesh;
    public Material bodyMaterial;
    public Material hitMaterial;
    public Material stunMaterial;

    #region hidden variables
    //[HideInInspector]
    public float currentHealth; //the HP the character has
    //[HideInInspector]
    public float bonusStrength;
    [HideInInspector]
    public float bonusIntellect;
    [HideInInspector]
    public float bonusAgility;
    //[HideInInspector]
    public float bonusAbility;
    [HideInInspector]
    public float bonusNeuralHealth;
    [HideInInspector]
    public float bonusLuck;
    //how much the dmg will be reduced
    [HideInInspector]
    public float bonusNeuralResistence;

    public int skillPoints = 2; // points to spend in skills
    public int attributePoints = 0; // point to spend in attributes
    [HideInInspector]
    public float totalHp;

    public GameObject healthFx;

    [Space(10), Header("VALUES PARAMETERS")]
    public BabScaling[] agilityStrenghtValues; //damages % increasing and scaling
    public BabScaling[] techBabValues; //damages % increasing and scaling
    public TechScaling[] intellectValues; //damages % increasing and scaling  
    public float[] hpValues; //hp respect the neuralHealth
    public float[] defenseValues; //values respect neural health
    public LevelScaling[] levelValues; //values to increase level

    bool useScaling = true;

    public bool UseScaling
    {
        get { return useScaling; }
        set { useScaling = value; }
    }

    public GameObject levelUpFx;

    #endregion

    private void Awake()
    {
        totalHp = hpValues[(int)(bonusNeuralHealth + baseNeuralHealth) -1];
        currentHealth = totalHp;
        NeuralHealthModified();
    }

    public IEnumerator Death(PlayerController player)
    {
        GameManager.instance.inputManager.enabled = false;
        GameManager.instance.pausable = false;

        FMODManager.instance.StopPlayerLowHp();
        FMODManager.instance.PlayPlayerDeath();
        StartCoroutine(player.CoEffect(4, player.deathFx, false));

        yield return new WaitForSeconds(5f);
        OnPlayerDeath?.Invoke();

        DataManager.instance.SaveData();
        StartCoroutine(DataManager.instance.SavingFeedback());
    }

    //called everytime the neural health gets updated
    public void NeuralHealthModified()
    {
        totalHp = hpValues[(int)(bonusNeuralHealth + baseNeuralHealth) - 1];
        float damageTaken = totalHp - currentHealth; //keeps tracks of the damage we took

        if (SceneManager.GetActiveScene().name == "Hub")
        {
            currentHealth = totalHp;
        }
        else
        {
            currentHealth = totalHp - damageTaken;
        }
    }

    //resets the health to the base level aswell as the other statistics
    public void ResetStatistics()
    {        
        currentHealth = totalHp;
        bonusAbility = 0;
        bonusAgility = 0;
        bonusIntellect = 0;
        bonusLuck = 0;
        bonusNeuralHealth = 0;
        bonusNeuralResistence = 0;
        bonusStrength = 0;
    }

    //Level Increase
    public void GetXP(float xp)
    {
        bool levelFound = false;       

        if (level < levelValues.Length)
        {
            exp += xp;

            float tempExp = exp;
            do
            {
                //checks if we have enough exp to move to the next level 
                if (levelValues[level].pointsNeeded <= tempExp)
                {
                    tempExp -= levelValues[level].pointsNeeded;
                    skillPoints += levelValues[level].increaseAbilityPoints;

                    attributePoints += levelValues[level].increaseStatsManageable;

                    if (baseStrength < 99)
                    {
                        baseStrength += levelValues[level].increaseStrenght;
                    }
                    if (baseIntellect < 99)
                    {
                        baseIntellect += levelValues[level].increaseIntellect;
                    }
                    if (baseAgility < 99)
                    {
                        baseAgility += levelValues[level].increaseAgility;
                    }
                    if (baseLuck < 99)
                    {
                        baseLuck += levelValues[level].increaseLuck;
                    }
                    if (baseNeuralHealth < 99)
                    {
                        baseNeuralHealth += levelValues[level].increaseNeuralHealth;
                    }
                    if (baseNeuralResistence < 99)
                    {
                        baseNeuralResistence += levelValues[level].increaseNeuralResistence;
                    }
                    totalHp = hpValues[(int)(bonusNeuralHealth + baseNeuralHealth) - 1];

                    playerLevelUp?.Invoke(level);

                    level++;

                    //spawn particellari
                    GameObject fx = Instantiate(levelUpFx, transform.position + new Vector3(0f, 0.2f, 0f), Quaternion.identity);
                    FMODManager.instance.PlayLevelUp();
                }
                else // if dont exit the cycle
                {
                    levelFound = true;
                }
            }
            while (!levelFound);
        }
    }

    private void Update()
    {
        List<ActiveBuffDebuff> actives = new List<ActiveBuffDebuff>();

        //if we have a debuff
        if (activeBuffDebuffs.Count > 0)
        {
            //for all debuffs
            foreach (var buff in activeBuffDebuffs)
            {
                //if the debuff is active
                if (buff.Update(this))
                {
                    actives.Add(buff);
                }
                else
                {
                    //if the debuff finishes his time , apply a buff
                    if (buff.buffDebuff.permanent == false)
                    {
                        ModifyStat(buff.buffDebuff, -buff.buffDebuff.statInstantMod);
                    }
                }
            }

            activeBuffDebuffs = actives;
        }

        
        if (currentHealth > totalHp)
        {
            GameObject h = Instantiate(healthFx, transform);
            Destroy(h, 2f);

            currentHealth = totalHp;
        }

    }

    void RemoveAllDebuffs()
    {
        activeBuffDebuffs = new List<ActiveBuffDebuff>();
    }

}
