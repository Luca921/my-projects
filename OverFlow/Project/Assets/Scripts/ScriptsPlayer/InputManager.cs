﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class InputManager : MonoBehaviour
{

    //we activates it when player can input 
    #region Variables
    public bool attack;
    public bool interact;
    public bool dash;
    public bool jump;
    public bool activeSkill;

    public float horizontalMove;
    public float verticalMove;

    public float DPadx;
    public float DPady;

    public float righStickX;
    public float rightStickY;

    public bool rightStick = false;

    float trigger;
    float lastTrigger;
    public bool rightTrigger;
    public bool leftTrigger;

    float lastX;
    float lastY;
    bool leftDPad;
    bool rightDPad;
    bool upDPad;

    public bool leftWeapon;
    public bool upWeapon;
    public bool rightWeapon;


    public bool rightBumper = false;
    public bool leftBumper = false;

    public bool freeze = false;
    public bool jam = false;
    public bool locked = false;

    public bool oneInput;

    public enum ActiveSkillInput { Shield, Others }
    public ActiveSkillInput skillInput;

    #endregion

    private void OnDisable()
    {
        
    }

    void Update()
    {
        if (!freeze)
        {
            verticalMove = Input.GetAxisRaw("Vertical");

            if (!jam)
            {
                if (skillInput == ActiveSkillInput.Shield)
                {
                    //different from the other skills becouse the shield needs to be kept pressed
                    activeSkill = trigger != 0 || Input.GetButton("Ability");
                }
                else
                {
                    activeSkill = leftTrigger || rightTrigger || Input.GetButtonDown("Ability");
                }

                leftBumper = Input.GetButtonDown("LBumper");
                rightBumper = Input.GetButtonDown("RBumper");
            }            

            attack = Input.GetButtonDown("Attack");
            DPadx = Input.GetAxis("DPadX");
            DPady = Input.GetAxis("DPadY");

            if (!locked)
            {
                dash = Input.GetButtonDown("Dash");
                jump = Input.GetButtonDown("Jump");

                horizontalMove = Input.GetAxisRaw("Horizontal");
                if (horizontalMove < -0.1f) horizontalMove = -1f;
                else if (horizontalMove > 0.1f) horizontalMove = 1f;
                else horizontalMove = 0f;
            }            

            righStickX = Input.GetAxis("RStickX");
            rightStickY = Input.GetAxis("RStickY");
            rightStick = Input.GetButtonUp("RStick");


            //Left and Right Trigger controls
            trigger = Input.GetAxis("Triggers");

            leftTrigger = false;
            rightTrigger = false;

            if (lastTrigger != trigger)
            {
                if (trigger == -1)
                {
                    leftTrigger = true;
                }
                else if (trigger == 1)
                {
                    rightTrigger = true;
                }
            }

            lastTrigger = trigger;

            //DPad axis controls
            // DPad Inputs
            float x = DPadx;
            float y = DPady;

            leftDPad = false;
            rightDPad = false;
            upDPad = false;

            if (lastX != x)
            {
                leftDPad = (x == -1);
                rightDPad = (x == 1);
            }

            if (lastY != y)
            {
                upDPad = (y == 1);
            }

            leftWeapon = Input.GetKeyDown(KeyCode.Alpha1) || leftDPad;
            upWeapon = Input.GetKeyDown(KeyCode.Alpha2) || upDPad;
            rightWeapon = Input.GetKeyDown(KeyCode.Alpha3) || rightDPad;

            lastX = x;
            lastY = y;

        }

        oneInput = leftTrigger || rightTrigger || dash || jump || attack || activeSkill || leftBumper 
                   || rightBumper || (horizontalMove != 0) || leftWeapon || upWeapon || rightWeapon;        
    }
}
