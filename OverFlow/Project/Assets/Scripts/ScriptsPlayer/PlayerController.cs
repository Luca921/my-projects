using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour
{
    public delegate void ConsumbaleInput(int index);
    public event ConsumbaleInput Consumable;

    //true if enter the state , off otherwise
    public delegate void DisableState(EnemyController.DisabledState state, float time); //called whenever the enemy gets a disable state
    public DisableState disableStateUpdated;

    public delegate void NotifySpellCdGUI(float cooldown);
    public NotifySpellCdGUI notifyAbilityUI;

    public delegate void WeaponChangeAction(int index);
    public event WeaponChangeAction OnWeaponChange;

    bool cDNotified = false; // true the first frame the CD starts

    #region Variables
    [Header("GRAVITY VARIABLES"),Space(3)]
    [HideInInspector]
    public float gravity;
    [Tooltip("Idle/Walk Gravity")]
    public float normalGravity;
    [Tooltip("Dash Gravity")]
    public float dashGravity;
    [Tooltip("Attack Gravity")]
    public float attackGravity;

    [Header("DASH VARIABLES"),Space(3)]
    [Tooltip("Time necessary to finish a dash")]
    public float timeDash;
    [Tooltip("Distance moved after a dash")]
    public float distanceDash;
    [Tooltip("Time necessary to do next dash")]
    public float cooldownDash;
    [HideInInspector]
    public bool canDash;
    [HideInInspector]
    public float dashSpeed;
    [Tooltip("DashVFX Prefab")]
    public GameObject dashVFX;

    [Tooltip("Horizontal speed while jumping")]
    public float jumpingHorizontalSpeed;

    [Tooltip("Speed while walking")]
    public float walkingSpeed;

    [Tooltip("Vertical speed")]
    public float jumpSpeed;

    [Tooltip("DoubleJump speed")]
    public float doubleJumpSpeed;

    [Tooltip("Impulse given to the player when Slam Down")]
    public float slamSpeed;

    [Tooltip("Player's model reference")]
    public GameObject[] model;

    public Material[] materials;

    [HideInInspector]
    public float speed;

    public float modifiedSpeed;

    public float maxSpeed;
    public float minSpeed;

    [HideInInspector]
    public CharacterStatistics playerStats;

    //[HideInInspector]
    public PlayerState playerState;

    [HideInInspector]
    public GameObject colliderWeapon;

    [HideInInspector]
    public bool isInvulnerable;

    [HideInInspector]
    public InputManager inputManager;

    public Animator anim;

    public float horizontalMove;

    public float stunTime;

    public Rigidbody rb;

    [System.Serializable]
    public struct Weapon
    {
        public GameObject w;
        public GameObject collider;
        public bool disabled;
        public Color c1;
        public Color c2;
    }

    public List<Weapon> weapons;

    [HideInInspector]
    public GameObject currentWeapon;
    [HideInInspector]
    public int indexWeapon;

    [HideInInspector]
    public float tempDashSpeed;

    float xMove;
    float zMove;
    bool isJumping;
    bool canJump;
    public bool canMove { get; set; }
    bool canAttack;
    [HideInInspector]
    public bool onHub;

    int counter = 0;

    //[HideInInspector]
    public bool doublejump = true;
    int jumpCount = 0;

    bool pressed;
    [HideInInspector]
    public bool abilityUsed;

    public bool Pressed
    {
        get { return pressed; }
        set { pressed = value; }
    }

    bool startCooldown = false;

    public bool StartCooldown
    {
        get { return startCooldown; }
        set { startCooldown = value; }
    }

    float skillCooldown;

    public float SkillCooldown
    {
        get { return skillCooldown; }
        set { skillCooldown = value; }
    }

    float currentCooldown;

    public float CurrentCooldown
    {
        get { return currentCooldown; }
        set { currentCooldown = value; }
    }

    int maxCariche;

    public int MaxCariche
    {
        get { return maxCariche; }
        set { maxCariche = value; }
    }

    int currentCharges = 0;

    public int CaricheCorrenti
    {
        get { return currentCharges; }
        set { currentCharges = value; }
    }

    bool hit;

    public bool Hit
    {
        get { return hit; }
        set { hit = value; }
    }

    bool secondLifeBar;

    public bool SecLifeBar
    {
        get { return secondLifeBar; }
        set { secondLifeBar = value; }
    }

    float dmg = 0f;

    public float Dmg
    {
        get { return dmg; }
        set { dmg = value; }
    }

    [HideInInspector]
    public bool thirdAttack = false;

    bool teleport = false;

    public enum PlayerState
    {
        idle,
        walk,
        jump,
        dash,
        attack,
        hit,
        dead,
        stun,
        ability
    }

    SpawnEffect spawnEffect;

    public GameObject spawnFx;
    public GameObject deathFx;

    bool frz = false;

    #endregion

    public delegate void UpdateAction(int num);
    public event UpdateAction UpdateValue;

    [HideInInspector]
    public bool imDead;

    public float lowHealthPerc;
    public float highHealthPerc;

    bool instantLowHealth;
    bool instantHighHealth;

    private void Awake()
    {
        DontDestroyOnLoad(this);
        rb = GetComponent<Rigidbody>();

        rb.useGravity = false;
        gravity = normalGravity;

        canAttack = true;

        canDash = true;
        dashSpeed = distanceDash / timeDash;
        tempDashSpeed = dashSpeed;

        playerStats = GetComponent<CharacterStatistics>();
        speed = walkingSpeed;
        modifiedSpeed = walkingSpeed;
        playerState = PlayerState.idle;

        indexWeapon = 0;
        currentWeapon = weapons[indexWeapon].w; //auto equip the katana
        colliderWeapon = weapons[indexWeapon].collider; //auto set weapon collider

        spawnEffect = GetComponent<SpawnEffect>();
    }

    private void Start()
    {
        Camera.main.GetComponent<CameraBehaviour>().target = transform; //sets the camera target
        GameManager.instance.OnDungeonStart += SetUpActiveSkillInput;

        StartCoroutine(CoEffect(3, spawnFx, true));

        if (onHub)
        {
            currentWeapon.SetActive(false);
        }
    }

    private void Update()
    {
        if (!onHub && playerStats.currentHealth < (playerStats.hpValues[(int)(playerStats.baseNeuralHealth + playerStats.bonusNeuralHealth)] * lowHealthPerc)/100f )
        {
            GameManager.instance.triggerManager.ActivateEffect(Trigger.Triggers.UntilLowHealth, null);

            if (instantLowHealth == false)
            {
                GameManager.instance.triggerManager.ActivateEffect(Trigger.Triggers.LowHealth, null);

                instantLowHealth = true;
            }
        }
        else
        {
            if (instantLowHealth)
            {
                instantLowHealth = false;
            }
        }

        if (!onHub && playerStats.currentHealth > (playerStats.hpValues[(int)(playerStats.baseNeuralHealth + playerStats.bonusNeuralHealth)] * highHealthPerc) / 100f)
        {
            GameManager.instance.triggerManager.ActivateEffect(Trigger.Triggers.UntilHighHealth, null);

            if (instantHighHealth == false)
            {
                GameManager.instance.triggerManager.ActivateEffect(Trigger.Triggers.HighHealth, null);

                instantHighHealth = true;
            }
        }
        else
        {
            if (instantHighHealth)
            {
                instantHighHealth = false;
            }
        }

        if (inputManager.leftBumper)
        {
            Consumable.Invoke(0);
        }

        if (inputManager.rightBumper)
        {
            Consumable?.Invoke(1);
        }

        if (!teleport)
        {
            //checks if the user activated the active ability
            if (inputManager.activeSkill)
            {
                pressed = true;
                GameManager.instance.triggerManager.ActivateEffect(Trigger.Triggers.ActiveAbility, null); // pulls that trigger
            }
            else
            {
                pressed = false;
            }
        }

        if (startCooldown && playerStats.currentHealth > 0f)
        {
            Cooldown();
        }

        //Input Movement
        if (inputManager.enabled)
        {
            horizontalMove = inputManager.horizontalMove;
            anim.SetFloat("horizontalMove", Mathf.Abs(horizontalMove));
        }
        else
        {
            horizontalMove = 0;
            anim.SetFloat("horizontalMove", 0);
        }        

        if (playerState == PlayerState.jump)
        {
            if (doublejump && jumpCount == 1)
            {
                //second jump
                if (inputManager.jump)
                {
                    jumpCount++;

                    playerState = PlayerState.jump;

                    rb.velocity = new Vector3(rb.velocity.x, 0f, rb.velocity.z); 
                    rb.AddForce(Vector3.up * jumpSpeed, ForceMode.Impulse);
                    anim.SetTrigger("Jump");
                    anim.SetBool("onFloor", false);
                }
            }

            //review non dovrebbe fare lo slam adesso ?

            //check for the slam while single jumping
            //if (!doublejump && slam)
            //{
            //    if (inputManager.jump && inputManager.verticalMove < -0.2f)
            //    {
            //        rb.AddForce(-Vector3.up * slamSpeed, ForceMode.Impulse);
            //    }
            //}
        }

        //FSM
        switch (playerState)
        {
            #region Idle
            case (PlayerState.idle):
                {
                    canMove = true;

                    gravity = normalGravity;
                    //If the axis is upper than deadzone change the state in walk
                    if (Mathf.Abs(horizontalMove) > 0.2f)
                    {
                        playerState = PlayerState.walk;
                    }

                    CheckGround();

                    //Change in attack state
                    if (!onHub && inputManager.attack && canAttack)
                    {
                        playerState = PlayerState.attack;
                        anim.SetTrigger("Attack1");
                    }

                    //Change in jump state, add an upper force and set that the player isn't touch the floor
                    if (inputManager.jump && anim.GetBool("onFloor"))
                    {
                        jumpCount++;

                        playerState = PlayerState.jump;
                        
                        rb.AddForce(Vector3.up * jumpSpeed, ForceMode.Impulse);
                        anim.SetTrigger("Jump");
                        anim.SetBool("onFloor", false);

                        return;
                    }

                    if (!onHub && inputManager.dash && canDash)
                    {
                        canDash = false;
                        anim.SetBool("Dash",true);
                        playerState = PlayerState.dash;
                        StartCoroutine(Dash(timeDash));

                        return;
                    }

                    if (!onHub && inputManager.upWeapon && currentWeapon != weapons[0].w && weapons[0].disabled == false)
                    {
                        anim.SetFloat("WeaponChange", 0f);
                        WeaponSwitch(0);
                    }

                    if (!onHub && inputManager.leftWeapon && currentWeapon != weapons[1].w && weapons[1].disabled == false)
                    {
                        anim.SetFloat("WeaponChange", .5f);
                        WeaponSwitch(1);
                    }

                    if (!onHub && inputManager.rightWeapon && currentWeapon != weapons[2].w && weapons[2].disabled == false)
                    {
                        anim.SetFloat("WeaponChange", 1f);
                        WeaponSwitch(2);
                    }

                    thirdAttack = false;

                    break;
                }
            #endregion

            #region Walk
            case (PlayerState.walk):
                {
                    gravity = normalGravity;
                    canMove = true;

                    //If the player isn't moving, change state in idle
                    if (horizontalMove == 0f)
                    {
                        playerState = PlayerState.idle;
                        FMODManager.instance.StopPlayerFootsteps();
                    }

                    CheckGround();

                    //Change in attack state
                    if (!onHub && inputManager.attack && canAttack)
                    {
                        anim.SetTrigger("Attack1");
                        playerState = PlayerState.attack;
                    }

                    //Change in jump state, add an upper force and set that the player isn't touch the floor
                    if (inputManager.jump && anim.GetBool("onFloor"))
                    {
                        jumpCount++;

                        playerState = PlayerState.jump;

                        rb.AddForce(Vector3.up * jumpSpeed, ForceMode.Impulse);
                        anim.SetTrigger("Jump");
                        anim.SetBool("onFloor", false);
                        GameManager.instance.triggerManager.ActivateEffect(Trigger.Triggers.Jump, null);

                        return;
                    }

                    if (!onHub && inputManager.dash && canDash)
                    {
                        canDash = false;
                        anim.SetBool("Dash", true);
                        playerState = PlayerState.dash;
                        StartCoroutine(Dash(timeDash));

                        return;
                    }

                    if (!onHub && inputManager.upWeapon && currentWeapon != weapons[0].w && weapons[0].disabled == false)
                    {
                        anim.SetFloat("WeaponChange", 0f);
                        WeaponSwitch(0);
                    }

                    if (!onHub && inputManager.leftWeapon && currentWeapon != weapons[1].w && weapons[1].disabled == false)
                    {
                        anim.SetFloat("WeaponChange", .5f);
                        WeaponSwitch(1);
                    }

                    if (!onHub && inputManager.rightWeapon && currentWeapon != weapons[2].w && weapons[2].disabled == false)
                    {
                        anim.SetFloat("WeaponChange", 1f);
                        WeaponSwitch(2);
                    }

                    thirdAttack = false;

                    break;
                }
            #endregion

            #region Attack
            case (PlayerState.attack):
                {
                    rb.velocity = Vector3.zero;
                    gravity = attackGravity;
                    //During the attack state the player can't move
                    canMove = false;

                    if (!onHub && inputManager.dash && canDash)
                    {
                        DisableCollider();
                        canDash = false;
                        anim.SetBool("Dash", true);
                        playerState = PlayerState.dash;
                        StartCoroutine(Dash(timeDash));

                        return;
                    }

                    break;
                }
            #endregion

            #region Jump
            case (PlayerState.jump):
                {
                    canMove = true;
                    CheckGround();

                    if (!onHub && inputManager.dash && canDash)
                    {
                        canDash = false;
                        anim.SetBool("Dash", true);
                        playerState = PlayerState.dash;
                        StartCoroutine(Dash(timeDash));

                        return;
                    }

                    //Change in attack state
                    if (!onHub && inputManager.attack && canAttack)
                    {
                        canAttack = false;
                        playerState = PlayerState.attack;
                        anim.SetTrigger("Attack1");
                    }

                    if (!onHub && inputManager.upWeapon && currentWeapon != weapons[0].w && weapons[0].disabled == false)
                    {
                        anim.SetFloat("WeaponChange", 0f);
                        WeaponSwitch(0);
                    }

                    if (!onHub && inputManager.leftWeapon && currentWeapon != weapons[1].w && weapons[1].disabled == false)
                    {
                        anim.SetFloat("WeaponChange", .5f);
                        WeaponSwitch(1);
                    }

                    if (!onHub && inputManager.rightWeapon && currentWeapon != weapons[2].w && weapons[2].disabled == false)
                    {
                        anim.SetFloat("WeaponChange", 1f);
                        WeaponSwitch(2);
                    }

                    thirdAttack = false;

                    break;
                }
            #endregion

            #region Dash
            case (PlayerState.dash):
                {
                    gravity = dashGravity;
                    canMove = false;
                    isInvulnerable = true;
                    thirdAttack = false;
                    break;
                }
            #endregion

            #region Hit
            case (PlayerState.hit):
                {
                    canMove = false;
                    colliderWeapon.SetActive(false); // disable the weapon collider when hitted
                    thirdAttack = false;
                    break;
                }
            #endregion

            #region Dead
            case (PlayerState.dead):
                {
                    canMove = false;
                    thirdAttack = false;
                    break;
                }
            #endregion

            #region Stun
            case (PlayerState.stun):
                {
                    canMove = false;

                    if(stunTime <= 0)
                    {
                        playerStats.bodyMesh.material = playerStats.bodyMaterial;
                        anim.SetBool("Stunned", false);

                        if (frz)
                        {
                            frz = false;

                            foreach (GameObject g in model)
                            {
                                g.GetComponent<SkinnedMeshRenderer>().material = materials[0];
                            }
                        }

                        playerState = PlayerState.idle;
                        return;
                    }
                    else
                    {
                        playerStats.bodyMesh.material.color = Color.Lerp(playerStats.stunMaterial.color, playerStats.bodyMaterial.color, 1f/stunTime);
                        anim.SetBool("Stunned", true);

                        if (!frz)
                        {
                            frz = true;

                            foreach (GameObject g in model)
                            {
                                g.GetComponent<SkinnedMeshRenderer>().material = materials[5];
                            }
                        }
                    }

                    stunTime -= Time.deltaTime;

                    //if the player attacks while stunned the stun time decrease
                    if (inputManager.attack)
                    {
                        stunTime--;
                    }

                    break;
                }
            #endregion

            case (PlayerState.ability):
                {
                    canMove = false;

                    CheckGround();

                    if (!abilityUsed)
                    {
                        anim.SetTrigger("AbilityOn");
                        abilityUsed = true;
                    }

                    thirdAttack = false;

                    break;
                }
        }
    }

    public void FootOnFloor()
    {
        FMODManager.instance.PlayPlayerFootsteps();
    }

    void FixedUpdate()
    {
        rb.AddForce(Vector3.down * gravity, ForceMode.Acceleration);

        if (canMove)
        {
            //Move the player whit axis and speed parameter using its rigidbody
            Vector3 playerMovement = transform.right * (horizontalMove * speed * Time.deltaTime);
            rb.MovePosition(rb.position + playerMovement);

            //Turn the player in base of its direction
            if (horizontalMove > 0.2f)
            {
                rb.transform.localScale = new Vector3(1f, 1f, 1f);
            }
            else if (horizontalMove < -0.2f)
            {
                rb.transform.localScale = new Vector3(-1f, 1f, 1f);
            }
        }
    }

    //TEST AFTER BETA REVIEW (HAVE TO DELETE)
    //private void OnCollisionStay(Collision collision)
    //{
    //    /*
    //    //The player touch the floor, so change the state in idle
    //    if ((collision.gameObject.layer == 9 || collision.gameObject.layer == 12) && rb.velocity.y <= 0.5f)
    //    {
    //        // takes the "or" between the two layers
    //        int layerMask = (1<<9) | (1 << 12);

    //        //checks if the collision is under player's feet
    //        Collider[] collisions = Physics.OverlapSphere(transform.position, 0.1f, layerMask);
    //        if(collisions.Length > 0)
    //        {
    //            anim.SetBool("onFloor", true);
    //            canAttack = true;
    //            if (playerState == PlayerState.jump)
    //            {
    //                playerState = PlayerState.idle;
    //            }
    //            jumpCount = 0;
    //        }
    //    }
    //    */
    //}

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<DoorBehaviour>() != null)
        {
            teleport = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<DoorBehaviour>() != null)
        {
            if (other.GetComponent<DoorBehaviour>().nextDoor != null)
            {
                teleport = true;
            }
        }

        //Enemy hit the player
        if (playerStats.currentHealth > 0f && !isInvulnerable && other.tag == "EnemyWeapon" && secondLifeBar == false)
        {
            //Pushback
            float direction = Mathf.Sign(transform.position.x - other.transform.position.x);
            rb.AddForce(Vector3.right * direction * 1000f);



            WeaponClass wp = other.GetComponent<WeaponClass>();
            wp.WeaponTypeCases();


            float res = (int)Mathf.Clamp(playerStats.baseNeuralResistence + playerStats.bonusNeuralResistence, 0, 100); //calculates the resistence

            //depending on the weapon calculates the damage
            if (wp.wpType == WeaponClass.Type.Katana)
            {
                dmg = GameManager.instance.damager.CalculateDamage(wp.Atk, wp.Stat, wp.LevelScalings, playerStats.baseDefense, res, playerStats.defenseValues, playerStats.UseScaling, 0f);
            }
            else
            {
                dmg = GameManager.instance.damager.CalculateDamage(wp.Atk, wp.Stat, wp.BabScalings, playerStats.baseDefense, res, playerStats.defenseValues, playerStats.UseScaling, 0f);
            }

            Enemy enemy = wp.ownerStats.GetComponent<Enemy>();

            if (enemy.championType == Enemy.ChampionVersion.green)
            {
                playerStats.DebuffAdd(enemy.greenLeak);
            }


            //if we have the shield activated we dont take damages
            if (secondLifeBar == false)
            {
                TakeDamage(dmg, true);

                //Dasher dasher = wp.ownerStats.GetComponent<Dasher>();
                //if(dasher != null)
                //{
                //    StartCoroutine(Dot(dmg, dasher.damageTime));
                //}

                GameManager.instance.triggerManager.ActivateEffect(Trigger.Triggers.PlayerHit, enemy);
            }

            dmg = 0;

            //If don't die set the hit animation, else invoke the player death events
            //if (playerStats.currentHealth > 0f && playerState != PlayerState.stun)
            //{
            //    playerState = PlayerState.hit;
            //    anim.SetTrigger("Hit");
            //    GameManager.instance.triggerManager.ActivateEffect(Trigger.Triggers.PlayerHit, enemy);
            //}
        }

        if (isInvulnerable && other.gameObject.layer == 10)
        {
            //calls the player hitted trigger
            GameManager.instance.triggerManager.ActivateEffect(Trigger.Triggers.Dash, other.gameObject.GetComponent<Enemy>());
        }
    }

    //TEST ON FLOOR POST BETA REVIEW
    void CheckGround()
    {
        int layerMask = (1 << 9) | (1 << 12);

        //checks if the collision is under player's feet
        Collider[] collisions = Physics.OverlapSphere(transform.position, 0.1f, layerMask);
        if (collisions.Length > 0)
        {
            anim.SetBool("onFloor", true);
            canAttack = true;
            if (playerState == PlayerState.jump)
            {
                FMODManager.instance.PlayPlayerLanding();
                playerState = PlayerState.idle;
            }
            jumpCount = 0;
        }
        else
        {
            anim.SetBool("onFloor", false);
            if (abilityUsed)
            {
                playerState = PlayerState.ability;
            }
            else
            {
                playerState = PlayerState.jump;
                
                if(jumpCount == 0)
                {
                    jumpCount = 1;
                }
            }
        }
    }

    public IEnumerator TeleportIn()
    {
        FMODManager.instance.TeleportUsed();
        //FMODManager.instance.HumanEnemiesTriggered();
        WaitForEndOfFrame wf = new WaitForEndOfFrame();

        gameObject.layer = 14; //Layer Dead, so he can't collide with enemy's weapon

        foreach (GameObject g in model)
        {
            g.GetComponent<SkinnedMeshRenderer>().material = materials[2];
        }

        MaterialPropertyBlock mpb = new MaterialPropertyBlock();
        float dissolve = 0;

        while (dissolve <= 1)
        {
            mpb.SetFloat("_DissolveAmount", dissolve);
            dissolve += Time.deltaTime;

            foreach (GameObject g in model)
            {
                g.GetComponent<SkinnedMeshRenderer>().SetPropertyBlock(mpb);
            }

            yield return wf;
        }
    }

    public IEnumerator TeleportOut()
    {
        WaitForEndOfFrame wf = new WaitForEndOfFrame();
        MaterialPropertyBlock mp = new MaterialPropertyBlock();
        float diss = 1f;

        while (diss > 0)
        {
            mp.SetFloat("_DissolveAmount", diss);
            diss -= Time.deltaTime;

            foreach (GameObject g in model)
            {
                g.GetComponent<SkinnedMeshRenderer>().SetPropertyBlock(mp);
            }

            yield return wf;
        }

        foreach (GameObject g in model)
        {
            g.GetComponent<SkinnedMeshRenderer>().material = materials[0];
        }

        gameObject.layer = 11; //Layer Player, so he can collide with enemy's weapon
    }

    IEnumerator Dash(float time)
    {
        foreach (GameObject g in model)
        {
            g.GetComponent<SkinnedMeshRenderer>().material = materials[1];
        }

        gameObject.layer = 13; //PlayerDash layer

        float velocityX = distanceDash / time;

        float direction = rb.transform.localScale.x;

        if(horizontalMove < -0.2f || horizontalMove > 0.2f)
        {
            direction = Mathf.Sign(horizontalMove);
            rb.transform.localScale = new Vector3(direction, 1f, 1f);
        }
        
        rb.velocity = new Vector3(dashSpeed * direction, 0, 0);

        GameObject tempVFX = Instantiate(dashVFX, transform.position + Vector3.up, Quaternion.identity, transform);
        FMODManager.instance.PlayPlayerDash();

        yield return new WaitForSeconds(time);

        gameObject.layer = 11; //Player layer

        foreach (GameObject g in model)
        {
            g.GetComponent<SkinnedMeshRenderer>().material = materials[0];
        }

        rb.velocity = Vector3.zero;

        Destroy(tempVFX);

        DashFinished();

        yield return new WaitForSeconds(cooldownDash);

        canDash = true;
    }

    //Return to idle state after that receive damage
    public void HitFinished()
    {
        if (!anim.GetBool("Hit"))
        {
            playerState = PlayerState.idle;
        }
    }

    public void DashFinished()
    {
        anim.SetBool("Dash", false);
        if(horizontalMove <= 0.1f)
        {
            playerState = PlayerState.idle;
        }
        else
        {
            playerState = PlayerState.walk;
        }
        
        isInvulnerable = false;
    }

    //Function called by animation that disable the weapon collider
    public void DisableCollider()
    {
        colliderWeapon.SetActive(false);
    }

    public void EnableCollider()
    {
        colliderWeapon.SetActive(true);
    }

    public void EnableDamage()
    {
        colliderWeapon.GetComponent<WeaponClass>().NextAttack();
    }

    //reset the position of the player and all the statistics/health
    public void ResetPlayer()
    {
        GameManager.instance.pausable = true;
        imDead = false;
        anim.SetTrigger("Reset");
        playerStats.bodyMesh.material = playerStats.bodyMaterial;
        playerStats.ResetStatistics();
        playerState = PlayerState.idle;
        gameObject.layer = 11; //Player layer
        speed = walkingSpeed;
        modifiedSpeed = walkingSpeed;
        dashSpeed = tempDashSpeed;
        canDash = true;
    }

    //bool isAllowed = true;
    public float weaponChangeTriggerCooldown;

    //called when the weapon gets switched , activates the new collider
    public void WeaponSwitch(int index)
    {        
        currentWeapon.SetActive(false);
        weapons[index].w.SetActive(true);
        colliderWeapon = weapons[index].collider;
        currentWeapon = weapons[index].w;
        indexWeapon = index;

       
        GameManager.instance.triggerManager.ActivateEffect(Trigger.Triggers.WeaponChange, null);
        //isAllowed = false;

        //StartCoroutine(ResetWeapon());
        OnWeaponChange?.Invoke(index);
    }

    public IEnumerator ResetWeapon()
    {
        yield return new WaitForSeconds(weaponChangeTriggerCooldown);

        //isAllowed = true;
    }

    //if the weapon gets disabled by an outside effect
    public void WeaponDisabled()
    {
        int n = 0;

        if (counter < 2)
        {
            foreach (Weapon wpn in weapons)
            {
                if (wpn.w == currentWeapon)
                {
                    n = weapons.IndexOf(wpn);
                    break;
                }
            }

            //create a temp weapon and "copy" the weapon that got disabled
            Weapon tempWeapon = new Weapon();
            tempWeapon.w = weapons[n].w;
            tempWeapon.c1 = weapons[n].c1;
            tempWeapon.c2 = weapons[n].c2;
            tempWeapon.disabled = true;
            weapons[n] = tempWeapon;

            currentWeapon.SetActive(false); //disable the current weapon

            if (n < 2)
            {
                //sets the next weapon as active
                weapons[n + 1].w.SetActive(true);
                colliderWeapon = weapons[n + 1].collider;
                currentWeapon = weapons[n + 1].w;
                indexWeapon = n + 1;

                anim.SetFloat("WeaponChange", (n+1)*0.5f);

                GameManager.instance.triggerManager.ActivateEffect(Trigger.Triggers.WeaponChange, null); //weapon changed trigger            

                OnWeaponChange?.Invoke(n + 1);
            }
            else
            {
                //if the weapon that has been disabled is the last one , the first one gets enabled
                weapons[0].w.SetActive(true);
                colliderWeapon = weapons[0].collider;
                currentWeapon = weapons[0].w;
                indexWeapon = 0;

                anim.SetFloat("WeaponChange", 0f);

                GameManager.instance.triggerManager.ActivateEffect(Trigger.Triggers.WeaponChange, null); //weapon changed trigger

                //if (isAllowed)
                //{
                //    GameManager.instance.triggerManager.ActivateEffect(Trigger.Triggers.WeaponChange, null); //weapon changed trigger
                //    isAllowed = false;

                //    StartCoroutine(ResetWeapon());
                //}

                OnWeaponChange?.Invoke(0);
            }
        }

        counter++;
    }

    //when the player gets stunned
    public void Stun(float time)
    {
        disableStateUpdated(EnemyController.DisabledState.Freeze, time);
        stunTime = time;
        playerState = PlayerState.stun;
    }

    //setUp the input of the abilities
    public void SetUpActiveSkillInput()
    {
        foreach (Skill skill in GameManager.instance.EquippedSkills)
        {
            if (skill.type == Skill.Types.Active)
            {
                //we check if the skill is the shield becouse the shield needs to be kept pressed
                if (skill.name == "Shield")
                {
                    inputManager.skillInput = InputManager.ActiveSkillInput.Shield;
                }
                else
                {
                    inputManager.skillInput = InputManager.ActiveSkillInput.Others;
                }
            }
        }
    }

    //check the cooldown of the ability
    public void Cooldown()
    {
        if (!cDNotified)
        {
            //notifies the GUI the CD has started
            notifyAbilityUI(skillCooldown);
            cDNotified = true;
        }

        if (currentCooldown > 0f)
        {
            currentCooldown -= Time.deltaTime;
        }
        else if (currentCooldown <= 0f)
        {

            currentCharges++;
            UpdateValue?.Invoke(1);


            cDNotified = false;

            //checks if we have all the charges ready , otherwise restarts the cooldown
            if (currentCharges >= maxCariche)
            {
                currentCooldown = skillCooldown;
                startCooldown = false;
                currentCharges = maxCariche;
            }
            else
            {
                currentCooldown = skillCooldown;
                startCooldown = true;
            }
        }
    }

    //function to inflict the player a FLAT damage , not based on scaling or other stuffs
    public void TakeDamage(float damage, bool hitControl)
    {
        hit = hitControl;

        playerStats.currentHealth -= damage;

        FMODManager.instance.PlayPlayerDamage();
        if (playerStats.currentHealth <= 160f)
        {
            FMODManager.instance.PlayPlayerLowHp();
        }

        //Debug panel update
        //GameManager.instance.debugUI.danniRicevuti = damage;
        
        if (playerStats.currentHealth > 0f && hitControl && playerState != PlayerState.stun)
        {
            playerState = PlayerState.hit;
            anim.SetTrigger("Hit");
        }
        else if (playerStats.currentHealth <= 0f)
        {
            startCooldown = false;

            anim.SetBool("Stunned", false);

            playerState = PlayerState.dead;
            anim.SetTrigger("Dead");

            //Can collide only with Ground and Map Layer
            gameObject.layer = 14; // Dead layer

            if (!imDead)
            {
                StartCoroutine(playerStats.Death(this));
                imDead = true;
            }
        }
    }

    public IEnumerator Dot(float damage, float damageTime)
    {
        //Every second of damageTime inflict some damage to player
        for (float counter = 0f; counter < damageTime - 1; ++counter)
        {
            yield return new WaitForSeconds(1f);
            TakeDamage(damage, false);
        }
    }

    public void StartDot(float damage, float damageTime)
    {
        StartCoroutine(Dot(damage, damageTime));
    }

    public void EnterDungeon(Vector3 startPosition)
    {
        transform.position = startPosition;
        transform.localScale = Vector3.one;
        onHub = false;
        currentWeapon.SetActive(true);
        canDash = true;

        StopAllCoroutines();
        StartCoroutine(CoEffect(3, spawnFx, true));
    }

    public void EnterHub(Vector3 startPosition)
    {
        transform.position = startPosition;
        transform.localScale = Vector3.one;
        onHub = true;
        currentWeapon.SetActive(false);
        GameManager.instance.inputManager.enabled = true;
        ResetPlayer();
        StopAllCoroutines();
        FMODManager.instance.PlayHubSpawn();
        StartCoroutine(CoEffect(3, spawnFx, true));
    }

    //public IEnumerator DeathEffect()
    //{
    //    foreach (GameObject g in model)
    //    {
    //        g.GetComponent<SkinnedMeshRenderer>().material = materials[4];
    //    }

    //    deathFx.SetActive(true);

    //    spawnEffect.enabled = true;

    //    yield return new WaitForSeconds(spawnEffect.spawnEffectTime + 1f);
    //}

    public IEnumerator CoEffect(int index, GameObject effect, bool resetDefaultMaterial)
    {
        foreach (GameObject g in model)
        {
            g.GetComponent<SkinnedMeshRenderer>().material = materials[index];
        }

        effect.SetActive(true);

        spawnEffect.ps = effect.GetComponentInChildren<ParticleSystem>();
        spawnEffect.enabled = true;

        //yield return new WaitUntil(() => spawnEffect.done);
        yield return new WaitForSeconds(spawnEffect.spawnEffectTime);

        spawnEffect.enabled = false;

        effect.SetActive(false);

        if (resetDefaultMaterial)
        {
            foreach (GameObject g in model)
            {
                g.GetComponent<SkinnedMeshRenderer>().material = materials[0];
            }
        }               
    }

    public GameObject jamFx;

    public void JamState(float dur)
    {
        disableStateUpdated(EnemyController.DisabledState.Jam, dur);

        GameObject fx = Instantiate(jamFx, transform);

        Destroy(fx, dur + 1f);

        var ps = fx.GetComponent<ParticleSystem>().main;
        ps.duration = dur;
        ps.startLifetime = dur;

        fx.GetComponent<ParticleSystem>().Play();

        LockJam j = fx.GetComponent<LockJam>();
        j.skinmesh = model[0].GetComponent<SkinnedMeshRenderer>();
    }

    public GameObject lockFx;

    public void LockState(float dur)
    {
        disableStateUpdated(EnemyController.DisabledState.Lock, dur);

        GameObject fx = Instantiate(lockFx, transform);

        Destroy(fx, dur + 1f);

        var ps = fx.GetComponent<ParticleSystem>().main;
        ps.duration = dur;
        ps.startLifetime = dur;

        fx.GetComponent<ParticleSystem>().Play();

        LockJam j = fx.GetComponent<LockJam>();
        j.skinmesh = model[0].GetComponent<SkinnedMeshRenderer>();
    }
}
