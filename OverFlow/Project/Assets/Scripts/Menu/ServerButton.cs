﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ServerButton : ScaleButton
{
    public ServerNavigation serverNavigation;

    public Image arrowImage;

    public float delayActivePanel;

    RectTransform rectTransform;

    GameObject missionPanel;


    TextMeshProUGUI dataServerTxt;

    [HideInInspector]
    public bool isInteractable = true;

    [HideInInspector]
    public float size;

    [HideInInspector]
    public Mission missionInfo;

    private void Awake()
    {
        button = GetComponent<MyButton>();
        rectTransform = GetComponent<RectTransform>();
    }

    private void Start()
    {
        size = serverNavigation.size;
        missionPanel = serverNavigation.dataServerPanel;
        dataServerTxt = serverNavigation.dataServerTxt;
    }

    private void Update()
    {
        if (button.Highlight() && !isHighlighted)
        {
            StopAllCoroutines();
            StartCoroutine(AnimationSize(size));

            //coroutine that show the mission info after a short delay
            StartCoroutine(StartArrowGeneration());
            StartCoroutine(ActiveInfoPanel());

            isHighlighted = true;
            serverNavigation.serverButton = gameObject;
            
            SetDataText();
            
        }
        else if(!button.Highlight() && isHighlighted)
        {
            StopAllCoroutines();
            StartCoroutine(AnimationSize(size*0.5f));
            isHighlighted = false;
            missionPanel.SetActive(false);
            arrowImage.fillAmount = 0;

        }
    }

    void SetDataText()
    {
        if (missionInfo == null)
        {
            dataServerTxt.text = "NON ESISTE UNA MISSIONE PER QUESTO SERVER!";
            return;
        }
        string chunk = "Chunks --> " + missionInfo.minRooms + " to " + missionInfo.pool.rooms.Count + "\n";
        string enemies = "Enemies --> " + missionInfo.enemiesMin + " to " + missionInfo.enemiesMax + "\n";
        string enemiesLV = "Enemies LV range --> " + missionInfo.enemiesLVMin + " to " + missionInfo.enemiesLVMax + "\n";
        string traps = "Traps --> " + missionInfo.trapsCountMin + " to " + missionInfo.trapsCountMax + "\n";
        dataServerTxt.text = chunk + enemies + enemiesLV + traps;
    }

    protected IEnumerator ActiveInfoPanel()
    {
        yield return new WaitForSeconds(delayActivePanel);

        missionPanel.SetActive(true);
    }

    protected IEnumerator AnimationSize(float finalSize)
    {
        WaitForEndOfFrame eof = new WaitForEndOfFrame();
        float startValue = rectTransform.sizeDelta.x;
        float animationAmount = 0.0f;
        float speed = 1.0f / time;

        while (animationAmount < 1.0f)
        {
            animationAmount = Mathf.Min(1.0f, animationAmount + Time.deltaTime * speed);

            float newSize = Mathf.Lerp(startValue, finalSize, animationAmount);
            rectTransform.sizeDelta = new Vector2(newSize, newSize);

            yield return eof;
        }
    }

    protected IEnumerator StartArrowGeneration()
    {
        yield return new WaitForSeconds(delayActivePanel/2);
        StartCoroutine(ArrowFiller());
    }


    protected IEnumerator ArrowFiller()
    {

        //while(arrowImage.fillAmount < 1)
        //{
        //    arrowImage.fillAmount += 0.031f;
        //    yield return new WaitForEndOfFrame();
        //}
        WaitForEndOfFrame eof = new WaitForEndOfFrame();
        float animationAmount = 0.0f;
        float speed = 1.0f / (delayActivePanel/2);

        while (animationAmount < 1.0f)
        {
            animationAmount = Mathf.Min(1.0f, animationAmount + Time.deltaTime * speed);

            float newSize = Mathf.Lerp(0, 1, animationAmount);
            arrowImage.fillAmount = newSize;

            yield return eof;
        }
    }

}
