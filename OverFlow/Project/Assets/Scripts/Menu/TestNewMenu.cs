﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TestNewMenu : MonoBehaviour
{
    public Transform mainCamera;
    public Transform target;
    public Vector3 offsetMonitor;
    public float time;

    public Vector3 OffsetMonitor { get => offsetMonitor; set => offsetMonitor = value; }

    public Transform startPosition;
    bool isMoving;

    //PANELS & BUTTONS REFERENCE
    public GameObject controlsButton;
    public GameObject soundButton;
    public GameObject creditsButton;
    public GameObject newGamePanel;
    public GameObject newGameConfirmPanel;
    public GameObject loadGamePanel;
    public GameObject optionsPanel;
    public GameObject controlsPanel;
    public GameObject soundPanel;
    public GameObject creditsPanel;
    public GameObject quitPanel;

    //BOOLEAN TO CONTROL THE STATE OF MENU
    [SerializeField] private GameObject firstButton;
    private bool onNewGame;
    private bool onNewGameConfirm;
    private bool onLoadGame;
    private bool onOptions;
    private bool onQuit;
    private bool onControls;
    private bool onSound;
    private bool onCredits;
    private GameObject lastSelected;
    private GameObject lastSlot;

    public bool OnNewGame { get => onNewGame; set => onNewGame = value; }
    public bool OnLoadGame { get => onLoadGame; set => onLoadGame = value; }
    public bool OnOptions { get => onOptions; set => onOptions = value; }
    public bool OnControls { get => onControls; set => onControls = value; }
    public bool OnSound { get => onSound; set => onSound = value; }
    public bool OnCredits { get => onCredits; set => onCredits = value; }
    public GameObject FirstButton { get => firstButton; set => firstButton = value; }
    public bool OnNewGameConfirm { get => onNewGameConfirm; set => onNewGameConfirm = value; }
    public GameObject LastSlot { get => lastSlot; set => lastSlot = value; }
    public GameObject LastSelected { get => lastSelected; set => lastSelected = value; }
    public bool OnQuit { get => onQuit; set => onQuit = value; }

    private void Awake()
    {
        //disable the mouse
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void OnDestroy()
    {
        FMODManager.instance.StopMenuMusic();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(2))
        {
            EventSystem.current.SetSelectedGameObject(LastSelected);
        }
        if (EventSystem.current.currentSelectedGameObject != null)
        {
            LastSelected = EventSystem.current.currentSelectedGameObject;
        }

        if (!isMoving)
        {
            if (Input.GetButtonDown("Cancel"))
            {
                FMODManager.instance.PlayMenuBack();
                if (OnNewGame)
                {
                    if (!onNewGameConfirm)
                    {
                        newGamePanel.SetActive(false);
                        StartCoroutine(ReturnStartPositionCo(firstButton));
                        MoveCamera(startPosition);
                        OnNewGame = false;
                    }
                    else
                    {
                        newGameConfirmPanel.SetActive(false);
                        StartCoroutine(SelectButtonCo(lastSlot));
                        onNewGameConfirm = false;
                    }

                }
                else if (onLoadGame)
                {
                    loadGamePanel.SetActive(false);
                    StartCoroutine(ReturnStartPositionCo(firstButton));
                    MoveCamera(startPosition);
                    onLoadGame = false;
                }
                else if (onOptions)
                {
                    optionsPanel.SetActive(false);
                    StartCoroutine(ReturnStartPositionCo(firstButton));
                    MoveCamera(startPosition);
                    onOptions = false;
                }
                else if (OnControls)
                {
                    controlsPanel.SetActive(false);

                    StartCoroutine(SelectButtonCo(controlsButton));
                    OnControls = false;
                    onOptions = true;
                }
                else if (onSound)
                {
                    soundPanel.SetActive(false);

                    StartCoroutine(SelectButtonCo(controlsButton));
                    OnSound = false;
                    onOptions = true;
                }
                else if (OnCredits)
                {
                    creditsPanel.SetActive(false);

                    StartCoroutine(SelectButtonCo(controlsButton));
                    OnCredits = false;
                    onOptions = true;
                }
                else if (onQuit)
                {
                    quitPanel.SetActive(false);
                    StartCoroutine(ReturnStartPositionCo(firstButton));
                    MoveCamera(startPosition);
                    OnQuit = false;
                }
            }
        }



        //RIMUOVERE
        if (Input.GetKeyDown(KeyCode.Q))
        {
            MoveCamera(startPosition);
        }
    }

    public void MoveCamera(Transform target)
    {
        if (isMoving) return;
        Vector3 finalPos = target.position;
        Quaternion finalRot = target.rotation;
        StartCoroutine(MoveCameraCo(finalPos, finalRot));
    }

    protected IEnumerator MoveCameraCo(Vector3 finalPos, Quaternion finalRot)
    {
        isMoving = true;
        WaitForEndOfFrame eof = new WaitForEndOfFrame();
        Vector3 startPos = mainCamera.position;
        Quaternion startRot = mainCamera.rotation;
        float animationAmount = 0.0f;
        float speed = 1.0f / time;
        
        while (animationAmount < 1.0f)
        {
            animationAmount = Mathf.Min(1.0f, animationAmount + Time.deltaTime * speed);

            Vector3 newPos = Vector3.Lerp(startPos, finalPos, animationAmount);
            Quaternion newRot = Quaternion.Slerp(startRot, finalRot, animationAmount);
            mainCamera.position = newPos;
            mainCamera.rotation = newRot;

            yield return eof;
        }

        isMoving = false;
    }

    public void ReturnStartPosition(GameObject button)
    {
        StartCoroutine(ReturnStartPositionCo(button));
    }

    IEnumerator ReturnStartPositionCo(GameObject button)
    {
        yield return StartCoroutine(MoveCameraCo(startPosition.position, startPosition.rotation));

        yield return new WaitForEndOfFrame();

        button.GetComponent<Button>().Select();
    }

    public void SelectButton(GameObject button)
    {
        StartCoroutine(SelectButtonCo(button));
    }

    IEnumerator SelectButtonCo(GameObject button)
    {
        yield return new WaitForEndOfFrame();

        yield return new WaitUntil(() => !isMoving);
        button.GetComponent<Button>().Select();
    }

    public void LoadScene()
    {
        SceneManager.LoadScene("MenuToHub");
    }

    public void ApplicationQuit()
    {
        Application.Quit();
    }
}
