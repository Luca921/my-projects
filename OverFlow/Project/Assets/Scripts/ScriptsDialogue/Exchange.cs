﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

//scripts on every exchange in the shop panel 
public class Exchange : MonoBehaviour
{
    [HideInInspector]
    public Card cardToGive;
    [HideInInspector]
    public Card cardToReceive;
    [HideInInspector]
    public int cardToGiveN;
    [HideInInspector]
    public int cardToReceiveN;

    [Header("TRADE STATUS") , Space(10)]
    public Material availableTrade;
    public Material unvailableTrade;
    public TextMeshProUGUI reward;
    public TextMeshProUGUI cost;
    public TextMeshProUGUI cardsToGive;



    [HideInInspector]
    public GameObject confirmExchangePanel;
    
    public GameObject panelButtonSelection;

    public bool exchangeFound;
    bool tradeCompleted;

    private void Start()
    {
        EnableConfirmExchangePanel exchangePanel = panelButtonSelection.GetComponent<EnableConfirmExchangePanel>();
        exchangePanel.confirmExchangePanel = confirmExchangePanel;
        exchangePanel.cardToGive = cardToGive;
        exchangePanel.cardToReceive = cardToReceive;
        exchangePanel.cardsToGiveN = cardToGiveN;
        exchangePanel.cardsToReceiveN = cardToReceiveN;

        //CheckExchange();
    }

    public void CheckExchange()
    {
        List<Card> cardToRemove = new List<Card>();
        exchangeFound = false;
        int howManyFound = 0;

        //check in the inventory how many copy of the card we need we have
        foreach (Card c in GameManager.instance.inventory.cards)
        {
            if (c.name == cardToGive.name)
            {
                howManyFound++;
                if (howManyFound == cardToGiveN)
                {
                    exchangeFound = true;
                    panelButtonSelection.GetComponent<Button>().interactable = true;

                    EnableConfirmExchangePanel componentExchange = new EnableConfirmExchangePanel();

                    ////if there is no object selected or the object selected is not a trade we select the first trade available
                    //if (EventSystem.current.currentSelectedGameObject != null)
                    //{
                    //    //componentExchange = EventSystem.current.currentSelectedGameObject.GetComponent<EnableConfirmExchangePanel>();
                    //}

                    //if (componentExchange == null || EventSystem.current.currentSelectedGameObject == null)
                    //{
                    //    //EventSystem.current.SetSelectedGameObject(transform.GetChild(5).gameObject);
                    //}
                    break;
                }
            }
        }

        //if not all the cards have been found in the inventory we check in the deck
        if (howManyFound < cardToGiveN)
        {
            foreach (Card c in GameManager.instance.deck.cards)
            {
                if (c.name == cardToGive.name)
                {
                    howManyFound++;
                    if (howManyFound == cardToGiveN)
                    {
                        //if we have all the cards needed exchange has been found
                        exchangeFound = true;
                        panelButtonSelection.GetComponent<Button>().interactable = true;

                        Exchange componentExchange = new Exchange();

                        //if there is no object selected or the object selected is not a trade we select the first trade available
                        //if (EventSystem.current.currentSelectedGameObject != null)
                        //{
                        //    componentExchange = EventSystem.current.currentSelectedGameObject.GetComponent<Exchange>();
                        //}

                        //if (EventSystem.current.currentSelectedGameObject == null)
                        //{
                        //    EventSystem.current.SetSelectedGameObject(transform.GetChild(5).gameObject);
                        //}

                        break;
                    }

                }
            }
        }

        if(!exchangeFound)
        {
            panelButtonSelection.GetComponent<Button>().interactable = true;

            //deactivate the button if you dont own enough cards
            //panelButtonSelection.GetComponent<Button>().interactable = false;
            reward.fontMaterial = unvailableTrade;
            cost.fontMaterial = unvailableTrade;
            cardsToGive.fontMaterial = unvailableTrade;
        }
        else
        {
            reward.fontMaterial = availableTrade;
            cost.fontMaterial = availableTrade;
            cardsToGive.fontMaterial = availableTrade;
        }
        
    }

}
