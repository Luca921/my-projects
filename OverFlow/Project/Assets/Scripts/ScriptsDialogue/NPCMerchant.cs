﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

[System.Serializable]
public struct ExchangeSystem
{
    public int cardsToGiveN;
    public int cardsToReceiveN;
    public Card cardToGiveImg;
    public Card cardToReceiveImg;
}

//script on the merchant NPC
public class NPCMerchant : DialogueSystem
{
    #region Fields

    public GameObject shopPanel;
    public GameObject shopContent;
    public GameObject loadingBar;
    GameObject cardSlot;
    public GameObject shopSlot;
    public GameObject confirmExchangePanel;
    GridLayoutGroup shopGrid;
    public List<ExchangeSystem> shop;
    public bool firstSelectedFound = false;

    public List<Exchange> allExchanges = new List<Exchange>();

    public TextMeshProUGUI faceText;
    #endregion

    #region Methods

    public void ActivateMerchantPanel()
    {
        shopPanel.SetActive(!shopPanel.activeSelf);
    }

    private void OnEnable()
    {
        
    }

    //setup all the trades
    public void InitializePanel()
    {
        EventSystem.current.SetSelectedGameObject(null);
        for ( int i = 0 ; i < shop.Count ; i++ )
        {
            
            //instantiating the shop panel
            GameObject tempShopSlot = Instantiate(shopSlot);

            

            Exchange thisShopExchange = tempShopSlot.GetComponent<Exchange>();

            allExchanges.Add(thisShopExchange);

            tempShopSlot.transform.parent = shopContent.transform;

            //giving the images and numbers (of the cards) to the shop GUI panel 
            tempShopSlot.transform.GetChild(0).transform.GetChild(1).GetComponent<Image>().sprite = shop[i].cardToReceiveImg.artwork;
            tempShopSlot.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "x" + shop[i].cardsToReceiveN + " " + shop[i].cardToReceiveImg.name;
            tempShopSlot.transform.GetChild(2).transform.GetChild(1).GetComponent<Image>().sprite = shop[i].cardToGiveImg.artwork;
            tempShopSlot.transform.GetChild(4).GetComponent<TextMeshProUGUI>().text = "x" + shop[i].cardsToGiveN + " " + shop[i].cardToGiveImg.name;
            
            thisShopExchange.cardToGive = shop[i].cardToGiveImg;
            thisShopExchange.cardToReceive = shop[i].cardToReceiveImg;
            thisShopExchange.cardToGiveN = shop[i].cardsToGiveN;
            thisShopExchange.cardToReceiveN = shop[i].cardsToReceiveN;
            thisShopExchange.confirmExchangePanel = confirmExchangePanel;

            thisShopExchange.CheckExchange();
            if (!firstSelectedFound)
            {
                
                if (thisShopExchange.exchangeFound)
                {
                    EventSystem.current.SetSelectedGameObject(tempShopSlot.transform.GetChild(5).gameObject);
                    firstSelectedFound = true;
                }
                
            }

        }
    }

    public void SelectTrade()
    {
        if (allExchanges.Count > 0)
        {
            foreach (Exchange ex in allExchanges)
            {
                ex.CheckExchange();

                if (ex.exchangeFound && !firstSelectedFound)
                {
                    StartCoroutine(SelectDelayed(ex));
                    firstSelectedFound = true;
                    break;
                }
                
            }
        }
    }

    IEnumerator SelectDelayed(Exchange ex)
    {
        yield return new WaitForEndOfFrame();
        EventSystem.current.SetSelectedGameObject(ex.transform.GetChild(5).gameObject);
    }

    protected override void OnTriggerEnter(Collider other)
    {
        base.OnTriggerEnter(other);

        if (this.enabled && other.CompareTag("Player"))
        {
            faceText.text = "^_^";
            faceText.gameObject.SetActive(true);
        }
    }

    protected override void OnTriggerExit(Collider other)
    {
        base.OnTriggerExit(other);

        if (this.enabled && other.CompareTag("Player"))
        {
            faceText.gameObject.SetActive(false);
        }
    }

    #endregion
}
