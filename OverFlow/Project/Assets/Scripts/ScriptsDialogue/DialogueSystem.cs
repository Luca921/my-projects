﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DialogueSystem : MonoBehaviour
{
    #region Fields

    public GameObject inputToInteractKeyboard;
    public GameObject inputToInteractJoypad;
    public GameObject panelDialogue;
    public GameObject firstButtonFirstDialogue;
    public GameObject firstButtonInfoDialogue;
    public GameObject endDialoguePanelNoExchange;
    bool canQuitDialogue;
    public bool canStartDialogue = true;

    public bool dialogueActive;

    public GameObject FirstButtonInfoDialogue { get => firstButtonInfoDialogue; set => firstButtonInfoDialogue = value; }

    protected bool isInside;
    #endregion

    #region Methods

    protected virtual void OnTriggerEnter(Collider other)
    {
        if (this.enabled && other.CompareTag("Player"))
        {
            isInside = true;
        }

    }

    protected virtual void Update()
    {
        //check if the character that is entering the collider is tagged "Player" with the Unity editor
        if (isInside)
        {
            //check if the joystick is connected
            //for (int i = 0; i < Input.GetJoystickNames().Length; i++)            // da rivedere
            //{
            //    //if connected set active the relative ui interaction element
            //    if (!string.IsNullOrEmpty(Input.GetJoystickNames()[i]))
            //    {
            //        inputToInteractJoypad.SetActive(true);
            //        inputToInteractKeyboard.SetActive(false);
            //    }
            //}

            inputToInteractJoypad.SetActive(true);

            //open the dialogue box and disables the player input 
            if (Input.GetButtonDown("Interact") && GameManager.instance.pausable)
            {
                FMODManager.instance.HubNpcDialogue();
                if (!panelDialogue.activeSelf && canStartDialogue)
                {
                    canStartDialogue = false;
                    panelDialogue.SetActive(true);
                    GameManager.instance.inputManager.enabled = false;

                    inputToInteractKeyboard.SetActive(false);
                    inputToInteractJoypad.SetActive(false);
                    EventSystem.current.SetSelectedGameObject(firstButtonFirstDialogue);
                    GameManager.instance.SetPause();
                }
            }
        }
    }

    public void InfoPanelIsActive()
    {
        EventSystem.current.SetSelectedGameObject(FirstButtonInfoDialogue);
    }

    protected virtual void OnTriggerExit(Collider other)
    {
        //check if the character that is exiting the collider is tagged "Player" with the Unity editor
        if (this.enabled && other.CompareTag("Player"))
        {
            //inputToInteractKeyboard.SetActive(false);
            inputToInteractJoypad.SetActive(false);

            isInside = false;
        }
    }

    #endregion
}
