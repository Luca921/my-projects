﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ExplanationDialogueBehaviour : MonoBehaviour
{

    #region Fields

    public GameObject infoDialoguePanel;
    public GameObject firstButtonInfoDialogue;

    int counter;
    #endregion

    #region Methods
    
    void Update()
    {
        
        if(Input.GetButtonDown("Submit"))
        {
            gameObject.SetActive(false);
            infoDialoguePanel.SetActive(true);
            EventSystem.current.SetSelectedGameObject(firstButtonInfoDialogue);
        }
    }

    #endregion
}
