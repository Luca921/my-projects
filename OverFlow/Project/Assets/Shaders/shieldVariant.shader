// Shader created with Shader Forge v1.38 
// Shader Forge (c) Freya Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:3,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:True,hqlp:False,rprd:True,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:False,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:2865,x:33305,y:32653,varname:node_2865,prsc:2|emission-6800-OUT;n:type:ShaderForge.SFN_Tex2d,id:2404,x:32081,y:32770,ptovrint:False,ptlb:Texture,ptin:_Texture,varname:node_2404,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-8970-OUT;n:type:ShaderForge.SFN_Fresnel,id:843,x:32097,y:32947,varname:node_843,prsc:2|EXP-8683-OUT;n:type:ShaderForge.SFN_Multiply,id:7957,x:32302,y:32787,varname:node_7957,prsc:2|A-2404-RGB,B-843-OUT,C-201-OUT;n:type:ShaderForge.SFN_Clamp01,id:3291,x:32480,y:32787,varname:node_3291,prsc:2|IN-7957-OUT;n:type:ShaderForge.SFN_Color,id:918,x:32480,y:32632,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_918,prsc:2,glob:False,taghide:False,taghdr:True,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0.3632075,c3:0.3632075,c4:1;n:type:ShaderForge.SFN_SceneColor,id:1338,x:32257,y:32478,varname:node_1338,prsc:2;n:type:ShaderForge.SFN_Clamp01,id:4524,x:32480,y:32478,varname:node_4524,prsc:2|IN-1338-RGB;n:type:ShaderForge.SFN_Lerp,id:6800,x:32726,y:32632,varname:node_6800,prsc:2|A-4524-OUT,B-918-RGB,T-3291-OUT;n:type:ShaderForge.SFN_Slider,id:8683,x:31732,y:32997,ptovrint:False,ptlb:Fresnel Strength,ptin:_FresnelStrength,varname:node_8683,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:3,max:5;n:type:ShaderForge.SFN_Slider,id:201,x:31966,y:33148,ptovrint:False,ptlb:Transparency,ptin:_Transparency,varname:node_201,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:2;n:type:ShaderForge.SFN_TexCoord,id:3419,x:31355,y:32442,varname:node_3419,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Panner,id:9074,x:31584,y:32736,varname:node_9074,prsc:2,spu:0,spv:1|UVIN-3419-UVOUT,DIST-8824-OUT;n:type:ShaderForge.SFN_Panner,id:5417,x:31584,y:32590,varname:node_5417,prsc:2,spu:1,spv:0|UVIN-3419-UVOUT,DIST-9070-OUT;n:type:ShaderForge.SFN_Multiply,id:9070,x:31355,y:32600,varname:node_9070,prsc:2|A-796-OUT,B-1718-T;n:type:ShaderForge.SFN_Multiply,id:8824,x:31355,y:32736,varname:node_8824,prsc:2|A-1718-T,B-5782-OUT;n:type:ShaderForge.SFN_Time,id:1718,x:31157,y:32644,varname:node_1718,prsc:2;n:type:ShaderForge.SFN_Slider,id:796,x:30995,y:32446,ptovrint:False,ptlb:YSpeed,ptin:_YSpeed,varname:node_796,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-2,cur:0,max:2;n:type:ShaderForge.SFN_Slider,id:5782,x:30981,y:32860,ptovrint:False,ptlb:XSpeed,ptin:_XSpeed,varname:node_5782,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-2,cur:0,max:2;n:type:ShaderForge.SFN_Multiply,id:8970,x:31759,y:32671,varname:node_8970,prsc:2|A-5417-UVOUT,B-9074-UVOUT;proporder:2404-918-8683-201-796-5782;pass:END;sub:END;*/

Shader "Shader Forge/shieldVariant" {
    Properties {
        _Texture ("Texture", 2D) = "white" {}
        [HDR]_Color ("Color", Color) = (1,0.3632075,0.3632075,1)
        _FresnelStrength ("Fresnel Strength", Range(0, 5)) = 3
        _Transparency ("Transparency", Range(0, 2)) = 1
        _YSpeed ("YSpeed", Range(-2, 2)) = 0
        _XSpeed ("XSpeed", Range(-2, 2)) = 0
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        GrabPass{ }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _GrabTexture;
            uniform sampler2D _Texture; uniform float4 _Texture_ST;
            uniform float4 _Color;
            uniform float _FresnelStrength;
            uniform float _Transparency;
            uniform float _YSpeed;
            uniform float _XSpeed;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 projPos : TEXCOORD3;
                UNITY_FOG_COORDS(4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float2 sceneUVs = (i.projPos.xy / i.projPos.w);
                float4 sceneColor = tex2D(_GrabTexture, sceneUVs);
////// Lighting:
////// Emissive:
                float4 node_1718 = _Time;
                float2 node_8970 = ((i.uv0+(_YSpeed*node_1718.g)*float2(1,0))*(i.uv0+(node_1718.g*_XSpeed)*float2(0,1)));
                float4 _Texture_var = tex2D(_Texture,TRANSFORM_TEX(node_8970, _Texture));
                float3 emissive = lerp(saturate(sceneColor.rgb),_Color.rgb,saturate((_Texture_var.rgb*pow(1.0-max(0,dot(normalDirection, viewDirection)),_FresnelStrength)*_Transparency)));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "Meta"
            Tags {
                "LightMode"="Meta"
            }
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_META 1
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #include "UnityMetaPass.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _GrabTexture;
            uniform sampler2D _Texture; uniform float4 _Texture_ST;
            uniform float4 _Color;
            uniform float _FresnelStrength;
            uniform float _Transparency;
            uniform float _YSpeed;
            uniform float _XSpeed;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 projPos : TEXCOORD3;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityMetaVertexPosition(v.vertex, v.texcoord1.xy, v.texcoord2.xy, unity_LightmapST, unity_DynamicLightmapST );
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                return o;
            }
            float4 frag(VertexOutput i) : SV_Target {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float2 sceneUVs = (i.projPos.xy / i.projPos.w);
                float4 sceneColor = tex2D(_GrabTexture, sceneUVs);
                UnityMetaInput o;
                UNITY_INITIALIZE_OUTPUT( UnityMetaInput, o );
                
                float4 node_1718 = _Time;
                float2 node_8970 = ((i.uv0+(_YSpeed*node_1718.g)*float2(1,0))*(i.uv0+(node_1718.g*_XSpeed)*float2(0,1)));
                float4 _Texture_var = tex2D(_Texture,TRANSFORM_TEX(node_8970, _Texture));
                o.Emission = lerp(saturate(sceneColor.rgb),_Color.rgb,saturate((_Texture_var.rgb*pow(1.0-max(0,dot(normalDirection, viewDirection)),_FresnelStrength)*_Transparency)));
                
                float3 diffColor = float3(0,0,0);
                float specularMonochrome;
                float3 specColor;
                diffColor = DiffuseAndSpecularFromMetallic( diffColor, 0, specColor, specularMonochrome );
                o.Albedo = diffColor;
                
                return UnityMetaFragment( o );
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
