using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

[Serializable]
[PostProcess(typeof(GlitchVFXRenderer), PostProcessEvent.AfterStack, "Custom/Glitch")]
public sealed class GlitchVFX : PostProcessEffectSettings
{
    [Range(0f, 10f)]
    public FloatParameter offsetRight = new FloatParameter { value = 1f };
    [Range(0f, 10f)]
    public FloatParameter offsetLeft = new FloatParameter { value = 1f };
    [Range(0f, 10f)]
    public FloatParameter scanlineHeight = new FloatParameter { value = 1f };
    [Range(0f, 50f)]
    public FloatParameter frequency = new FloatParameter { value = 20f };
}

public sealed class GlitchVFXRenderer : PostProcessEffectRenderer<GlitchVFX>
{
    public override void Render(PostProcessRenderContext context)
    {
        var sheet = context.propertySheets.Get(Shader.Find("Hidden/Custom/GlitchShader"));
        sheet.properties.SetFloat("_OffsetRight", settings.offsetRight / 1920f);
        sheet.properties.SetFloat("_OffsetLeft", settings.offsetLeft / 1920f);
        sheet.properties.SetFloat("_ScanlineHeight", settings.scanlineHeight / 1080f);
        sheet.properties.SetFloat("_Frequency", settings.frequency);
        context.command.BlitFullscreenTriangle(context.source, context.destination, sheet, 0);
    }
}
