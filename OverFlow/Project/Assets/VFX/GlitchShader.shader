Shader "Hidden/Custom/GlitchShader"
{
	HLSLINCLUDE

#include "Packages/com.unity.postprocessing/PostProcessing/Shaders/StdLib.hlsl"

	TEXTURE2D_SAMPLER2D(_MainTex, sampler_MainTex);
	float _OffsetRight;
	float _OffsetLeft;
	float _ScanlineHeight;
	float _Frequency;

	float4 Frag(VaryingsDefault i) : SV_Target
	{
		float offset = 0;
		uint scanline = uint(i.texcoord.y / _ScanlineHeight) % 4;
		if (scanline == 0)
		{
			offset = _OffsetRight * sin(_Time.w * _Frequency);
		}
		else if (scanline == 2)
		{
			offset = _OffsetLeft * cos(_Time.w * _Frequency);
		}
		return SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, i.texcoord + float2(offset, 0));
	}

	ENDHLSL

	SubShader
	{
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			HLSLPROGRAM

			#pragma vertex VertDefault
			#pragma fragment Frag

			ENDHLSL
		}
	}
}
